﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace ExamSchedule
{
    public static class DB
    {
        private static MySqlConnection _connection;
        public static MySqlConnection Connection { get { return _connection; } }
        public static bool EstablishConnection(string server, string user, string port, string password, string database)
        {
            if (_connection != null)
                _connection.Dispose();
            _connection = new MySqlConnection(string.Format("server={0};user={1};port={2};password={3};database={4}", server, user, port, password, database));
            bool result = false;
            try
            {
                _connection.Open();
                result = _connection.Ping();
                _connection.Close();
            }
            catch (MySqlException e)
            {
#if DEBUG
                Trace.TraceError("Connection to database failed: {0}", e.Message);
#endif
                return false;
            }
            return result;
        }

        /// <method>Select Query</method>
        public static DataTable ExecuteSelectQuery(string query, params MySqlParameter[] sqlParameters)
        {
            DataTable dataTable;
            using (MySqlCommand command = new MySqlCommand(query, _connection))
            {
                MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                DataSet ds = new DataSet();
                try
                {
                    command.Parameters.AddRange(sqlParameters);
                    command.ExecuteNonQuery();

                    adapter.Fill(ds);
                    dataTable = ds.Tables[0];
                }
                catch (MySqlException e)
                {
                    Trace.TraceError("Error - Connection.executeSelectQuery - Query: " + query + " \nException: " + e.Message);
                    return null;
                }
                
            }
            return dataTable;
        }

        /// <method>Execute Query</method>
        public static bool ExecuteQuery(string query, params MySqlParameter[] sqlParameters)
        {
            bool r = true;
            using (MySqlCommand command = new MySqlCommand(query, _connection))
            {
                try
                {
                    command.Parameters.AddRange(sqlParameters);
                    command.ExecuteNonQuery();
                }
                catch (MySqlException e)
                {
                    Trace.TraceError("Error - Connection.ExecuteQuery - Query: " + query + " \nException: " + e.Message);
                    r = false;
                }
            }
            return r;
        }
        public static int ExecuteInsertQuery(string table, string[] columns, string[] values)
        {
            string query = "INSERT INTO `"+table+"`(";
            query += string.Join(", ", columns);
            query += ") VALUES (";
            query += string.Join(", ", values) + ")";
            int id = -1;
            using (MySqlCommand command = new MySqlCommand(query, _connection))
            {
                try
                {
                    command.ExecuteNonQuery();
                    id = (int)command.LastInsertedId;
                }
                catch (MySqlException e)
                {
                    Trace.TraceError("Error - Connection.ExecuteInsertQuery - Query: " + query + " \nException: " + e.Message);
                }
            }
            return id;
        }
        public static bool ExecuteUpdateQuery(string table, string[] columns, string[] values, string condition)
        {
            string query = "UPDATE `"+table+"` SET ";
            for (int i = 0; i < columns.Length; i++)
            {
                query += columns[i] + "=" + values[i] + ((i == columns.Length - 1) ? " " : ", ");
            }
            query += "WHERE " + condition;
            bool r = true;
            using (MySqlCommand command = new MySqlCommand(query, _connection))
            {
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (MySqlException e)
                {
                    Trace.TraceError("Error - Connection.ExecuteUpdateQuery - Query: " + query + " \nException: " + e.Message);
                    r = false;
                }
            }
            return r;
        }
        public static bool ExecuteDeleteQuery(string table, string condition)
        {
            string query = "DELETE FROM `" + table + "` WHERE "+condition;
            bool r = true;
            using (MySqlCommand command = new MySqlCommand(query, _connection))
            {
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (MySqlException e)
                {
                    
                    Trace.TraceError("Error - Connection.ExecuteDeleteQuery - Query: " + query + " \nException: " + e.Message);
                    r = false;
                }
            }
            return r;
        }
        public static void DisposeConnection()
        {
            _connection.Dispose();
            _connection = null;
            
        }
    }
}
