﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace ExamSchedule
{
    /*
    public abstract class DataBuffer
    {
        public virtual string Table { get { return ""; } }
        //protected List<IDataStructure> buffer;
        //public List<IDataStructure> Data { get { return buffer; } }
        

        public DataBuffer()
        {
            //buffer = new List<IDataStructure>();
            //_loaded = false;
        }
        public abstract void ReloadColumns(DataGridView controll);
        public abstract void AddRow(DataGridView controll, MySqlDataReader reader);
        public abstract string GetSelectCommand();

        public virtual void Reload(DataGridView controll)
        {
            controll.Rows.Clear();
           G.databaseConnection.Open();

            using (var com = new MySqlCommand(GetSelectCommand(), G.databaseConnection))
            {
                var reader = com.ExecuteReader();
                while (reader.Read())
                    AddRow(controll, reader);
            }
            G.databaseConnection.Close();
            
        }
        
    }

    public class LecturerBuffer : DataBuffer
    {
        public override string Table { get { return "Lecturer"; } }
        public override void ReloadColumns(DataGridView controll)
        {
            controll.Columns.Clear();
            controll.Columns.Add("idLecturer", "ID");
            controll.Columns.Add("nameLecturer", "ФИО");
            controll.Columns["idLecturer"].Width = 50;
            controll.Columns["nameLecturer"].Width = 300;
        }
        public override void AddRow(DataGridView controll, MySqlDataReader reader)
        {
            controll.Rows.Add(reader.GetInt32("idLecturer"), reader.GetString("nameLecturer"));
        }
        public override string GetSelectCommand() { return "SELECT * FROM Lecturer"; }
    }
    public class GroupBuffer : DataBuffer
    {
        public override string Table { get { return "Group"; } }
        public override void ReloadColumns(DataGridView controll)
        {
            controll.Columns.Clear();
            controll.Columns.Add("idGroup", "ID");
            controll.Columns.Add("nameGroup", "Название");
            controll.Columns.Add("departmentGroup", "Отделение");
            controll.Columns["idGroup"].Width = 50;
            controll.Columns["departmentGroup"].Width = 200;
        }
        public override void AddRow(DataGridView controll, MySqlDataReader reader)
        {
            controll.Rows.Add(reader.GetInt32("idGroup"), reader.GetString("nameGroup"), reader.GetString("departmentGroup"));
        }
        public override string GetSelectCommand() { return "SELECT * FROM `Group`"; }
    }
    public class SubjectBuffer : DataBuffer
    {
        public override string Table { get { return "Subject"; } }
        public override void ReloadColumns(DataGridView controll)
        {
            controll.Columns.Clear();
            controll.Columns.Add("idSubject", "ID");
            controll.Columns.Add("codeSubject", "Код");
            controll.Columns.Add("nameSubject", "Название");
            controll.Columns.Add("hoursSubject", "Кол-во часов");
            controll.Columns.Add("attestationTypeSubject", "Вид аттестации");
            controll.Columns.Add("testTypeSubject", "Вид оценивания");
            controll.Columns.Add("creditTypeSubject", "Вид зачета");
            controll.Columns.Add("markTypeSubject", "Вид оценки");

            controll.Columns["idSubject"].Width = 50;
            controll.Columns["codeSubject"].Width = 70;
            controll.Columns["nameSubject"].Width = 200;
            controll.Columns["hoursSubject"].Width = 140;
            controll.Columns["attestationTypeSubject"].Width = 150;
            controll.Columns["testTypeSubject"].Width = 170;
            controll.Columns["creditTypeSubject"].Width = 140;
            controll.Columns["markTypeSubject"].Width = 140;
        }

        public override void AddRow(DataGridView controll, MySqlDataReader reader)
        {
            controll.Rows.Add(reader.GetInt32("idSubject"), reader.GetString("codeSubject"), reader.GetString("nameSubject"), reader.GetInt32("hoursSubject"),
                reader.GetString("attestationTypeSubject") == "credit" ? "зачет" : "экзамен",
                reader.GetString("testTypeSubject") == "oral" ? "устно" : "письменно",
                reader.GetString("creditTypeSubject") == "dif" ? "дифференцированный" : "недифференцированный",
                (reader["markTypeSubject"] as string) ?? "");
        }
        public override string GetSelectCommand() { return "SELECT * FROM Subject"; }
    }
    public class StudentBuffer : DataBuffer
    {
        public override string Table { get { return "Student"; } }
        
        public override void ReloadColumns(DataGridView controll)
        {
            controll.Columns.Clear();
            controll.Columns.Add("idStudent", "ID");
            controll.Columns.Add("nameStudent", "ФИО");
            controll.Columns.Add("nameGroup", "Группа");
            controll.Columns.Add("idGroup", "idGroup");
            controll.Columns.Add("regStudent", "Номер зачетки");
            controll.Columns["idStudent"].Width = 50;
            controll.Columns["nameStudent"].Width = 400;
            controll.Columns["regStudent"].Width = 200;
            controll.Columns["idGroup"].Visible = false;
        }
        public override void AddRow(DataGridView controll, MySqlDataReader reader)
        {
            controll.Rows.Add(reader.GetInt32("idStudent"), reader.GetString("nameStudent"), reader.GetString("nameGroup"), reader.GetInt32("idGroup"), reader.GetInt32("regStudent"));
        }
        public override string GetSelectCommand() { return "SELECT a.idStudent, a.nameStudent, a.idGroup, b.nameGroup, a.regStudent FROM `Student` a, `Group` b WHERE a.idGroup = b.idGroup"; }

    }

    public class MarkBuffer : DataBuffer
    {
        public override string Table { get { return "Mark"; } }
        public override void ReloadColumns(DataGridView controll)
        {
            controll.Columns.Add("idMark", "ID");
            controll.Columns.Add("idStudent", "idStudent");
            controll.Columns.Add("nameStudent", "ФИО");
            controll.Columns.Add("regStudent", "№ зачетки");
            controll.Columns.Add("idSubject", "idSubject");
            controll.Columns.Add("nameSubject", "Предмет");
            controll.Columns.Add("codeSubject", "Код предмета");
            controll.Columns.Add("idLecturer", "idLecturer");
            controll.Columns.Add("nameLecturer", "Преподаватель");
            controll.Columns.Add("idGroup", "idGroup");
            controll.Columns.Add("nameGroup", "Группа");
            controll.Columns.Add("departmentGroup", "departmentGroup");
            controll.Columns.Add("mark", "Оценка");
            controll.Columns.Add("dateMark", "Дата");

            controll.Columns["idStudent"].Visible = false;
            controll.Columns["idSubject"].Visible = false;
            controll.Columns["idLecturer"].Visible = false;
            controll.Columns["idGroup"].Visible = false;
            controll.Columns["departmentGroup"].Visible = false;
        }

        public override string GetSelectCommand()
        {
            return @"SELECT   m.idMark, 
                                        st.idStudent, 
                                        st.nameStudent, 
                                        st.regStudent, 
                                        sb.idSubject, 
                                        sb.nameSubject, 
                                        sb.codeSubject, 
                                        l.idLecturer, 
                                        l.nameLecturer, 
                                        g.idGroup, 
                                        g.nameGroup, 
                                        g.departmentGroup, 
                                        m.mark, 
                                        m.dateMark 
                                FROM    Mark m, 
                                        Student st, 
                                        Subject sb, 
                                        Lecturer l, 
                                        `Group` g 
                                WHERE   m.idStudent = st.idStudent and 
                                        m.idLecturer = l.idLecturer and 
                                        m.idSubject = sb.idSubject and 
                                        st.idGroup = g.idGroup";
        }
        public override void AddRow(DataGridView controll, MySqlDataReader reader)
        {
            controll.Rows.Add(reader.GetInt32("idMark"),
                                        reader.GetInt32("idStudent"),
                                        reader.GetString("nameStudent"),
                                        reader.GetInt32("regStudent"),
                                        reader.GetInt32("idSubject"),
                                        reader.GetString("nameSubject"),
                                        reader.GetString("codeSubject"),
                                        reader.GetInt32("idLecturer"),
                                        reader.GetString("nameLecturer"),
                                        reader.GetInt32("idGroup"),
                                        reader.GetString("nameGroup"),
                                        reader.GetString("departmentGroup"),
                                        reader.GetInt32("mark"),
                                        reader.GetDateTime("dateMark"));
        }
    }
     * */
}
