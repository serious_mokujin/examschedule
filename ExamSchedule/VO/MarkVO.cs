﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamSchedule.VO
{
    public class MarkVO
    {
        private int _id;
        private int _mark;
        private DateTime _date;
        private int _idLecturer;
        private int _idStudent;
        private int _idSubject;

        public MarkVO(int id, int mark, DateTime date, int idLecturer, int idStudent, int idSubject)
        {
            _id = id;
            _mark = mark;
            _date = date;
            _idLecturer = idLecturer;
            _idStudent = idStudent;
            _idSubject = idSubject;
        }

        public MarkVO()
        {
            _id = -1;
            _mark = 0;
            _date = DateTime.MinValue;
            _idLecturer = -1;
            _idStudent = -1;
            _idSubject = -1;
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int Mark
        {
            get { return _mark; }
            set { _mark = value; }
        }

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public int IdStudent
        {
            get { return _idStudent; }
            set { _idStudent = value; }
        }

        public int IdLecturer
        {
            get { return _idLecturer; }
            set { _idLecturer = value; }
        }

        public int IdSubject
        {
            get { return _idSubject; }
            set { _idSubject = value; }
        }
    }
}
