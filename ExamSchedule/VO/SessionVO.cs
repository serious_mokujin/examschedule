﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamSchedule.VO
{
    public class SessionVO
    {
        private int _id;
        private DateTime _startDate;
        private DateTime _endDate;
        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private bool _gov;

        public bool Gov
        {
            get { return _gov; }
            set { _gov = value; }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        public SessionVO(int id, DateTime startDate, DateTime endDate, string description, bool gov)
        {
            _id = id;
            _startDate = startDate;
            _endDate = endDate;
            _gov = gov;
            _description = description;
        }

        public SessionVO()
        {
            _id = -1;
            _startDate = DateTime.Now;
            _endDate = DateTime.Now;
            _gov = false;
            _description = "";
        }
    }
}
