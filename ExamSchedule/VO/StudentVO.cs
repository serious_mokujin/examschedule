﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamSchedule.VO
{
    public class StudentVO
    {
        private int _id;
        private string _name;
        private int _idGroup;
        private int _registerNumber;

        public int Id { get { return _id; } set { _id = value; } }
        public string Name { get { return _name; } set { _name = value; } }
        public int IdGroup { get { return _idGroup; } set { _idGroup = value; } }
        public int RegisterNumber { get { return _registerNumber; } set { _registerNumber = value; } }

        public StudentVO(int id, string name, int groupId, int registerNumber)
        {
            this._id = id;
            this._name = name;
            this._idGroup = groupId;
            this._registerNumber = registerNumber;
        }
        public StudentVO()
        {
            this._id = -1;
            this._name = "";
            this._idGroup = -1;
            this._registerNumber = -1;
        }
    }
}
