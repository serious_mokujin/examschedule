﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamSchedule.VO
{
    public class LecturerVO
    {
        private int _id;
        private string _name;

        public int Id { get { return _id; } set { _id = value; } }
        public string Name { get { return _name; } set { _name = value; } }
        
        public LecturerVO(int id, string name)
        {
            this._id = id;
            this._name = name;
        }
        public LecturerVO()
	    {
            this._id = -1;
            this._name = "";
        }
    }
}
