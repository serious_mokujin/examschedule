﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamSchedule.VO
{
    public class SubjectVO
    {
        public enum MarkT { _5, _12 }

        private int _id;
        private string _name;
        private string _code;
        private int _hours;
        private MarkT _markType;

        public int Id { get { return _id; } set { _id = value; } }
        public string Code { get { return _code; } set { _code = value; } }
        public string Name { get { return _name; } set { _name = value; } }
        public int Hours { get { return _hours; } set { _hours = value; } }
        public MarkT MarkType { get { return _markType; } set { _markType = value; } }
        

        public SubjectVO(int id, string name, string code, int hours, MarkT markType)
        {
            this._id = id;
            this._name = name;
            this._code = code;
            this._hours = hours;
            this._markType = markType;
        }
        public SubjectVO()
        {
            this._id = -1;
            this._name = "";
            this._code = "";
            this._hours = 0;
            this._markType = 0;
        }
    }
}
