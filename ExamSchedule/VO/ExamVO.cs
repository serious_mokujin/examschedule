﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamSchedule.VO
{
    public class ExamVO
    {
        private int _id;
        private string _nameGroup;
        private int _idLecturer;
        private int _idSubject;
        private int _idSession;
        private string[] _commission;
        private int _idGroup;

        public int IdGroup
        {
            get { return _idGroup; }
            set { _idGroup = value; }
        }

        public string[] Commission
        {
            get { return _commission; }
            set { _commission = value; }
        }
        public string CommissionStr
        {
            get { return string.Join(";", _commission); }
            set { _commission = value.Split(';'); }
        }

        public int IdSession
        {
            get { return _idSession; }
            set { _idSession = value; }
        }

        private int _subGroup;

        public int SubGroup
        {
            get { return _subGroup; }
            set { _subGroup = value; }
        }

        private DateTime _consDateTime;
        private DateTime _examDateTime;
        private string _consAuditory;
        private string _examAuditory;

        public DateTime ExamDate {
            get { return _examDateTime; }
            set { _examDateTime = value; }
        }

        public DateTime ConsDate
        {
            get { return _consDateTime; }
            set { _consDateTime = value; }
        }

        public string ConsAuditory {
            get { return _consAuditory; }
            set { _consAuditory = value; }
        }

        public string ExamAuditory
        {
            get { return _examAuditory; }
            set { _examAuditory = value; }
        }


        public int Id { get { return _id; } set { _id = value; } }
        public string NameGroup { get { return _nameGroup; } set { _nameGroup = value; } }
        public int IdLecturer
        {
            get { return _idLecturer; }
            set { _idLecturer = value; }
        }

        public int IdSubject
        {
            get { return _idSubject; }
            set { _idSubject = value; }
        }
        
        public ExamVO(int id, int idSession, int idGroup, string nameGroup, int idLecturer, int idSubject, DateTime consDateTime, DateTime examDateTime, string consAuditory, string examAuditory, int subGroup, string commission)
        {
            _id = id;
            _idSession = idSession;
            _idGroup = idGroup;
            _nameGroup = nameGroup;
            _idLecturer = idLecturer;
            _idSubject = idSubject;
            _consDateTime = consDateTime;
            _examDateTime = examDateTime;
            _consAuditory = consAuditory;
            _examAuditory = examAuditory;
            _subGroup = subGroup;
            CommissionStr = commission;
        }

        public ExamVO()
        {
            _id = -1;
            _idSession = -1;
            _idGroup = -1;
            _nameGroup = "";
            _idLecturer = -1;
            _idSubject = -1;
            _consDateTime = DateTime.Now;
            _examDateTime = DateTime.Now;
            _consAuditory = "000";
            _examAuditory = "000";
            _subGroup = 0;
            CommissionStr = "";
        }
    }
}
