﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamSchedule.VO
{
    public class GroupVO
    {
        private int _id;
        private int _number;
        private int _courseGroup;
        private int _yearPostfixGroup;
        private char _customPostfixGroup;
        private int _idDepartment;
        private bool _graduated;

        public GroupVO(int id, int number, int courseGroup, int yearPostfixGroup, char customPostfixGroup, int department, bool graduated)
        {
            _id = id;
            _number = number;
            _courseGroup = courseGroup;
            _yearPostfixGroup = yearPostfixGroup;
            _customPostfixGroup = customPostfixGroup;
            _idDepartment = department;
            _graduated = graduated;
        }
        public override string ToString()
        {
            return Name;
        }
        public int Number
        {
            get { return _number; }
            set { _number = value; }
        }

        public int Course
        {
            get { return _courseGroup; }
            set { _courseGroup = value; }
        }

        public int YearPostfix
        {
            get { return _yearPostfixGroup; }
            set { _yearPostfixGroup = value; }
        }

        public char CustomPostfix
        {
            get { return _customPostfixGroup; }
            set { _customPostfixGroup = value; }
        }

        
        public int Id { get { return _id; } set { _id = value; } }
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public int IdDepartment { get { return _idDepartment; } set { _idDepartment = value; } }

        public bool Graduated
        {
            get { return _graduated; }
            set { _graduated = value; }
        }

        public GroupVO()
        {
            _graduated = false;
            _id = -1;
            _number = 1;
            _courseGroup = 1;
            _yearPostfixGroup = 0;
            _customPostfixGroup = ' ';
            _idDepartment = -1;
        }
    }
}
