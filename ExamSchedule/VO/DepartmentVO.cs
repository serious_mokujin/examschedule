﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamSchedule.VO
{
    public class DepartmentVO
    {
        private int _id;
        private string _name;
        private string _shortForm;
        private int _idLecturer;
        private string _specializationName;

        private string _specializationCode;

        public DepartmentVO(int id, string name, int idLecturer, string shortForm, string specializationName, string specializationCode)
        {
            _id = id;
            _name = name;
            _idLecturer = idLecturer;
            _shortForm = shortForm;
            _specializationName = specializationName;
            _specializationCode = specializationCode;
        }

        public DepartmentVO()
        {
            _id = -1;
            _name = "";
            _idLecturer = -1;
            _shortForm = "";
            _specializationName = "";
            _specializationCode = "";
        }
        public override string ToString()
        {
            return ShortForm;
        }
        public string SpecializationName
        {
            get { return _specializationName; }
            set { _specializationName = value; }
        }

        public string SpecializationCode
        {
            get { return _specializationCode; }
            set { _specializationCode = value; }
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string ShortForm
        {
            get { return _shortForm; }
            set { _shortForm = value; }
        }

        public int IdLecturer
        {
            get { return _idLecturer; }
            set { _idLecturer = value; }
        }
    }
}
