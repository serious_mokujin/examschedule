CREATE DATABASE IF NOT EXISTS `ExamSchedule` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `ExamSchedule` ;

-- -----------------------------------------------------
-- Table `ExamSchedule`.`Group`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Group` (
  `idGroup` INT NOT NULL AUTO_INCREMENT ,
  `nameGroup` CHAR(8) NULL ,
  `otdelenieGroup` CHAR(45) NULL ,
  PRIMARY KEY (`idGroup`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExamSchedule`.`Student`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Student` (
  `idStudent` INT NOT NULL AUTO_INCREMENT ,
  `nameStudent` CHAR(80) NULL ,
  `idGroup` INT NOT NULL ,
  `regStudent` INT NOT NULL ,
  PRIMARY KEY (`idStudent`) ,
  INDEX `fk_Student_Group1_idx` (`idGroup` ASC) ,
  CONSTRAINT `fk_Student_Group1`
    FOREIGN KEY (`idGroup` )
    REFERENCES `ExamSchedule`.`Group` (`idGroup` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExamSchedule`.`Subject`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Subject` (
  `idSubject` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `codeSubject` CHAR(20) NOT NULL ,
  `nameSubject` CHAR(80) NULL ,
  `hoursSubject` INT NULL ,
  `attestationTypeSubject` ENUM('exam','credit') NOT NULL ,
  `testTypeSubject` ENUM('oral','written') NOT NULL ,
  `creditTypeSubject` ENUM('dif', 'ndif') NULL ,
  `markTypeSubject` ENUM('5','12') NULL ,
  PRIMARY KEY (`idSubject`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExamSchedule`.`Lecturer`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Lecturer` (
  `idLecturer` INT NOT NULL AUTO_INCREMENT ,
  `nameLecturer` CHAR(80) NULL ,
  PRIMARY KEY (`idLecturer`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExamSchedule`.`Mark`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Mark` (
  `idMark` INT NOT NULL AUTO_INCREMENT ,
  `mark` INT NOT NULL ,
  `dateMark` DATE NULL ,
  `idLecturer` INT NOT NULL ,
  `idStudent` INT NOT NULL ,
  `idSubject` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`idMark`) ,
  INDEX `fk_Mark_Lecturer1_idx` (`idLecturer` ASC) ,
  INDEX `fk_Mark_Student1_idx` (`idStudent` ASC) ,
  INDEX `fk_Mark_Subject1_idx` (`idSubject` ASC) ,
  CONSTRAINT `fk_Mark_Lecturer1`
    FOREIGN KEY (`idLecturer` )
    REFERENCES `ExamSchedule`.`Lecturer` (`idLecturer` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Mark_Student1`
    FOREIGN KEY (`idStudent` )
    REFERENCES `ExamSchedule`.`Student` (`idStudent` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Mark_Subject1`
    FOREIGN KEY (`idSubject` )
    REFERENCES `ExamSchedule`.`Subject` (`idSubject` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExamSchedule`.`Exam`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Exam` (
  `idExam` INT NOT NULL ,
  `idGroup` INT NOT NULL ,
  `idLecturer` INT NOT NULL ,
  `idSubject` INT UNSIGNED NOT NULL ,
  `consAuditory` INT NULL ,
  `consDate` DATETIME NULL ,
  `examAuditory` INT NULL ,
  `examDate` DATETIME NULL ,
  `examType` ENUM('regular', 'gos') NULL ,
  PRIMARY KEY (`idExam`) ,
  INDEX `fk_Exam_Group_idx` (`idGroup` ASC) ,
  INDEX `fk_Exam_Lecturer1_idx` (`idLecturer` ASC) ,
  INDEX `fk_Exam_Subject1_idx` (`idSubject` ASC) ,
  CONSTRAINT `fk_Exam_Group`
    FOREIGN KEY (`idGroup` )
    REFERENCES `ExamSchedule`.`Group` (`idGroup` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Exam_Lecturer1`
    FOREIGN KEY (`idLecturer` )
    REFERENCES `ExamSchedule`.`Lecturer` (`idLecturer` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Exam_Subject1`
    FOREIGN KEY (`idSubject` )
    REFERENCES `ExamSchedule`.`Subject` (`idSubject` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `ExamSchedule` ;