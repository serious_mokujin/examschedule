
CREATE SCHEMA IF NOT EXISTS `ExamSchedule` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `ExamSchedule` ;

-- -----------------------------------------------------
-- Table `ExamSchedule`.`Lecturer`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Lecturer` (
  `idLecturer` INT NOT NULL AUTO_INCREMENT ,
  `nameLecturer` CHAR(50) NOT NULL ,
  PRIMARY KEY (`idLecturer`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExamSchedule`.`Department`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Department` (
  `idDepartment` INT NOT NULL AUTO_INCREMENT ,
  `nameDepartment` CHAR(80) NOT NULL ,
  `idHeadLecturer` INT NOT NULL ,
  `shortFormDepartment` CHAR(3) NOT NULL ,
  `specializationCode` CHAR(45) NULL ,
  `specializationName` CHAR(100) NULL ,
  PRIMARY KEY (`idDepartment`) ,
  INDEX `fk_Department_Lecturer1_idx` (`idHeadLecturer` ASC) ,
  CONSTRAINT `fk_Department_Lecturer1`
    FOREIGN KEY (`idHeadLecturer` )
    REFERENCES `ExamSchedule`.`Lecturer` (`idLecturer` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExamSchedule`.`Group`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Group` (
  `idGroup` INT NOT NULL AUTO_INCREMENT ,
  `numberGroup` INT NOT NULL ,
  `courseGroup` INT NOT NULL ,
  `yearPostfixGroup` INT NOT NULL ,
  `customPostfixGroup` CHAR(1) NULL ,
  `idDepartment` INT NOT NULL ,
  `graduated` TINYINT(1) NULL ,
  PRIMARY KEY (`idGroup`) ,
  INDEX `fk_Group_Department1_idx` (`idDepartment` ASC) ,
  CONSTRAINT `fk_Group_Department1`
    FOREIGN KEY (`idDepartment` )
    REFERENCES `ExamSchedule`.`Department` (`idDepartment` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExamSchedule`.`Student`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Student` (
  `idStudent` INT NOT NULL AUTO_INCREMENT ,
  `nameStudent` CHAR(50) NOT NULL ,
  `idGroup` INT NOT NULL ,
  `regStudent` INT NOT NULL ,
  PRIMARY KEY (`idStudent`) ,
  INDEX `fk_Student_Group1_idx` (`idGroup` ASC) ,
  CONSTRAINT `fk_Student_Group1`
    FOREIGN KEY (`idGroup` )
    REFERENCES `ExamSchedule`.`Group` (`idGroup` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExamSchedule`.`Subject`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Subject` (
  `idSubject` INT NOT NULL AUTO_INCREMENT ,
  `codeSubject` CHAR(20) NOT NULL ,
  `nameSubject` CHAR(80) NOT NULL ,
  `hoursSubject` INT NOT NULL ,
  `markTypeSubject` ENUM('5','12') NOT NULL ,
  PRIMARY KEY (`idSubject`) ,
  UNIQUE INDEX `codeSubject_UNIQUE` (`codeSubject` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExamSchedule`.`Mark`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Mark` (
  `idMark` INT NOT NULL AUTO_INCREMENT ,
  `mark` INT NOT NULL ,
  `dateMark` DATE NULL ,
  `idLecturer` INT NOT NULL ,
  `idStudent` INT NOT NULL ,
  `idSubject` INT NOT NULL ,
  PRIMARY KEY (`idMark`) ,
  INDEX `fk_Mark_Lecturer1_idx` (`idLecturer` ASC) ,
  INDEX `fk_Mark_Student1_idx` (`idStudent` ASC) ,
  INDEX `fk_Mark_Subject1_idx` (`idSubject` ASC) ,
  CONSTRAINT `fk_Mark_Lecturer1`
    FOREIGN KEY (`idLecturer` )
    REFERENCES `ExamSchedule`.`Lecturer` (`idLecturer` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Mark_Student1`
    FOREIGN KEY (`idStudent` )
    REFERENCES `ExamSchedule`.`Student` (`idStudent` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Mark_Subject1`
    FOREIGN KEY (`idSubject` )
    REFERENCES `ExamSchedule`.`Subject` (`idSubject` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExamSchedule`.`Session`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Session` (
  `idSession` INT NOT NULL AUTO_INCREMENT ,
  `startDateSession` DATE NOT NULL ,
  `endDateSession` DATE NOT NULL ,
  `descriptionSession` CHAR(80) NULL ,
  `govSession` TINYINT(1) NOT NULL ,
  PRIMARY KEY (`idSession`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExamSchedule`.`Exam`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`Exam` (
  `idExam` INT NOT NULL AUTO_INCREMENT ,
  `idSession` INT NOT NULL ,
  `nameGroup` CHAR(8) NOT NULL ,
  `idGroup` INT NOT NULL ,
  `idLecturer` INT NOT NULL ,
  `idSubject` INT NOT NULL ,
  `subGroup` INT NOT NULL ,
  `commissionExam` CHAR(255) NULL ,
  `consAuditory` CHAR(4) NULL ,
  `consDate` DATETIME NOT NULL ,
  `examAuditory` CHAR(4) NULL ,
  `examDate` DATETIME NOT NULL ,
  PRIMARY KEY (`idExam`) ,
  INDEX `fk_Exam_Session1_idx` (`idSession` ASC) ,
  INDEX `fk_Exam_Group1_idx` (`idGroup` ASC) ,
  INDEX `fk_Exam_Lecturer1_idx` (`idLecturer` ASC) ,
  INDEX `fk_Exam_Subject1_idx` (`idSubject` ASC) ,
  CONSTRAINT `fk_Exam_Session1`
    FOREIGN KEY (`idSession` )
    REFERENCES `ExamSchedule`.`Session` (`idSession` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Exam_Group1`
    FOREIGN KEY (`idGroup` )
    REFERENCES `ExamSchedule`.`Group` (`idGroup` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Exam_Lecturer1`
    FOREIGN KEY (`idLecturer` )
    REFERENCES `ExamSchedule`.`Lecturer` (`idLecturer` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Exam_Subject1`
    FOREIGN KEY (`idSubject` )
    REFERENCES `ExamSchedule`.`Subject` (`idSubject` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ExamSchedule`.`LecturerSubject`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ExamSchedule`.`LecturerSubject` (
  `idLecturerSubject` INT NOT NULL AUTO_INCREMENT ,
  `idLecturer` INT NOT NULL ,
  `idSubject` INT NOT NULL ,
  PRIMARY KEY (`idLecturerSubject`) ,
  INDEX `fk_LecturerSubject_Lecturer1_idx` (`idLecturer` ASC) ,
  INDEX `fk_LecturerSubject_Subject1_idx` (`idSubject` ASC) ,
  CONSTRAINT `fk_LecturerSubject_Lecturer1`
    FOREIGN KEY (`idLecturer` )
    REFERENCES `ExamSchedule`.`Lecturer` (`idLecturer` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_LecturerSubject_Subject1`
    FOREIGN KEY (`idSubject` )
    REFERENCES `ExamSchedule`.`Subject` (`idSubject` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `ExamSchedule` ;

