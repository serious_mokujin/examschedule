﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ExamSchedule.DAO
{
    public class GroupDAO
    {
        public DataTable GetGroups()
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT g.*, CONCAT(d.shortFormDepartment, '-', g.courseGroup, g.numberGroup, g.yearPostfixGroup, IFNULL(g.customPostfixGroup, ' ')) AS nameGroup FROM `Group` g, `Department` d WHERE g.idDepartment = d.idDepartment");
            DB.Connection.Close();
            return table;
        }
        public DataTable GetNotGraduatedGroups()
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT g.*, CONCAT(d.shortFormDepartment, '-', g.courseGroup, g.numberGroup, g.yearPostfixGroup, IFNULL(g.customPostfixGroup, ' ')) AS nameGroup FROM `Group` g, `Department` d WHERE g.idDepartment = d.idDepartment and g.graduated = 0");
            DB.Connection.Close();
            return table;
        }
        public int InsertGroup(int course, int number, int year, char postfix, int department, bool graduated)
        {
            DB.Connection.Open();
            int p = DB.ExecuteInsertQuery("Group",
                                          new string[]
                                              {
                                                  "idGroup", "courseGroup", "numberGroup", "yearPostfixGroup",
                                                  "customPostfixGroup", "idDepartment", "graduated"
                                              },
                                          new string[]
                                              {
                                                  "NULL", course.ToString(), number.ToString(), year.ToString(),
                                                  postfix == ' ' ? "NULL" : postfix.ToString().Embrace("'"),
                                                  department.ToString(), (graduated ? 1 : 0).ToString()
                                              });
            DB.Connection.Close();
            return p;
        }

        public DataTable GetGroupsInDepartment(int department)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT g.*, CONCAT(d.shortFormDepartment, '-', g.courseGroup, g.numberGroup, g.yearPostfixGroup, IFNULL(g.customPostfixGroup, ' ')) AS nameGroup FROM `Group` g, `Department` d WHERE g.idDepartment = d.idDepartment and g.idDepartment=@department", new MySqlParameter("department", department));
            DB.Connection.Close();
            return table;
        }
        internal DataTable GetGroupsInDepartmentNotGraduated(int department)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT g.*, CONCAT(d.shortFormDepartment, '-', g.courseGroup, g.numberGroup, g.yearPostfixGroup, IFNULL(g.customPostfixGroup, ' ')) AS nameGroup FROM `Group` g, `Department` d WHERE g.idDepartment = d.idDepartment and g.idDepartment=@department and g.graduated = 0", new MySqlParameter("department", department));
            DB.Connection.Close();
            return table;
        }
        public DataRow GetGroupWithId(int id)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT g.*, CONCAT(d.shortFormDepartment, '-', g.courseGroup, g.numberGroup, g.yearPostfixGroup, IFNULL(g.customPostfixGroup, ' ')) AS nameGroup FROM `Group` g, `Department` d WHERE g.idDepartment = d.idDepartment and g.idGroup = @id", new MySqlParameter("id", id));
                                                          
            DB.Connection.Close();
            if(table.Rows.Count > 0)
                return table.Rows[0];
            return null;
        }

        internal void UpdateGroup(int id, int course, int number, int year, char postfix, int department, bool graduated)
        {
            DB.Connection.Open();
            DB.ExecuteUpdateQuery("Group", new string[]
                {
                    "courseGroup", "numberGroup", "yearPostfixGroup",
                    "customPostfixGroup",  "idDepartment", "graduated"
                },
                                  new string[]
                                      {
                                          course.ToString(), number.ToString(), year.ToString(),
                                          postfix == ' ' ? "NULL" : postfix.ToString().Embrace("'"),
                                          department.ToString(), (graduated ? 1 : 0).ToString()
                                      }, "idGroup = " + id);
            DB.Connection.Close();
        }

        public void DeleteGroup(int id)
        {
            DB.Connection.Open();
            DB.ExecuteDeleteQuery("Group", "idGroup=" + id);
            DB.Connection.Close();
        }


        public DataRow GetGroupWithName(string name)
        {
            DB.Connection.Open();
            var table =
                DB.ExecuteSelectQuery(
                    "SELECT g.*, CONCAT(d.shortFormDepartment, '-', g.courseGroup, g.numberGroup, g.yearPostfixGroup, IFNULL(g.customPostfixGroup, ' ')) AS nameGroup FROM `Group` g, `Department` d WHERE g.idDepartment = d.idDepartment and CONCAT(d.shortFormDepartment, '-', g.courseGroup, g.numberGroup, g.yearPostfixGroup, IFNULL(g.customPostfixGroup, ' ')) = @name",
                    new MySqlParameter("name", name));

            DB.Connection.Close();
            if (table.Rows.Count > 0)
                return table.Rows[0];
            return null;
        }
    }
}
