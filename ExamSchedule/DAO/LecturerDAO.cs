﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ExamSchedule.DAO
{
    public class LecturerDAO
    {
        public DataTable GetLecturers()
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Lecturer`");
            DB.Connection.Close();
            return table;
        }
        public DataTable GetLecturersBySubject(int idSubject)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT l.* FROM `Lecturer` l, `LecturerSubject` ls WHERE ls.idSubject = @id and ls.idLecturer = l.idLecturer", new MySqlParameter("id", idSubject));
            DB.Connection.Close();
            return table;
        }
        public int InsertLecturer(string name)
        {
            DB.Connection.Open();
            int p = DB.ExecuteInsertQuery("Lecturer", new string[] { "idLecturer", "nameLecturer"}, new string[] { "NULL", name.Embrace("'") });
            DB.Connection.Close();
            return p;
        }
        public int InsertLecturerSubject(int idLecturer, int idSubject)
        {
            DB.Connection.Open();
            int p = DB.ExecuteInsertQuery("LecturerSubject", new string[] { "idLecturerSubject", "idLecturer", "idSubject" }, new string[] { "NULL", idLecturer.ToString(), idSubject.ToString() });
            DB.Connection.Close();
            return p;
        }
        public void DeleteLecturerSubject(int idLecturer, int idSubject)
        {
            DB.Connection.Open();
            DB.ExecuteDeleteQuery("LecturerSubject", string.Format("idLecturer={0} and idSubject={1}", idLecturer, idSubject));
            DB.Connection.Close();
        }

        public DataRow GetLecturerWithId(int id)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Lecturer` WHERE idLecturer = @id", new MySqlParameter("id", id));

            DB.Connection.Close();
            if (table.Rows.Count > 0)
                return table.Rows[0];
            return null;
        }

        internal void UpdateLecturer(int id, string name)
        {
            DB.Connection.Open();
            DB.ExecuteUpdateQuery("Lecturer", new string[]
                                                    {
                                                        "nameLecturer",
                                                    },
                                                new string[]
                                                    {
                                                        name.Embrace("'"),
                                                    }, "idLecturer = " + id);
            DB.Connection.Close();
        }

        public void DeleteLecturer(int id)
        {
            DB.Connection.Open();
            DB.ExecuteDeleteQuery("Lecturer", "idLecturer=" + id);
            DB.Connection.Close();
        }

        public DataRow GetLecturerWithName(string name)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Lecturer` WHERE nameLecturer = @name", new MySqlParameter("name", name));

            DB.Connection.Close();
            if (table.Rows.Count > 0)
                return table.Rows[0];
            return null;
        }
    }
}
