﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ExamSchedule.DAO
{
    public class SubjectDAO
    {
        public DataTable GetSubjects()
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Subject`");
            DB.Connection.Close();
            return table;
        }
        public DataTable GetSubjectsByLecturer(int idLecturer)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT s.* FROM `Subject` s, `LecturerSubject` ls WHERE ls.idLecturer = @id and ls.idSubject = s.idSubject", new MySqlParameter("id", idLecturer));
            DB.Connection.Close();
            return table;
        }
        public int InsertSubject(string code, string name, int hours, string markType)
        {
            DB.Connection.Open();
            int p = DB.ExecuteInsertQuery("Subject", new string[] { "idSubject",
                                                                    "codeSubject", 
                                                                    "nameSubject", 
                                                                    "hoursSubject", 
                                                                    "markTypeSubject" }, new string[] { "NULL",  code.Embrace("'"), 
                                                                                                                name.Embrace("'"),
                                                                                                                hours.ToString(), 
                                                                                                                markType.Embrace("'")});
            DB.Connection.Close();
            return p;
        }
        public int InsertLecturerSubject(int idLecturer, int idSubject)
        {
            DB.Connection.Open();
            int p = DB.ExecuteInsertQuery("LecturerSubject", new string[] { "idLecturerSubject", "idLecturer", "idSubject" }, new string[] { "NULL", idLecturer.ToString(), idSubject.ToString() });
            DB.Connection.Close();
            return p;
        }
        public void DeleteLecturerSubject(int idLecturer, int idSubject)
        {
            DB.Connection.Open();
            DB.ExecuteDeleteQuery("LecturerSubject", string.Format("idLecturer={0} and idSubject={1}", idLecturer, idSubject));
            DB.Connection.Close();
        }

        public DataRow GetSubjectrWithId(int id)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Subject` WHERE idSubject = @id", new MySqlParameter("id", id));

            DB.Connection.Close();
            if (table.Rows.Count > 0)
                return table.Rows[0];
            return null;
        }

        internal void UpdateSubject(int id, string code, string name, int hours, string markType)
        {
            DB.Connection.Open();
            DB.ExecuteUpdateQuery("Subject", new string[] { "codeSubject", 
                                                            "nameSubject", 
                                                            "hoursSubject", 
                                                            "markTypeSubject" }, new string[] { code.Embrace("'"), 
                                                                                               name.Embrace("'"),
                                                                                               hours.ToString(), 
                                                                                               markType.Embrace("'")}, "idSubject = " + id);
            DB.Connection.Close();
        }

        public void DeleteSubject(int id)
        {
            DB.Connection.Open();
            DB.ExecuteDeleteQuery("Subject", "idSubject=" + id);
            DB.Connection.Close();
        }

        public DataRow GetSubjectrWithName(string name)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Subject` WHERE nameSubject = @name", new MySqlParameter("name", name));

            DB.Connection.Close();
            if (table.Rows.Count > 0)
                return table.Rows[0];
            return null;
        }

        public DataRow GetSubjectrWithCode(string code)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Subject` WHERE codeSubject = @code", new MySqlParameter("code", code));

            DB.Connection.Close();
            if (table.Rows.Count > 0)
                return table.Rows[0];
            return null;
        }
    }
}
