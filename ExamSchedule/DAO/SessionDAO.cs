﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace ExamSchedule.DAO
{
    public class SessionDAO
    {
        public DataTable GetSessions()
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Session`");
            DB.Connection.Close();
            return table;
        }
        public DataTable GetLecturersInSession(int id)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT l.* FROM `Exam` e, `Lecturer` l WHERE e.idLecturer = l.idLecturer AND idSession = @id GROUP BY e.idLecturer", new MySqlParameter("id", id));
            DB.Connection.Close();
            return table;
        }
        public DataTable GetGroupsInSession(int id)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT nameGroup FROM `Exam` WHERE idSession = @id GROUP BY nameGroup", new MySqlParameter("id", id));
            DB.Connection.Close();
            return table;
        }

        public int InsertSession(DateTime start, DateTime end, string description, bool gov)
        {
            DB.Connection.Open();
            int p = DB.ExecuteInsertQuery("Session",
                                          new string[] {"idSession", "startDateSession", "endDateSession", "descriptionSession", "govSession"},
                                          new string[]
                                              {
                                                  "NULL", start.ToString("yyyy-MM-dd").Embrace("'"),
                                                  end.ToString("yyyy-MM-dd").Embrace("'"), description.Embrace("'"), (gov ? 1 : 0).ToString()
                                              });
            DB.Connection.Close();
            return p;
        }

        public DataRow GetSessionWithId(int id)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Session` WHERE idSession = @id", new MySqlParameter("id", id));

            DB.Connection.Close();
            if (table.Rows.Count > 0)
                return table.Rows[0];
            return null;
        }

        internal void UpdateSession(int id, DateTime start, DateTime end,string description, bool gov)
        {
            DB.Connection.Open();
            DB.ExecuteUpdateQuery("Session", new string[] { "startDateSession", "endDateSession", "descriptionSession", "govSession" },
                                  new string[]
                                      {
                                          start.ToString("yyyy-MM-dd").Embrace("'"),
                                          end.ToString("yyyy-MM-dd").Embrace("'"),description.Embrace("'"),
                                          (gov ? 1 : 0).ToString()
                                      }, "idSession = " + id);
            DB.Connection.Close();
        }

        public void DeleteSession(int id)
        {
            DB.Connection.Open();
            DB.ExecuteQuery("DELETE FROM `Session` WHERE idSession = @id", new MySqlParameter("id", id));
            DB.Connection.Close();
        }

        public DataTable GetSessionsOnYear( int year )
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Session` WHERE YEAR(startDateSession) = @yy", new MySqlParameter("yy", year));
            DB.Connection.Close();
            return table;
        }
    }
}
