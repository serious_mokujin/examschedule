﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ExamSchedule.DAO
{
    public class StudentDAO
    {
        public DataTable GetStudents()
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Student`");
            DB.Connection.Close();
            return table;
        }
        public DataTable GetStudentMark(int groupId, int subjectId)
        {
            DB.Connection.Open();
            /*
            var table = DB.ExecuteSelectQuery(@"SELECT s.nameStudent, m.mark 
                                                FROM `Student` s, `Mark` m
                                                WHERE s.idStudent = m.idStudent AND s.idGroup = @group AND m.idSubject = @subject",
                                              new MySqlParameter("group", groupId),
                                              new MySqlParameter("subject", subjectId));
             */
            var table = DB.ExecuteSelectQuery(@"SELECT s.nameStudent, (SELECT m.mark FROM `Mark` m WHERE s.idStudent = m.idStudent AND m.idSubject = @subject) AS mark 
                                                FROM `Student` s WHERE s.idGroup = @group ",
                                              new MySqlParameter("group", groupId),
                                              new MySqlParameter("subject", subjectId));
            DB.Connection.Close();
            return table;
        }
        public DataTable GetStudentsSearchName(string name)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Student` WHERE nameStudent LIKE '%"+name+"%'");
            DB.Connection.Close();
            return table;
        }
        public DataTable GetStudentsWithGroups()
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery(@"SELECT idStudent, nameStudent, regStudent, CONCAT(d.shortFormDepartment, '-', g.courseGroup, g.numberGroup, g.yearPostfixGroup, IFNULL(g.customPostfixGroup, ' ')) AS nameGroup FROM `Student` s, `Group` g, `Department` d WHERE g.idDepartment = d.idDepartment and s.idGroup = g.idGroup");
            DB.Connection.Close();
            return table;
        }
        public DataTable GetStudentsWithGroupsSearchName(string name)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery(@"SELECT idStudent, nameStudent, regStudent, CONCAT(d.shortFormDepartment, '-', g.courseGroup, g.numberGroup, g.yearPostfixGroup, IFNULL(g.customPostfixGroup, ' ')) AS nameGroup FROM `Student` s, `Group` g, `Department` d WHERE g.idDepartment = d.idDepartment and s.idGroup = g.idGroup and s.nameStudent LIKE '%" + name + "%'");
            DB.Connection.Close();
            return table;
        }
        public DataTable GetStudentsWithGroupsWithinGroup(int groupId)
        {
            DB.Connection.Open();
            var table =
                DB.ExecuteSelectQuery(
                    @"SELECT idStudent, nameStudent, regStudent, CONCAT(d.shortFormDepartment, '-', g.courseGroup, g.numberGroup, g.yearPostfixGroup, IFNULL(g.customPostfixGroup, ' ')) AS nameGroup FROM `Student` s, `Group` g, `Department` d WHERE g.idDepartment = d.idDepartment and s.idGroup = g.idGroup and s.idGroup=@id",
                    new MySqlParameter("id", groupId));
            DB.Connection.Close();
            return table;
        }
        public DataTable GetStudentsWithGroupsWithinGroupSearchName(int groupId, string name)
        {
            DB.Connection.Open();
            var table =
                DB.ExecuteSelectQuery(
                    @"SELECT idStudent, nameStudent, regStudent, CONCAT(d.shortFormDepartment, '-', g.courseGroup, g.numberGroup, g.yearPostfixGroup, IFNULL(g.customPostfixGroup, ' ')) AS nameGroup FROM `Student` s, `Group` g, `Department` d WHERE g.idDepartment = d.idDepartment and s.idGroup = g.idGroup and s.idGroup=@id and s.nameStudent LIKE '%" + name + "%'",
                    new MySqlParameter("id", groupId));
            DB.Connection.Close();
            return table;
        }
        public int InsertStudent(string name, int idGroup, int registerNumber)
        {
            DB.Connection.Open();
            int p = DB.ExecuteInsertQuery("Student", new string[] { "idStudent", "nameStudent", "idGroup", "regStudent" }, new string[] { "NULL", name.Embrace("'"), idGroup.ToString(), registerNumber.ToString() });
            DB.Connection.Close();
            return p;
        }

        public DataRow GetStudentWithId(int id)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Student` WHERE idStudent = @id", new MySqlParameter("id", id));

            DB.Connection.Close();
            if (table.Rows.Count > 0)
                return table.Rows[0];
            return null;
        }

        internal void UpdateStudent(int id, string name, int idGroup, int registerNumber)
        {
            DB.Connection.Open();
            DB.ExecuteUpdateQuery("Student", new string[]
                                                    {
                                                        "nameStudent",
                                                        "idGroup",
                                                        "regStudent"
                                                    },
                                                new string[]
                                                    {
                                                        name.Embrace("'"),
                                                        idGroup.ToString(),
                                                        registerNumber.ToString()
                                                    }, "idStudent = " + id);
            DB.Connection.Close();
        }

        public void DeleteStudent(int id)
        {
            DB.Connection.Open();
            DB.ExecuteDeleteQuery("Student", "idStudent=" + id);
            DB.Connection.Close();
        }

        internal DataTable GetStudentsFromGroup(int groupId)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Student` WHERE idGroup = @Id",
                                              new MySqlParameter("Id", groupId));
            DB.Connection.Close();
            return table;
        }
    }
}
