﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace ExamSchedule.DAO
{
    public class DepartmentDAO
    {
        public DataTable GetDepartments()
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Department`");
            DB.Connection.Close();
            return table;
        }
        public DataTable GetDepartmentsWithNames()
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT d.idDepartment, d.nameDepartment, d.shortFormDepartment, d.specializationName, d.specializationCode, l.nameLecturer FROM `Department` d, `Lecturer` l WHERE d.idHeadLecturer = l.idLecturer");
            DB.Connection.Close();
            return table;
        }

        public int InsertDepartment(string name, string shortForm, int headLecturer, string specializationName, string specializationCode)
        {
            DB.Connection.Open();
            int p = DB.ExecuteInsertQuery("Department",
                                          new string[]
                                              {
                                                  "idDepartment", "nameDepartment", "shortFormDepartment", "idHeadLecturer", "specializationName", "specializationCode"
                                              },
                                          new string[]
                                              {
                                                  "NULL", name.Embrace("'"), shortForm.Embrace("'"), headLecturer.ToString(), specializationName.Embrace("'"), specializationCode.Embrace("'")
                                              });
            DB.Connection.Close();
            return p;
        }
        
        public DataRow GetDepartmentWithId(int id)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Department` WHERE idDepartment = @id", new MySqlParameter("id", id));

            DB.Connection.Close();
            if (table.Rows.Count > 0)
                return table.Rows[0];
            return null;
        }

        internal void UpdateDepartment(int id, string name, string shortForm, int headLecturer, string specializationName, string specializationCode)
        {
            DB.Connection.Open();
            DB.ExecuteUpdateQuery("Department", new string[]
                {
                    "nameDepartment","shortFormDepartment", "idHeadLecturer", "specializationName", "specializationCode"
                },
                                  new string[]
                                      {
                                           name.Embrace("'"),shortForm.Embrace("'"), headLecturer.ToString(), specializationName.Embrace("'"), specializationCode.Embrace("'")
                                      }, "idDepartment = " + id);
            DB.Connection.Close();
        }

        public void DeleteDepartment(int id)
        {
            DB.Connection.Open();
            DB.ExecuteDeleteQuery("Department", "idDepartment=" + id);
            DB.Connection.Close();
        }
    }
}
