﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace ExamSchedule.DAO
{
    public class MarkDAO
    {
        public DataTable GetMarks()
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Mark`");
            DB.Connection.Close();
            return table;
        }
        
        public int InsertMark(int mark, DateTime date, int idLecturer,int idStudent, int idSubject)
        {
            DB.Connection.Open();
            int p = DB.ExecuteInsertQuery("Mark",
                                          new string[] {"idMark","mark", "dateMark", "idLecturer", "idStudent", "idSubject"},
                                          new string[]
                                              {
                                                  "NULL", mark.ToString(), date.ToString("yyyy-MM-dd").Embrace("'"),
                                                  idLecturer.ToString(), idStudent.ToString(), idSubject.ToString()
                                              });
                                              
            DB.Connection.Close();
            return p;
        }

        public DataRow GetMarkWithId(int id)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Mark` WHERE idMark = @id", new MySqlParameter("id", id));

            DB.Connection.Close();
            if (table.Rows.Count > 0)
                return table.Rows[0];
            return null;
        }

        internal void UpdateMark(int id, int mark, DateTime date, int idLecturer, int idStudent, int idSubject)
        {
            DB.Connection.Open();
            DB.ExecuteUpdateQuery("Mark", new string[]
                                                    {
                                                        "mark", "dateMark", "idLecturer", "idStudent", "idSubject"
                                                    },
                                                new string[]
                                                    {
                                                        mark.ToString(), date.ToString("yyyy-MM-dd").Embrace("'"),
                                                  idLecturer.ToString(), idStudent.ToString(), idSubject.ToString()
                                                    }, "idMark = " + id);
            DB.Connection.Close();
        }

        public void DeleteMark(int id)
        {
            DB.Connection.Open();
            DB.ExecuteDeleteQuery("Mark", "idMark=" + id);
            DB.Connection.Close();
        }

        internal DataTable GetMarksFromGroup(int groupId)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT m.* FROM `Mark` m, `Student` s WHERE m.idStudent=s.idStudent and s.idGroup= @Id",
                                              new MySqlParameter("Id", groupId));
            DB.Connection.Close();
            return table;
        }
        internal DataTable GetMarksFromGroupBySubject(int groupId, int subjectId)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT m.* FROM `Mark` m, `Student` s WHERE m.idStudent=s.idStudent and s.idGroup= @Id and m.idSubject=@idSubj",
                                              new MySqlParameter("Id", groupId),
                                              new MySqlParameter("idSubj", subjectId));
            DB.Connection.Close();
            return table;
        }
        internal DataTable GetMarksForStudent(int studentId)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Mark` WHERE m.idStudent=@Id",
                                              new MySqlParameter("Id", studentId));
            DB.Connection.Close();
            return table;
        }
        internal DataTable GetMarksForStudentBySubject(int studentId, int subjectId)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Mark` WHERE m.idStudent=@Id and m.idSubject=@idSubj",
                                              new MySqlParameter("Id", studentId),
                                              new MySqlParameter("idSubj", subjectId));
            DB.Connection.Close();
            return table;
        }

        public DataTable GetMarksFromGroupBySubjectWithNames(int groupId, int subjectId)
        {
            DB.Connection.Open();
            var table =
                DB.ExecuteSelectQuery(
                    "SELECT m.idMark, m.mark, m.dateMark, l.nameLecturer, st.nameStudent, sb.nameSubject FROM `Mark` m, `Student` st, `Subject` sb, `Lecturer` l WHERE l.idLecturer = m.idLecturer and m.idStudent=st.idStudent and st.idGroup= @Id and m.idSubject=@idSubj and m.idSubject=sb.idSubject",
                    new MySqlParameter("Id", groupId),
                    new MySqlParameter("idSubj", subjectId));
            DB.Connection.Close();
            return table;
        }
        public DataTable GetMarkWithSubjectStudentLecturer(int idSubject, int idStudent, int idLecturer)
        {
            DB.Connection.Open();
            var table =
                DB.ExecuteSelectQuery(
                    "SELECT * FROM `Mark` WHERE idLecturer = @l and idStudent = @st and idSubject=@sb",
                    new MySqlParameter("l", idLecturer),
                    new MySqlParameter("st", idStudent),
                    new MySqlParameter("sb", idSubject));
            DB.Connection.Close();
            return table;
        }
    }
}
