﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using ExamSchedule.VO;

namespace ExamSchedule.DAO
{
    public class ExamDAO
    {
        public DataTable GetExams()
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Exam`");
            DB.Connection.Close();
            return table;
        }
        
        public DataTable GetExamsInSession(int sessionId)
        {
            DB.Connection.Open();
            var table =
                DB.ExecuteSelectQuery("SELECT * FROM `Exam` WHERE idSession=@id",
                                      new MySqlParameter("id", sessionId));
            DB.Connection.Close();
            return table;
        }
        public DataTable GetExamsInSessionWithNames(int sessionId)
        {
            DB.Connection.Open();
            var table =
                DB.ExecuteSelectQuery(@"SELECT e.*, l.nameLecturer, s.nameSubject FROM `Exam` e, `Lecturer` l, `Subject` s WHERE 
                                        e.idLecturer = l.idLecturer AND e.idSubject = s.idSubject AND e.idSession=@id",
                                      new MySqlParameter("id", sessionId));
            DB.Connection.Close();
            return table;
        }

        public int InsertExam(int idSession, int idGroup, string nameGroup, int idLecturer, int idSubject,
                              string consAuditory, DateTime consDate, string examAuditory,
                              DateTime examDate,int subGroup, string commission)
        {
            DB.Connection.Open();
            int p = DB.ExecuteInsertQuery("Exam", new string[]
                {
                    "idExam",
                    "idSession",
                    "idGroup",
                    "nameGroup",
                    "idLecturer",
                    "idSubject",
                    "consAuditory",
                    "consDate",
                    "examAuditory",
                    "examDate",
                    "subGroup",
                    "commissionExam"
                }, new string[]
                    {
                        "NULL",
                        idSession.ToString(),
                        idGroup.ToString(),
                        nameGroup.Embrace("'"),
                        idLecturer.ToString(),
                        idSubject.ToString(),
                        consAuditory.Embrace("'"),
                        consDate.ToString("yyyy-MM-dd HH:mm:ss").Embrace("'"),
                        examAuditory.Embrace("'"),
                        examDate.ToString("yyyy-MM-dd HH:mm:ss").Embrace("'"),
                        subGroup.ToString().Embrace("'"),
                        commission.Embrace("'")
                    });
            DB.Connection.Close();
            return p;
        }

        internal void UpdateExam(int id, int idSession, int idGroup, string nameGroup, int idLecturer, int idSubject,
                                 string consAuditory, DateTime consDate, string examAuditory,
                                 DateTime examDate, int subGroup, string commission)
        {
            DB.Connection.Open();
            DB.ExecuteUpdateQuery("Exam", new string[]
                {
                    "idSession",
                    "idGroup",
                    "nameGroup",
                    "idLecturer",
                    "idSubject",
                    "consAuditory",
                    "consDate",
                    "examAuditory",
                    "examDate",
                    "subGroup",
                    "commissionExam"
                },
                                  new string[]
                                      {
                                          idSession.ToString(),
                                          idGroup.ToString(),
                                          nameGroup.Embrace("'"),
                                          idLecturer.ToString(),
                                          idSubject.ToString(),
                                          consAuditory.Embrace("'"),
                                          consDate.ToString("yyyy-MM-dd HH:mm:ss").Embrace("'"),
                                          examAuditory.Embrace("'"),
                                          examDate.ToString("yyyy-MM-dd HH:mm:ss").Embrace("'"),
                                          subGroup.ToString().Embrace("'"),
                                          commission.Embrace("'")
                                      }, "idExam = " + id);

            DB.Connection.Close();
        }

        public void DeleteExam(int id)
        {
            DB.Connection.Open();
            DB.ExecuteQuery(
                "DELETE FROM `Exam` WHERE idExam=@id",
                new MySqlParameter("id", id));
            DB.Connection.Close();
        }

        internal DataRow GetExamWithId(int id)
        {
            DB.Connection.Open();
            var table = DB.ExecuteSelectQuery("SELECT * FROM `Exam` WHERE idExam=@id", new MySqlParameter("id", id));

            DB.Connection.Close();
            if (table.Rows.Count > 0)
                return table.Rows[0];
            return null;
        }

        internal DataTable GetExamsInSessionOfCourse(int session, int course)
        {
            DB.Connection.Open();
            var table =
                DB.ExecuteSelectQuery(
                    @"SELECT e.*, l.nameLecturer, s.nameSubject FROM `Exam` e, `Lecturer` l, `Subject` s WHERE 
                                        e.idLecturer = l.idLecturer AND e.idSubject = s.idSubject AND e.nameGroup LIKE '%-" + course +"__%' AND e.idSession=@id",
                    new MySqlParameter("id", session));
            DB.Connection.Close();
            return table;
        }

        internal DataTable GetExamsInSessionForLecturer(int session, int lecturer)
        {
            DB.Connection.Open();
            var table =
                DB.ExecuteSelectQuery(
                    @"SELECT e.*, l.nameLecturer, s.nameSubject FROM `Exam` e, `Lecturer` l, `Subject` s WHERE 
                                        e.idLecturer = l.idLecturer AND e.idSubject = s.idSubject AND e.idLecturer = @lecturer AND e.idSession=@id",
                    new MySqlParameter("id", session), new MySqlParameter("lecturer", lecturer));
            DB.Connection.Close();
            return table;
        }

        public DataTable GetExamsInSessionForGroup(int session, string group)
        {
            DB.Connection.Open();
            var table =
                DB.ExecuteSelectQuery(
                    @"SELECT e.*, l.nameLecturer, s.nameSubject FROM `Exam` e, `Lecturer` l, `Subject` s WHERE 
                                        e.idLecturer = l.idLecturer AND e.idSubject = s.idSubject AND e.nameGroup=@group and e.idSession=@id",
                    new MySqlParameter("id", session), new MySqlParameter("group", group));
            DB.Connection.Close();
            return table;
        }
        public DataTable GetExamsInSessionOnDate(int session, DateTime date)
        {
            DB.Connection.Open();
            var table =
                DB.ExecuteSelectQuery(
                    @"SELECT e.*, l.nameLecturer, s.nameSubject FROM `Exam` e, `Lecturer` l, `Subject` s WHERE 
                                        e.idLecturer = l.idLecturer AND e.idSubject = s.idSubject AND e.idSession=@id and (DATE(e.consDate)=@date or DATE(e.examDate)=@date)",
                    new MySqlParameter("id", session), new MySqlParameter("date", date.Date));
            DB.Connection.Close();
            return table;
        }

        public DataTable GetExamsFromDate(DateTime date)
        {
            DB.Connection.Open();
            var table =
                DB.ExecuteSelectQuery(
                    @"SELECT e.*, l.nameLecturer, s.nameSubject FROM `Exam` e, `Lecturer` l, `Subject` s WHERE 
                                        e.idLecturer = l.idLecturer AND 
                                        e.idSubject = s.idSubject AND (DATE(e.consDate) >= @date OR DATE(e.examDate) >= @date)",
                    new MySqlParameter("date", date.Date));
            DB.Connection.Close();
            return table;
        }
    }
}
