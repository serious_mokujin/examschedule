﻿using ExamSchedule.BO;
using ExamSchedule.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExamSchedule.InputForms
{
    public partial class SubjectForm : Form, InputForm
    {
        private Form _next = null;
        public Form Next
        {
            get { return _next; }
            private set { _next = value; this.Close(); }
        }
        SubjectVO current;
        SubjectBO subjectBO;

        public SubjectForm()
        {
            InitializeComponent();
            subjectBO = new SubjectBO();
            current = new SubjectVO();
            ReloadGridView();
        }

        void ReloadGridView(bool saveSelection = false)
        {
            int selection = -1;
            if (saveSelection)
                if (dgv.SelectedRows.Count > 0)
                    selection = dgv.SelectedRows[0].Index;
            dgv.DataSource = subjectBO.GetRaw();
            dgv.Sort(dgv.Columns[0], ListSortDirection.Ascending);
            if (saveSelection)
                if (selection != -1 && dgv.Rows.Count > 0)
                    dgv.Rows[selection].Selected = true;

        }
        private void DataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                int id = (int)dgv.SelectedRows[0].Cells[0].Value;
                current = subjectBO.GetSubjectWithId(id);
                LoadData(current);
            }
        }

        public bool GrabData()
        {
            current.Code = subjectCodeTextBox.Text;
            current.Name = subjectNameTextBox.Text;
            current.Hours = (int)hoursNumericUpDown.Value;
            current.MarkType = fiveRadioButton.Checked ? SubjectVO.MarkT._5 : SubjectVO.MarkT._12;
            return true;
        }

        public void LoadData(SubjectVO data)
        {
            subjectCodeTextBox.Text = data.Code;
            subjectNameTextBox.Text = data.Name;
            hoursNumericUpDown.Value = data.Hours;

            fiveRadioButton.Checked = data.MarkType == SubjectVO.MarkT._5;
            twelveRadioButton.Checked = data.MarkType == SubjectVO.MarkT._12;
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            if (GrabData())
            {
                current.Id = -1;
                subjectBO.UpdateSubject(current);
                ReloadGridView();
                dgv.Rows[dgv.Rows.GetLastRow(DataGridViewElementStates.None)].Selected = true;
                if (dgv.SelectedRows.Count > 0)
                dgv.FirstDisplayedScrollingRowIndex = dgv.SelectedRows[0].Index;
            }
        }

        private void RemoveButtonClick(object sender, EventArgs e)
        {
            int selection = -1;
            if (dgv.SelectedRows.Count > 0)
                selection = dgv.SelectedRows[0].Index;

            subjectBO.DeleteSubject(current);

            ReloadGridView();
            if (selection != -1 && dgv.Rows.Count > 0)
            {
                try
                {
                    dgv.Rows[selection].Selected = true;
                }
                catch
                {
                    try
                    {
                        dgv.Rows[selection - 1].Selected = true;
                    }
                    catch
                    { }
                }
            }
            if (dgv.SelectedRows.Count > 0)
            dgv.FirstDisplayedScrollingRowIndex = dgv.SelectedRows[0].Index;
        }

        private void ChangeButtonClick(object sender, EventArgs e)
        {
            if (GrabData())
            {
                subjectBO.UpdateSubject(current);
                ReloadGridView(true);
            }
        }

        private void группыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new GroupForm();
        }

        private void студентыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new StudentForm();
        }

        private void преподавателиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new LecturerForm();
        }

        private void dgv_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgv.Columns[e.ColumnIndex].DataPropertyName == "attestationTypeSubject")
            {
                if (e.Value.ToString() == "exam")
                    e.Value = "экзамен";
                else
                    e.Value = "зачет";
            }
            if (dgv.Columns[e.ColumnIndex].DataPropertyName == "testTypeSubject")
            {
                if (e.Value.ToString() == "oral")
                    e.Value = "устно";
                else
                    e.Value = "письменно";
            }
            if (dgv.Columns[e.ColumnIndex].DataPropertyName == "creditTypeSubject")
            {
                if (e.Value.ToString() == "dif")
                    e.Value = "диф";
                else
                    e.Value = "не диф";
            }
            if (dgv.Columns[e.ColumnIndex].DataPropertyName == "markTypeSubject")
            {
                if (e.Value.ToString() == "5")
                    e.Value = "5";
                else if (e.Value.ToString() == "12")
                    e.Value = "12";
                else e.Value = "зачет";
            }
        }

        private void отделенияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new DepartmentForm();
        }

        private void закрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
