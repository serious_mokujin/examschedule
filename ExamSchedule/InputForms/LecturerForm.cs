﻿using ExamSchedule.BO;
using ExamSchedule.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExamSchedule.InputForms
{
    public partial class LecturerForm : Form, InputForm
    {
        private Form _next = null;
        public Form Next
        {
            get { return _next; }
            private set { _next = value; this.Close(); }
        }
        LecturerVO current;
        SubjectBO subjectBO;
        LecturerBO lecturerBO;
        
        public LecturerForm()
        {
            InitializeComponent();
            subjectBO = new SubjectBO();
            lecturerBO = new LecturerBO();
            current = new LecturerVO();
            ReloadAutocompleteData();
            ReloadGridView();
        }

        private void ReloadGridView(bool saveSelection = false)
        {
            int selection = -1;
            if (saveSelection)
                if (dgv.SelectedRows.Count > 0)
                    selection = dgv.SelectedRows[0].Index;
            dgv.DataSource = lecturerBO.GetRaw();
            dgv.Sort(dgv.Columns[0], ListSortDirection.Ascending);
            if (saveSelection)
                if (selection != -1 && dgv.Rows.Count > 0)
                    dgv.Rows[selection].Selected = true;

        }

        private void DataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                current = lecturerBO.GetLecturerWithId((int)dgv.SelectedRows[0].Cells[0].Value);
                LoadData(current);
                
            }
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            if (GrabData())
            {
                current.Id = -1;
                lecturerBO.UpdateLecturer(current);
                ReloadGridView();
                dgv.Rows[dgv.Rows.GetLastRow(DataGridViewElementStates.None)].Selected = true;
                if (dgv.SelectedRows.Count > 0)
                dgv.FirstDisplayedScrollingRowIndex = dgv.SelectedRows[0].Index;
            }
        }

        private void RemoveButtonClick(object sender, EventArgs e)
        {
            int selection = -1;
            if (dgv.SelectedRows.Count > 0)
                selection = dgv.SelectedRows[0].Index;

            lecturerBO.DeleteLecturer(current);

            ReloadGridView();
            if (selection != -1 && dgv.Rows.Count > 0)
            {
                try
                {
                    dgv.Rows[selection].Selected = true;
                }
                catch
                {
                    try
                    {
                        dgv.Rows[selection - 1].Selected = true;
                    }
                    catch
                    { }
                }
            }
            if (dgv.SelectedRows.Count > 0)
            dgv.FirstDisplayedScrollingRowIndex = dgv.SelectedRows[0].Index;
        }

        private void ChangeButtonClick(object sender, EventArgs e)
        {
            if (GrabData())
            {
                lecturerBO.UpdateLecturer(current);
                ReloadGridView(true);
            }
        }

        public bool GrabData()
        {
            current.Name = lecturerNameTextBox.Text;
            return true;
        }

        public void LoadData(LecturerVO data)
        {
            lecturerNameTextBox.Text = data.Name;
            listBox1.Items.Clear();
            var items = subjectBO.GetSubjectsByLecturer(data.Id);
            listBox1.Items.AddRange(items);
        }
        public void ReloadAutocompleteData() 
        {
            var subjects = subjectBO.GetSubjects();
            nameComboBox.Items.Clear();
            codeComboBox.Items.Clear();
            nameComboBox.Items.AddRange(subjects);
            codeComboBox.Items.AddRange(subjects);
        }

        private void addbutton_Click(object sender, EventArgs e)
        {
            if ((nameComboBox.SelectedItem == codeComboBox.SelectedItem) && nameComboBox.SelectedItem != null)
            {
                SubjectVO s = nameComboBox.SelectedItem as SubjectVO;
                foreach (var item in listBox1.Items)
                {
                    if (((SubjectVO)item).Id == s.Id)
                        return;
                }
                subjectBO.LinkWithLecturer(s, current);
                listBox1.Items.Add(s);
            }
        }

        private void deletebutton_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                subjectBO.DetachSubjectFromLecturer((SubjectVO)listBox1.SelectedItem, current);
                listBox1.Items.Remove(listBox1.SelectedItem);
            }
        }

        private void nameСomboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (codeComboBox.SelectedIndex != nameComboBox.SelectedIndex)
            {
                codeComboBox.SelectedIndex = nameComboBox.SelectedIndex;
            }
        }

        private void codeComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (nameComboBox.SelectedIndex != codeComboBox.SelectedIndex)
            {
                nameComboBox.SelectedIndex = codeComboBox.SelectedIndex;
            }
        }

        private void предметыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new SubjectForm();
        }

        private void студентыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new StudentForm();
        }

        private void группыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new GroupForm();
        }

        private void отделенияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new DepartmentForm();
        }

        private void закрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
