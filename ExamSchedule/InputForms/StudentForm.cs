﻿using ExamSchedule.BO;
using ExamSchedule.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExamSchedule.InputForms
{
    public partial class StudentForm : Form, InputForm
    {
        private Form _next = null;
        public Form Next
        {
            get { return _next; }
            private set { _next = value; this.Close(); }
        }
        StudentVO current;
        StudentBO studentBO;
        GroupBO groupBO;

        public StudentForm()
        {
            InitializeComponent();
            studentBO = new StudentBO();
            groupBO = new GroupBO();
            current = new StudentVO();
            ReloadAutocompleteData();
            ReloadGridView();
        }
        void ReloadGridView(bool saveSelection = false)
        {
            int selection = -1;
            if(saveSelection)
                if (dgv.SelectedRows.Count > 0)
                    selection = dgv.SelectedRows[0].Index;
            if (nameSearchToolStripTextBox1.Text != "")
                if (filterGroupToolStripComboBox.SelectedItem != null && filterGroupToolStripComboBox.SelectedIndex != 0)
                    dgv.DataSource =
                        studentBO.GetRawWithinGroupSearchName((GroupVO) filterGroupToolStripComboBox.SelectedItem,
                                                              nameSearchToolStripTextBox1.Text);
                else
                    dgv.DataSource = studentBO.GetRawSearchName(nameSearchToolStripTextBox1.Text);
            else
            {
                if (filterGroupToolStripComboBox.SelectedItem != null && filterGroupToolStripComboBox.SelectedIndex != 0)
                    dgv.DataSource =
                        studentBO.GetRawWithinGroup((GroupVO) filterGroupToolStripComboBox.SelectedItem);
                else
                    dgv.DataSource = studentBO.GetRaw();
            }
            
            dgv.Sort(dgv.Columns[0], ListSortDirection.Ascending);
            if(saveSelection)
                if (selection != -1 && dgv.Rows.Count > 0)
                    dgv.Rows[selection].Selected = true;
            
        }
        private void StudentDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                current = studentBO.GetStudentWithId((int)dgv.SelectedRows[0].Cells[0].Value);
                LoadData(current);
                
            }
        }

        public bool GrabData()
        {
            current.Name = studentNameTextBox.Text;
            current.RegisterNumber = (int)regnumNumericUpDown.Value;
            if (groupComboBox.SelectedItem != null)
                current.IdGroup = ((GroupVO)groupComboBox.SelectedItem).Id;   
            else
            {
                controlsWithErrors.Add(groupComboBox);
                timer1.Enabled = true;
                return false;
            }
            return true;
        }

        public void LoadData(StudentVO data)
        {
            studentNameTextBox.Text = data.Name;
            foreach (GroupVO item in groupComboBox.Items)
            {
                if (item.Id == data.IdGroup)
                {
                    groupComboBox.SelectedItem = item;
                    break;
                }
            }
            regnumNumericUpDown.Value = data.RegisterNumber;
        }

        public void ReloadAutocompleteData()
        {
            var groups = groupBO.GetNotGraduatedGroups();
            groupComboBox.Items.Clear();
            groupComboBox.Items.AddRange(groups);

            filterGroupToolStripComboBox.Items.Clear();
            filterGroupToolStripComboBox.Items.Add("-- все --");
            filterGroupToolStripComboBox.Items.AddRange(groups);
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            if (GrabData())
            {
                current.Id = -1;
                studentBO.UpdateStudent(current);
                ReloadGridView();
                dgv.Rows[dgv.Rows.GetLastRow(DataGridViewElementStates.None)].Selected = true;
                if (dgv.SelectedRows.Count > 0)
                dgv.FirstDisplayedScrollingRowIndex = dgv.SelectedRows[0].Index;
            }
        }

        private void RemoveButtonClick(object sender, EventArgs e)
        {
            int selection = -1;
            if (dgv.SelectedRows.Count > 0)
                selection = dgv.SelectedRows[0].Index;
            
            studentBO.DeleteStudent(current);

            ReloadGridView();
            if (selection != -1 && dgv.Rows.Count > 0)
            {
                try
                {
                    dgv.Rows[selection].Selected = true;
                }
                catch
                {
                    try
                    {
                        dgv.Rows[selection - 1].Selected = true;
                    }
                    catch
                    { }
                }
            }
            if (dgv.SelectedRows.Count > 0)
                if (dgv.SelectedRows.Count > 0)
            dgv.FirstDisplayedScrollingRowIndex = dgv.SelectedRows[0].Index;
        }

        private void ChangeButtonClick(object sender, EventArgs e)
        {
            if (GrabData())
            {
                studentBO.UpdateStudent(current);
                ReloadGridView(true);
            }
        }

        List<Control> controlsWithErrors = new List<Control>();
        bool _period = false;
        int _counter = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            _period = !_period;
            foreach (var item in controlsWithErrors)
            {
                if (_period)
                    item.BackColor = Color.Red;
                else
                    item.BackColor = SystemColors.Control;
            }
            _counter++;
            if (_counter > 5)
            {
                _counter = 0;
                _period = false;
                timer1.Enabled = false;
                controlsWithErrors.Clear();
            }
        }

        private void группыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new GroupForm();
        }

        private void преподавателиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new LecturerForm();
        }

        private void предметыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new SubjectForm();
        }

        private void SearchToolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            reloadTimer.Stop();
            reloadTimer.Enabled = true;
        }

        private void reloadTimer_Tick(object sender, EventArgs e)
        {
            reloadTimer.Enabled = false;
            ReloadGridView();
        }

        private void filterGroupToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReloadGridView();
        }

        private void сбросToolStripMenuItem_Click(object sender, EventArgs e)
        {
            filterGroupToolStripComboBox.SelectedIndex = 0;
            nameSearchToolStripTextBox1.Text = "";
            ReloadGridView();
        }

        private void отделенияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new DepartmentForm();
        }

        private void закрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
