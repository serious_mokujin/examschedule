﻿namespace ExamSchedule.InputForms
{
    partial class GroupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GroupForm));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.idGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.graduationStatusColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.таблицаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.перейтиКToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.студентіToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.преподавателиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.предметыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отделенияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.закрытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.действияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.повыситьКурсToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отображатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showGraduatedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filterDepartmentToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.сбросToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AddButton = new System.Windows.Forms.Button();
            this.ChangeButton = new System.Windows.Forms.Button();
            this.RemoveButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.postfixTextBox = new System.Windows.Forms.TextBox();
            this.courseNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.numberNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.yearNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.departmentComboBox = new System.Windows.Forms.ComboBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.courseNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idGroup,
            this.nameGroup,
            this.graduationStatusColumn});
            this.dgv.Location = new System.Drawing.Point(0, 33);
            this.dgv.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 24;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(544, 357);
            this.dgv.TabIndex = 1;
            this.dgv.SelectionChanged += new System.EventHandler(this.DataGridView_SelectionChanged);
            // 
            // idGroup
            // 
            this.idGroup.DataPropertyName = "idGroup";
            this.idGroup.HeaderText = "ID";
            this.idGroup.Name = "idGroup";
            this.idGroup.ReadOnly = true;
            // 
            // nameGroup
            // 
            this.nameGroup.DataPropertyName = "nameGroup";
            this.nameGroup.HeaderText = "Название";
            this.nameGroup.Name = "nameGroup";
            this.nameGroup.ReadOnly = true;
            this.nameGroup.Width = 200;
            // 
            // graduationStatusColumn
            // 
            this.graduationStatusColumn.DataPropertyName = "graduated";
            this.graduationStatusColumn.HeaderText = "Выпуск";
            this.graduationStatusColumn.Name = "graduationStatusColumn";
            this.graduationStatusColumn.ReadOnly = true;
            this.graduationStatusColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.graduationStatusColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.таблицаToolStripMenuItem,
            this.действияToolStripMenuItem,
            this.отображатьToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(544, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // таблицаToolStripMenuItem
            // 
            this.таблицаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.перейтиКToolStripMenuItem,
            this.закрытьToolStripMenuItem});
            this.таблицаToolStripMenuItem.Name = "таблицаToolStripMenuItem";
            this.таблицаToolStripMenuItem.Size = new System.Drawing.Size(95, 24);
            this.таблицаToolStripMenuItem.Text = "Таблица";
            // 
            // перейтиКToolStripMenuItem
            // 
            this.перейтиКToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.студентіToolStripMenuItem,
            this.преподавателиToolStripMenuItem,
            this.предметыToolStripMenuItem,
            this.отделенияToolStripMenuItem});
            this.перейтиКToolStripMenuItem.Name = "перейтиКToolStripMenuItem";
            this.перейтиКToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.перейтиКToolStripMenuItem.Text = "Перейти к";
            // 
            // студентіToolStripMenuItem
            // 
            this.студентіToolStripMenuItem.Name = "студентіToolStripMenuItem";
            this.студентіToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.студентіToolStripMenuItem.Text = "Студенты";
            this.студентіToolStripMenuItem.Click += new System.EventHandler(this.студентіToolStripMenuItem_Click);
            // 
            // преподавателиToolStripMenuItem
            // 
            this.преподавателиToolStripMenuItem.Name = "преподавателиToolStripMenuItem";
            this.преподавателиToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.преподавателиToolStripMenuItem.Text = "Преподаватели";
            this.преподавателиToolStripMenuItem.Click += new System.EventHandler(this.преподавателиToolStripMenuItem_Click);
            // 
            // предметыToolStripMenuItem
            // 
            this.предметыToolStripMenuItem.Name = "предметыToolStripMenuItem";
            this.предметыToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.предметыToolStripMenuItem.Text = "Предметы";
            this.предметыToolStripMenuItem.Click += new System.EventHandler(this.предметыToolStripMenuItem_Click);
            // 
            // отделенияToolStripMenuItem
            // 
            this.отделенияToolStripMenuItem.Name = "отделенияToolStripMenuItem";
            this.отделенияToolStripMenuItem.Size = new System.Drawing.Size(214, 24);
            this.отделенияToolStripMenuItem.Text = "Отделения";
            this.отделенияToolStripMenuItem.Click += new System.EventHandler(this.отделенияToolStripMenuItem_Click);
            // 
            // закрытьToolStripMenuItem
            // 
            this.закрытьToolStripMenuItem.Name = "закрытьToolStripMenuItem";
            this.закрытьToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.закрытьToolStripMenuItem.Text = "Закрыть";
            this.закрытьToolStripMenuItem.Click += new System.EventHandler(this.закрытьToolStripMenuItem_Click);
            // 
            // действияToolStripMenuItem
            // 
            this.действияToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.повыситьКурсToolStripMenuItem});
            this.действияToolStripMenuItem.Name = "действияToolStripMenuItem";
            this.действияToolStripMenuItem.Size = new System.Drawing.Size(103, 24);
            this.действияToolStripMenuItem.Text = "Действия";
            // 
            // повыситьКурсToolStripMenuItem
            // 
            this.повыситьКурсToolStripMenuItem.Name = "повыситьКурсToolStripMenuItem";
            this.повыситьКурсToolStripMenuItem.Size = new System.Drawing.Size(209, 24);
            this.повыситьКурсToolStripMenuItem.Text = "Повысить курс";
            this.повыситьКурсToolStripMenuItem.Click += new System.EventHandler(this.повыситьКурсToolStripMenuItem_Click);
            // 
            // отображатьToolStripMenuItem
            // 
            this.отображатьToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showGraduatedToolStripMenuItem,
            this.filterDepartmentToolStripComboBox,
            this.сбросToolStripMenuItem});
            this.отображатьToolStripMenuItem.Name = "отображатьToolStripMenuItem";
            this.отображатьToolStripMenuItem.Size = new System.Drawing.Size(100, 24);
            this.отображатьToolStripMenuItem.Text = "Фильтры";
            // 
            // showGraduatedToolStripMenuItem
            // 
            this.showGraduatedToolStripMenuItem.CheckOnClick = true;
            this.showGraduatedToolStripMenuItem.Name = "showGraduatedToolStripMenuItem";
            this.showGraduatedToolStripMenuItem.Size = new System.Drawing.Size(293, 24);
            this.showGraduatedToolStripMenuItem.Text = "Отображать выпущеные";
            this.showGraduatedToolStripMenuItem.CheckedChanged += new System.EventHandler(this.отображатьВыпущеныеToolStripMenuItem_CheckedChanged);
            // 
            // filterDepartmentToolStripComboBox
            // 
            this.filterDepartmentToolStripComboBox.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.filterDepartmentToolStripComboBox.Name = "filterDepartmentToolStripComboBox";
            this.filterDepartmentToolStripComboBox.Size = new System.Drawing.Size(121, 28);
            this.filterDepartmentToolStripComboBox.Text = "Отделение";
            this.filterDepartmentToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.filterDepartmentToolStripComboBox_SelectedIndexChanged);
            // 
            // сбросToolStripMenuItem
            // 
            this.сбросToolStripMenuItem.Name = "сбросToolStripMenuItem";
            this.сбросToolStripMenuItem.Size = new System.Drawing.Size(293, 24);
            this.сбросToolStripMenuItem.Text = "Сброс";
            this.сбросToolStripMenuItem.Click += new System.EventHandler(this.сбросToolStripMenuItem_Click);
            // 
            // AddButton
            // 
            this.AddButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.AddButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.AddButton.Location = new System.Drawing.Point(12, 456);
            this.AddButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(168, 39);
            this.AddButton.TabIndex = 33;
            this.AddButton.Text = "Добавить";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButtonClick);
            // 
            // ChangeButton
            // 
            this.ChangeButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ChangeButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ChangeButton.Location = new System.Drawing.Point(186, 456);
            this.ChangeButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ChangeButton.Name = "ChangeButton";
            this.ChangeButton.Size = new System.Drawing.Size(168, 39);
            this.ChangeButton.TabIndex = 34;
            this.ChangeButton.Text = "Изменить";
            this.ChangeButton.UseVisualStyleBackColor = true;
            this.ChangeButton.Click += new System.EventHandler(this.ChangeButtonClick);
            // 
            // RemoveButton
            // 
            this.RemoveButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.RemoveButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RemoveButton.Location = new System.Drawing.Point(360, 456);
            this.RemoveButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RemoveButton.Name = "RemoveButton";
            this.RemoveButton.Size = new System.Drawing.Size(168, 39);
            this.RemoveButton.TabIndex = 35;
            this.RemoveButton.Text = "Удалить";
            this.RemoveButton.UseVisualStyleBackColor = true;
            this.RemoveButton.Click += new System.EventHandler(this.RemoveButtonClick);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(42, 395);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 20);
            this.label5.TabIndex = 38;
            this.label5.Text = "Отделение";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(397, 395);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 20);
            this.label6.TabIndex = 36;
            this.label6.Text = "Постфикс";
            // 
            // postfixTextBox
            // 
            this.postfixTextBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.postfixTextBox.Location = new System.Drawing.Point(401, 420);
            this.postfixTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.postfixTextBox.MaxLength = 1;
            this.postfixTextBox.Name = "postfixTextBox";
            this.postfixTextBox.Size = new System.Drawing.Size(88, 28);
            this.postfixTextBox.TabIndex = 37;
            // 
            // courseNumericUpDown
            // 
            this.courseNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.courseNumericUpDown.Location = new System.Drawing.Point(152, 420);
            this.courseNumericUpDown.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.courseNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.courseNumericUpDown.Name = "courseNumericUpDown";
            this.courseNumericUpDown.Size = new System.Drawing.Size(77, 28);
            this.courseNumericUpDown.TabIndex = 45;
            this.courseNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(153, 395);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 44;
            this.label1.Text = "Курс";
            // 
            // numberNumericUpDown
            // 
            this.numberNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.numberNumericUpDown.Location = new System.Drawing.Point(235, 420);
            this.numberNumericUpDown.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numberNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numberNumericUpDown.Name = "numberNumericUpDown";
            this.numberNumericUpDown.Size = new System.Drawing.Size(77, 28);
            this.numberNumericUpDown.TabIndex = 47;
            this.numberNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(231, 395);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 46;
            this.label2.Text = "Номер";
            // 
            // yearNumericUpDown
            // 
            this.yearNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.yearNumericUpDown.Location = new System.Drawing.Point(318, 420);
            this.yearNumericUpDown.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.yearNumericUpDown.Name = "yearNumericUpDown";
            this.yearNumericUpDown.Size = new System.Drawing.Size(77, 28);
            this.yearNumericUpDown.TabIndex = 49;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(314, 395);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 20);
            this.label3.TabIndex = 48;
            this.label3.Text = "Год";
            // 
            // departmentComboBox
            // 
            this.departmentComboBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.departmentComboBox.DisplayMember = "ShortForm";
            this.departmentComboBox.FormattingEnabled = true;
            this.departmentComboBox.Location = new System.Drawing.Point(46, 420);
            this.departmentComboBox.Name = "departmentComboBox";
            this.departmentComboBox.Size = new System.Drawing.Size(100, 28);
            this.departmentComboBox.TabIndex = 50;
            this.departmentComboBox.ValueMember = "Id";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // GroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 508);
            this.Controls.Add(this.departmentComboBox);
            this.Controls.Add(this.yearNumericUpDown);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numberNumericUpDown);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.courseNumericUpDown);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.postfixTextBox);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.ChangeButton);
            this.Controls.Add(this.RemoveButton);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.dgv);
            this.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "GroupForm";
            this.Text = "Группы";
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.courseNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numberNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem таблицаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem перейтиКToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem закрытьToolStripMenuItem;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button ChangeButton;
        private System.Windows.Forms.Button RemoveButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox postfixTextBox;
        private System.Windows.Forms.ToolStripMenuItem студентіToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem преподавателиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem предметыToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown courseNumericUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numberNumericUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown yearNumericUpDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox departmentComboBox;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem действияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem повыситьКурсToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отображатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showGraduatedToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox filterDepartmentToolStripComboBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn idGroup;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameGroup;
        private System.Windows.Forms.DataGridViewCheckBoxColumn graduationStatusColumn;
        private System.Windows.Forms.ToolStripMenuItem сбросToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отделенияToolStripMenuItem;
    }
}