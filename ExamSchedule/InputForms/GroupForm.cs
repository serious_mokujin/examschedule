﻿using ExamSchedule.BO;
using ExamSchedule.VO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExamSchedule.InputForms
{
    public partial class GroupForm : Form, InputForm
    {
        private Form _next = null;
        public Form Next {
            get { return _next; }
            private set { _next = value; this.Close(); }
        }

        private GroupVO current;
        private GroupBO groupBO;
        private DepartmentBO departmentBO;

        public GroupForm()
        {
            InitializeComponent();
            
            dgv.AutoGenerateColumns = false;
            groupBO = new GroupBO();
            current = new GroupVO();
            departmentBO = new DepartmentBO();
            ReloadGridView();
            ReloadAutocompleteData();
        }

        private void ReloadGridView(bool saveSelection = false)
        {
            int selection = -1;
            if (saveSelection)
                if (dgv.SelectedRows.Count > 0)
                    selection = dgv.SelectedRows[0].Index;
            if (showGraduatedToolStripMenuItem.Checked)
            {
                if (filterDepartmentToolStripComboBox.SelectedItem != null &&
                    filterDepartmentToolStripComboBox.SelectedIndex != 0)
                    dgv.DataSource =
                        groupBO.GetGroupsInDepartmentRaw((DepartmentVO) filterDepartmentToolStripComboBox.SelectedItem);
                else
                    dgv.DataSource = groupBO.GetRaw();
            }
            else
            {
                if (filterDepartmentToolStripComboBox.SelectedItem != null &&
                    filterDepartmentToolStripComboBox.SelectedIndex != 0)
                    dgv.DataSource =
                        groupBO.GetGroupsInDepartmentNotGraduatedRaw(
                            (DepartmentVO) filterDepartmentToolStripComboBox.SelectedItem);
                else
                    dgv.DataSource = groupBO.GetNotGraduatedGroupsRaw();
            }


            dgv.Sort(dgv.Columns[0], ListSortDirection.Ascending);
            if (saveSelection)
                if (selection != -1 && dgv.Rows.Count > 0)
                    dgv.Rows[selection].Selected = true;
        }

        private void DataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                current = groupBO.GetGroupWithId((int)dgv.SelectedRows[0].Cells[0].Value);
                LoadData(current);
                
            }
        }
        public void ReloadAutocompleteData()
        {
            departmentComboBox.Items.Clear();
            departmentComboBox.Items.AddRange(departmentBO.GetDepartments());

            filterDepartmentToolStripComboBox.Items.Clear();
            filterDepartmentToolStripComboBox.Items.Add("-- все --");
            filterDepartmentToolStripComboBox.Items.AddRange(departmentBO.GetDepartments());
            
        }
        public bool GrabData()
        {
            current.Course = (int) courseNumericUpDown.Value;
            current.Number = (int) numberNumericUpDown.Value;
            current.YearPostfix = (int) yearNumericUpDown.Value;
            current.CustomPostfix = (postfixTextBox.Text.Length > 0) ? postfixTextBox.Text[0] : ' ';
            if (departmentComboBox.SelectedItem != null)
                current.IdDepartment = ((DepartmentVO) departmentComboBox.SelectedItem).Id;
            else
            {
                controlsWithErrors.Add(departmentComboBox);
                timer1.Enabled = true;
                return false;
            }
            return true;
        }

        public void LoadData(GroupVO data)
        {
            postfixTextBox.Text = data.CustomPostfix.ToString();
            courseNumericUpDown.Value = Math.Min(Math.Max(data.Course, 1), 4);
            numberNumericUpDown.Value = Math.Min(Math.Max(data.Number, 1), 9);
            yearNumericUpDown.Value = Math.Min(Math.Max(data.YearPostfix, 0), 9);
            foreach (DepartmentVO item in departmentComboBox.Items)
            {
                if (item.Id == data.IdDepartment)
                {
                    departmentComboBox.SelectedItem = item;
                    break;
                }
            }
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            if (GrabData())
            {
                current.Id = -1;
                groupBO.UpdateGroup(current);
                ReloadGridView();
                dgv.Rows[dgv.Rows.GetLastRow(DataGridViewElementStates.None)].Selected = true;
                if (dgv.SelectedRows.Count > 0)
                dgv.FirstDisplayedScrollingRowIndex = dgv.SelectedRows[0].Index;
            }
        }

        private void RemoveButtonClick(object sender, EventArgs e)
        {
            int selection = -1;
            if (dgv.SelectedRows.Count > 0)
                selection = dgv.SelectedRows[0].Index;
            
            groupBO.DeleteGroup(current);

            ReloadGridView();
            if (selection != -1 && dgv.Rows.Count > 0)
            {
                try
                {
                    dgv.Rows[selection].Selected = true;
                }
                catch
                {
                    try
                    {
                        dgv.Rows[selection - 1].Selected = true;
                    }
                    catch
                    { }
                }
            }
            if (dgv.SelectedRows.Count > 0)
            dgv.FirstDisplayedScrollingRowIndex = dgv.SelectedRows[0].Index;
        }

        private void ChangeButtonClick(object sender, EventArgs e)
        {
            if (GrabData())
            {
                groupBO.UpdateGroup(current);
                ReloadGridView(true);
            }
        }

        private void студентіToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new StudentForm();
        }

        private void преподавателиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new LecturerForm();
        }

        private void предметыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new SubjectForm();
        }

        List<Control> controlsWithErrors = new List<Control>();
        bool _period = false;
        int _counter = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            _period = !_period;
            foreach (var item in controlsWithErrors)
            {
                if (_period)
                    item.BackColor = Color.Red;
                else
                    item.BackColor = SystemColors.Control;
            }
            _counter++;
            if (_counter > 5)
            {
                _counter = 0;
                _period = false;
                timer1.Enabled = false;
                controlsWithErrors.Clear();
            }
        }

        private void отображатьВыпущеныеToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            ReloadGridView(false);
        }

        private void filterDepartmentToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReloadGridView(false);
        }

        private void повыситьКурсToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GroupVO[] groups = groupBO.GetGroups();
            foreach (var groupVo in groups)
            {
                if (groupVo.Course == 4)
                    groupVo.Graduated = true;
                else
                    groupVo.Course++;
                groupBO.UpdateGroup(groupVo);
            }
            ReloadGridView(false);
        }

        private void сбросToolStripMenuItem_Click(object sender, EventArgs e)
        {
            showGraduatedToolStripMenuItem.Checked = false;
            filterDepartmentToolStripComboBox.SelectedIndex = 0;
        }

        private void закрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void отделенияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new DepartmentForm();
        }
    }
}
