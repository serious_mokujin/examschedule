﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ExamSchedule.BO;
using ExamSchedule.VO;

namespace ExamSchedule.InputForms
{
    public partial class DepartmentForm : Form, InputForm
    {
        DepartmentVO current;
        DepartmentBO departmentBO;
        LecturerBO lecturerBO;
        private Form _next = null;
        public Form Next
        {
            get { return _next; }
            private set { _next = value; this.Close(); }
        }
        public DepartmentForm()
        {
            InitializeComponent();
            dgv.AutoGenerateColumns = false;
            departmentBO = new DepartmentBO();
            lecturerBO = new LecturerBO();
            current = new DepartmentVO();
            ReloadAutocompleteData();
            ReloadGridView();
        }

        void ReloadGridView(bool saveSelection = false)
        {
            int selection = -1;
            if(saveSelection)
                if (dgv.SelectedRows.Count > 0)
                    selection = dgv.SelectedRows[0].Index;
            dgv.DataSource = departmentBO.GetRawWithNames();
            dgv.Sort(dgv.Columns[0], ListSortDirection.Ascending);
            if(saveSelection)
                if (selection != -1 && dgv.Rows.Count > 0)
                    dgv.Rows[selection].Selected = true;
            
        }
        private void DataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                current = departmentBO.GetDepartmentWithId((int)dgv.SelectedRows[0].Cells[0].Value);
                LoadData(current);
                
            }
        }

        public bool GrabData()
        {
            current.Name = departmentNameTextBox.Text;
            current.ShortForm = shortFormTextBox.Text;
            current.SpecializationName = specializationNameTextBox.Text;
            current.SpecializationCode = specializationCodeTextBox.Text;

            if (lecturerComboBox.SelectedItem != null)
                current.IdLecturer = ((LecturerVO)lecturerComboBox.SelectedItem).Id;
            else
            {
                controlsWithErrors.Add(lecturerComboBox);
                timer1.Enabled = true;
                return false;
            }
            return true;
        }

        public void LoadData(DepartmentVO data)
        {
            departmentNameTextBox.Text = data.Name;
            shortFormTextBox.Text = data.ShortForm;
            specializationNameTextBox.Text = data.SpecializationName;
            specializationCodeTextBox.Text = data.SpecializationCode;
            foreach (LecturerVO item in lecturerComboBox.Items)
            {
                if (item.Id == data.IdLecturer)
                {
                    lecturerComboBox.SelectedItem = item;
                    break;
                }
            }
        }

        public void ReloadAutocompleteData()
        {
            lecturerComboBox.Items.Clear();
            lecturerComboBox.Items.AddRange(lecturerBO.GetLecturers());
        }

        private void AddButtonClick(object sender, EventArgs e)
        {
            if (GrabData())
            {
                current.Id = -1;
                departmentBO.UpdateDepartment(current);
                ReloadGridView();
                dgv.Rows[dgv.Rows.GetLastRow(DataGridViewElementStates.None)].Selected = true;
                if (dgv.SelectedRows.Count > 0)
                dgv.FirstDisplayedScrollingRowIndex = dgv.SelectedRows[0].Index;
            }
        }

        private void RemoveButtonClick(object sender, EventArgs e)
        {
            int selection = -1;
            if (dgv.SelectedRows.Count > 0)
                selection = dgv.SelectedRows[0].Index;
            
            departmentBO.DeleteDepartment(current);

            ReloadGridView();
            if (selection != -1 && dgv.Rows.Count > 0)
            {
                try
                {
                    dgv.Rows[selection].Selected = true;
                }
                catch
                {
                    try
                    {
                        dgv.Rows[selection - 1].Selected = true;
                    }
                    catch
                    { }
                }
            }
            if (dgv.SelectedRows.Count > 0)
            dgv.FirstDisplayedScrollingRowIndex = dgv.SelectedRows[0].Index;
        }

        private void ChangeButtonClick(object sender, EventArgs e)
        {
            if (GrabData())
            {
                departmentBO.UpdateDepartment(current);
                ReloadGridView(true);
            }
        }

        List<Control> controlsWithErrors = new List<Control>();
        bool _period = false;
        int _counter = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            _period = !_period;
            foreach (var item in controlsWithErrors)
            {
                if (_period)
                    item.BackColor = Color.Red;
                else
                    item.BackColor = SystemColors.Control;
            }
            _counter++;
            if (_counter > 5)
            {
                _counter = 0;
                _period = false;
                timer1.Enabled = false;
                controlsWithErrors.Clear();
            }
        }

        private void группыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new GroupForm();
        }

        private void преподавателиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new LecturerForm();
        }

        private void предметыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new SubjectForm();
        }

        private void закрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void студентыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Next = new StudentForm();
        }
    }
}
