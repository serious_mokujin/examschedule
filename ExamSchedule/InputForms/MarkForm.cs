﻿using ExamSchedule.BO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ExamSchedule.VO;

namespace ExamSchedule
{
    public partial class MarkForm : Form
    {
        private SubjectBO subjectBO;
        private GroupBO groupBO;
        private LecturerBO lecturerBO;
        private StudentBO studentBO;
        private MarkBO markBO;
        private DepartmentBO departmentBO;

        private MarkVO current;

        public MarkForm(GroupVO group, SubjectVO subject, LecturerVO lecturer, DateTime date)
        {
            Initialize();
            foreach (object item in departmentComboBox.Items)
                if (item is DepartmentVO && (item as DepartmentVO).Id == group.IdDepartment)
                    departmentComboBox.SelectedItem = item;
            foreach (object item in groupComboBox.Items)
                if (item is GroupVO && (item as GroupVO).Id == group.Id)
                    groupComboBox.SelectedItem = item;
            foreach (object item in subjectNameComboBox.Items)
                if (item is SubjectVO && (item as SubjectVO).Id == subject.Id)
                {
                    subjectNameComboBox.SelectedItem = item;
                    subjectCodeComboBox.SelectedItem = item;
                }
            foreach (object item in lecturerComboBox.Items)
                if (item is LecturerVO && (item as LecturerVO).Id == lecturer.Id)
                    lecturerComboBox.SelectedItem = item;
            dateTimePicker1.Value = date.Date;
            dateTimePicker1.Enabled = false;
            departmentComboBox.Enabled = false;
            groupComboBox.Enabled = false;
            subjectCodeComboBox.Enabled = false;
            subjectNameComboBox.Enabled = false;
            lecturerComboBox.Enabled = false;
            UpdateData();
        }
        public MarkForm()
        {
            Initialize();
        }
        void Initialize()
        {
            InitializeComponent();
            dataGridView.AutoGenerateColumns = false;
            subjectBO = new SubjectBO();
            groupBO = new GroupBO();
            lecturerBO = new LecturerBO();
            studentBO = new StudentBO();
            departmentBO = new DepartmentBO();
            markBO = new MarkBO();
            current = new MarkVO();
            ReloadAutocomplete();
        }

        private void MarkForm_Load(object sender, EventArgs e)
        {

        }

        void UpdateData()
        {
            if (groupComboBox.SelectedItem != null && subjectNameComboBox.SelectedItem != null &&
                subjectCodeComboBox.SelectedItem != null)
                dataGridView.DataSource =
                    markBO.GetMarksFromGroupBySubjectWithNamesRaw(groupComboBox.SelectedItem as GroupVO,
                                                                  subjectNameComboBox.SelectedItem as SubjectVO);
        }

        private void ReloadAutocomplete()
        {
            departmentComboBox.Items.Clear();
            departmentComboBox.Items.Add("-- все --");
            departmentComboBox.Items.AddRange(departmentBO.GetDepartments());

            ReloadGroupComboBox();
            var subjects = subjectBO.GetSubjects();
            subjectNameComboBox.Items.Clear();
            subjectCodeComboBox.Items.Clear();
            subjectNameComboBox.Items.AddRange(subjects);
            subjectCodeComboBox.Items.AddRange(subjects);

            ReloadLecturerComboBox();
            ReloadStudentComboBoxes();
        }
        void ReloadStudentComboBoxes()
        {
            studentComboBox.Items.Clear();
            regComboBox.Items.Clear();
            StudentVO[] data;
            if (groupComboBox.SelectedItem != null)
                data = studentBO.GetStudentsFromGroup((GroupVO) groupComboBox.SelectedItem);
            else
                data = studentBO.GetStudents();
            studentComboBox.Items.AddRange(data);
            regComboBox.Items.AddRange(data);
        }
        void ReloadGroupComboBox()
        {
            GroupVO[] data;
            if (departmentComboBox.SelectedItem != null && departmentComboBox.SelectedIndex != 0)
                data = groupBO.GetGroupsInDepartment((DepartmentVO)departmentComboBox.SelectedItem);
            else
                data = groupBO.GetNotGraduatedGroups();
            groupComboBox.Items.Clear();
            groupComboBox.Items.AddRange(data);

        }
        void ReloadLecturerComboBox()
        {
            LecturerVO[] data;
            if (subjectNameComboBox.SelectedItem != null)
                data = lecturerBO.GetLecturersBySubject((SubjectVO) subjectNameComboBox.SelectedItem);
            else
                data = lecturerBO.GetLecturers();
            lecturerComboBox.Items.Clear();
            lecturerComboBox.Items.AddRange(data);
        }
        void ReloadMarkComboBox()
        {
            markComboBox.Items.Clear();
            if (subjectNameComboBox.SelectedItem != null)
            {
                SubjectVO subject = (SubjectVO) subjectNameComboBox.SelectedItem;
                if (subject.MarkType == SubjectVO.MarkT._5)
                    markComboBox.Items.AddRange(new object[] {2, 3, 4, 5});
                else if (subject.MarkType == SubjectVO.MarkT._12)
                    markComboBox.Items.AddRange(new object[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12});
            }

        }

        private void nameСomboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (subjectCodeComboBox.SelectedIndex != subjectNameComboBox.SelectedIndex)
                subjectCodeComboBox.SelectedIndex = subjectNameComboBox.SelectedIndex;
            UpdateData();
            ReloadLecturerComboBox();
            ReloadMarkComboBox();
        }
        private void codeComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (subjectNameComboBox.SelectedIndex != subjectCodeComboBox.SelectedIndex)
                subjectNameComboBox.SelectedIndex = subjectCodeComboBox.SelectedIndex;
            UpdateData();
            ReloadLecturerComboBox();
            ReloadMarkComboBox();
        }
        private void studentNameСomboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (studentComboBox.SelectedIndex != regComboBox.SelectedIndex)
                regComboBox.SelectedIndex = studentComboBox.SelectedIndex;
            
        }
        private void studentRegComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (regComboBox.SelectedIndex != studentComboBox.SelectedIndex)
                studentComboBox.SelectedIndex = regComboBox.SelectedIndex;
        }
        private void departmentComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReloadGroupComboBox();
        }
        private void groupComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateData();
            ReloadStudentComboBoxes();
        }


        private void InsertButton_Click(object sender, EventArgs e)
        {
            if (GrabData())
            {
                if (
                    !markBO.MarkExists(studentComboBox.SelectedItem as StudentVO,
                                       subjectNameComboBox.SelectedItem as SubjectVO,
                                       lecturerComboBox.SelectedItem as LecturerVO))
                {
                    current.Id = -1;
                    markBO.UpdateMark(current);
                    UpdateData();
                }
                if (dataGridView.SelectedRows.Count > 0)
                dataGridView.FirstDisplayedScrollingRowIndex = dataGridView.SelectedRows[0].Index;
            }
        }

        public bool GrabData()
        {
            controlsWithErrors.Clear();
            if (studentComboBox.SelectedItem == null)
                controlsWithErrors.Add(studentComboBox);
            if (regComboBox.SelectedItem == null)
                controlsWithErrors.Add(regComboBox);
            if (subjectNameComboBox.SelectedItem == null)
                controlsWithErrors.Add(subjectNameComboBox);
            if (subjectCodeComboBox.SelectedItem == null)
                controlsWithErrors.Add(subjectCodeComboBox);
            if (lecturerComboBox.SelectedItem == null)
                controlsWithErrors.Add(lecturerComboBox);
            if(markComboBox.SelectedItem == null)
                controlsWithErrors.Add(markComboBox);
            if (controlsWithErrors.Count > 0)
            {
                timer1.Enabled = true;
                return false;
            }
            current.Mark = markComboBox.SelectedItem.ToString() == "зачет"
                               ? 0
                               : int.Parse(markComboBox.SelectedItem.ToString());
            current.Date = dateTimePicker1.Value.Date;
            current.IdLecturer = (lecturerComboBox.SelectedItem as LecturerVO).Id;
            current.IdStudent = (studentComboBox.SelectedItem as StudentVO).Id;
            current.IdSubject = (subjectNameComboBox.SelectedItem as SubjectVO).Id;
            
            
            return true;
        }

        public void LoadData(MarkVO data)
        {
            foreach (object item in subjectNameComboBox.Items)
                if (item is SubjectVO && (item as SubjectVO).Id == data.IdSubject)
                {
                    subjectNameComboBox.SelectedItem = item;
                    subjectCodeComboBox.SelectedItem = item;
                }
            foreach (object item in lecturerComboBox.Items)
                if (item is LecturerVO && (item as LecturerVO).Id == data.IdLecturer)
                    lecturerComboBox.SelectedItem = item;
            foreach (object item in studentComboBox.Items)
                if (item is StudentVO && (item as StudentVO).Id == data.IdStudent)
                {
                    studentComboBox.SelectedItem = item;
                    regComboBox.SelectedItem = item;
                }
            if (data.Mark == 0) markComboBox.SelectedIndex = 0;
            else markComboBox.Text = data.Mark.ToString();
        }
        List<Control> controlsWithErrors = new List<Control>();
        bool _period = false;
        int _counter = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            _period = !_period;
            foreach (var item in controlsWithErrors)
            {
                if (_period)
                    item.BackColor = Color.Red;
                else
                    item.BackColor = SystemColors.Control;
            }
            _counter++;
            if (_counter > 5)
            {
                _counter = 0;
                _period = false;
                timer1.Enabled = false;
                controlsWithErrors.Clear();
            }
        }

        private void DataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count > 0)
            {
                current = markBO.GetMarkWithId((int)dataGridView.SelectedRows[0].Cells[0].Value);
                LoadData(current);
                
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count > 0)
            {
                markBO.DeleteMark(current);
                UpdateData();
                if (dataGridView.SelectedRows.Count > 0)
                dataGridView.FirstDisplayedScrollingRowIndex = dataGridView.SelectedRows[0].Index;
            }
        }

        /*
        
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count > 0)
            {
                IDataStructure s = new Mark(dataGridView.SelectedRows[0].Cells);
                G.databaseConnection.Open();
                s.Delete();
                G.databaseConnection.Close();
                buffer.Reload(dataGridView);
            }
        }
         * */
    }
}
