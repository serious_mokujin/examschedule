﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ExamSchedule
{
    public static class Server
    {
        private static MySqlConnection _connection;
        public static MySqlConnection Connection { get { return _connection; } }
        public static bool EstablishConnection(string server, string user, string port, string password)
        {
            if (_connection != null)
                _connection.Dispose();
            _connection = new MySqlConnection(string.Format("server={0};user={1};port={2};password={3};", server, user, port, password));
            bool result = false;
            try
            {
                _connection.Open();
                result = _connection.Ping();
                _connection.Close();
            }
            catch (MySqlException e)
            {
#if DEBUG
                Trace.TraceError("Connection to server failed: {0}", e.Message);
#endif
                return false;
            }
            return result;
        }
        public static bool CheckDatabase(string database)
        {
            bool result = false;
            try
            {
                _connection.Open();
                string s0 = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'ExamSchedule';";
                MySqlCommand cmd = new MySqlCommand(s0, _connection);
                string res = (string)cmd.ExecuteScalar();
                result = res == database.ToLower();
                _connection.Close();
            }
            catch (MySqlException e)
            {
#if DEBUG
                Trace.TraceError("Checking database failed: {0}", e.Message);
#endif
                return false;
            }
            return result;
        }

        public static bool CreateDatabase(string creationScriptFilename)
        {
            try
            {
                string script;
                try
                {
                    script = File.ReadAllText(creationScriptFilename);
                }
                catch
                {
                    MessageBox.Show(creationScriptFilename+" file not found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                _connection.Open();

                MySqlCommand cmd = new MySqlCommand(script, _connection);
                cmd.ExecuteNonQuery();
                _connection.Close();
                return true;
            }
            catch (MySqlException e)
            {
                _connection.Close();
#if DEBUG
                Trace.TraceError("Creation database failed: {0}", e.Message);
#endif
                return false;
            }
        }
        public static bool DropDatabase(string databaseName)
        {
            try
            {
                _connection.Open();

                MySqlCommand cmd = new MySqlCommand("drop schema `"+databaseName+"`;", _connection);
                cmd.ExecuteNonQuery();
                _connection.Close();
                return true;
            }
            catch (MySqlException e)
            {
                _connection.Close();
#if DEBUG
                Trace.TraceError("Drop database failed: {0}", e.Message);
#endif
                return false;
            }
        }

        public static void DisposeConnection()
        {
            _connection.Dispose();
            _connection = null;
        }
    }
}
