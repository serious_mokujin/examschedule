﻿using System.IO;
using ExamSchedule.BO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ExamSchedule.VO;

namespace ExamSchedule
{
    public partial class SheetBuildForm : Form
    {
        GroupBO groupBO;
        SubjectBO subjectBO;
        LecturerBO lecturerBO;
        StudentBO studentBO;
        private DepartmentBO departmentBO;

        public SheetBuildForm()
        {
            InitializeComponent();
            groupBO = new GroupBO();
            subjectBO = new SubjectBO();
            lecturerBO = new LecturerBO();
            studentBO = new StudentBO();
            departmentBO = new DepartmentBO();
            ReloadAutocomplete();
        }
        private void ReloadAutocomplete()
        {
            departmentComboBox.Items.Clear();
            departmentComboBox.Items.AddRange(departmentBO.GetDepartments());

            groupComboBox.Items.Clear();
            groupComboBox.Items.AddRange(groupBO.GetNotGraduatedGroups());

            var subjects = subjectBO.GetSubjects();
            subjectNameComboBox.Items.Clear();
            subjectCodeComboBox.Items.Clear();
            subjectNameComboBox.Items.AddRange(subjects);
            subjectCodeComboBox.Items.AddRange(subjects);

            lecturerComboBox.Items.Clear();
            lecturerComboBox.Items.AddRange(lecturerBO.GetLecturers());
        }

        private void nameСomboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (subjectCodeComboBox.SelectedIndex != subjectNameComboBox.SelectedIndex)
                subjectCodeComboBox.SelectedIndex = subjectNameComboBox.SelectedIndex;
        }

        private void codeComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (subjectNameComboBox.SelectedIndex != subjectCodeComboBox.SelectedIndex)
                subjectNameComboBox.SelectedIndex = subjectCodeComboBox.SelectedIndex;
        }

        private void departmentComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (departmentComboBox.SelectedItem != null)
            {
                groupComboBox.Items.Clear();
                groupComboBox.Items.AddRange(groupBO.GetGroupsInDepartment((DepartmentVO)departmentComboBox.SelectedItem));
            }
        }

        private void makeReportButton_Click(object sender, EventArgs e)
        {
            if (groupComboBox.SelectedItem == null)
            {
                wrongComponents.Add(groupComboBox);
                timer1.Enabled = true;
                return;
            }
            CreateReport();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CreateReport(true);
        }
        void CreateReport(bool design = false)
        {
            using (FastReport.Report report = new FastReport.Report())
            {
                if (design)
                    report.Load("../../Reports/SheetTemplate.frx");
                else
                    report.Load("Reports/SheetTemplate.frx");
                report.AutoFillDataSet = true;
                var selectedGroup = (GroupVO)groupComboBox.SelectedItem;
                var t = studentBO.GetStudentsFromGroupRaw(selectedGroup.Id);

                report.RegisterData(t, "Student");
                report.GetDataSource("Student").Enabled = true;
                report.SetParameterValue("registerValues", checkBox1.Checked);
                report.SetParameterValue("course", Helper.ToRoman(selectedGroup.Course));
                report.SetParameterValue("semestr", Helper.ToRoman((selectedGroup.Course - 1) * 2 + (sem1RadioButton.Checked ? 1 : 2)));
                report.SetParameterValue("group", groupComboBox.Text);
                report.SetParameterValue("lecturer", lecturerCheckBox.Checked ? lecturerComboBox.Text : "");
                report.SetParameterValue("subject", subjectCheckBox.Checked ? subjectNameComboBox.Text : "");
                report.SetParameterValue("date", dateTimePicker.Value);
                report.SetParameterValue("printDate", dateCheckBox.Checked);
                if (design)
                {
                    report.Design();
                    File.Copy("../../Reports/SheetTemplate.frx", "Reports/SheetTemplate.frx", true);
                }
                else
                    report.Show();
            }
        }

        private int _counter = 0;
        private bool _period = false;
        private List<Control> wrongComponents = new List<Control>(); 
        private void timer1_Tick(object sender, EventArgs e)
        {
            _period = !_period;
            foreach (var wrongComponent in wrongComponents)
            {
                if (_period)
                    wrongComponent.BackColor = Color.Red;
                else
                    wrongComponent.BackColor = SystemColors.Window;
            }
            _counter++;
            if (_counter > 5)
            {
                wrongComponents.Clear();
                _counter = 0;
                _period = false;
                timer1.Enabled = false;
            }
        }

        private void subjectCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            subjectNameComboBox.Enabled = subjectCheckBox.Checked;
            subjectCodeComboBox.Enabled = subjectCheckBox.Checked;
        }

        private void lecturerCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            lecturerComboBox.Enabled = lecturerCheckBox.Checked;
        }

        private void dateCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePicker.Enabled = dateCheckBox.Checked;
        }
    }
}
