﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ExamSchedule.BO;
using ExamSchedule.OtherForms.CreateSessionMaster;
using ExamSchedule.VO;

namespace ExamSchedule.OtherForms
{
    public partial class SessionReviewForm : Form
    {
        private SessionVO session;
        private SessionBO sessionBO;
        private GroupBO groupBO;
        private SubjectBO subjectBO;
        private StudentBO studentBO;
        private DepartmentBO departmentBO;

        private ExamBO examBO;
        private Form _next;
        private LecturerBO lecturerBO;
        public Form Next { get { return _next; } }

        private bool winterSession;
        private string years;


        public SessionReviewForm(SessionVO session)
        {
            InitializeComponent();
            dgv.AutoGenerateColumns = false;
            sessionBO = new SessionBO();
            examBO = new ExamBO();
            groupBO = new GroupBO();
            subjectBO = new SubjectBO();
            lecturerBO = new LecturerBO();
            studentBO = new StudentBO();
            departmentBO = new DepartmentBO();
            this.session = session;
            
        }

        private void SessionReviewForm_Load(object sender, EventArgs e)
        {
            dgv.DataSource = examBO.GetRawInSessionWithNames(session.Id);
            descrTextBox.Text = session.Description;
            govCheckBox.Checked = session.Gov;

            if (session.StartDate.Month > 10)
                years = session.StartDate.Year.ToString() + "/" + (session.StartDate.Year + 1).ToString();
            else
                years = (session.StartDate.Year - 1).ToString() + "/" + (session.StartDate.Year).ToString();
            winterSession = session.StartDate.Month == 12 || session.StartDate.Month == 1 ||
                            session.StartDate.Month == 2;
            this.Text = (winterSession ? "Зимняя сессия " : "Летняя сессия ") + years;

        }

        private void generateSchedule(object sender, EventArgs e)
        {
            new CreateScheduleForm(session).ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<SessionEntry> entries = new List<SessionEntry>();
            foreach (DataRow row in ((DataTable) dgv.DataSource).Rows)
            {
                if ((int) row["subGroup"] == 0)
                    entries.Add(new SessionEntry((int)row["idGroup"], (string)row["nameGroup"], (string)row["nameLecturer"], (int)row["idLecturer"],
                                                 (string) row["nameSubject"], (int) row["idSubject"],
                                                 new string[] {(string) row["examAuditory"]},
                                                 ((string) row["commissionExam"]).Split(';'), session.Gov));
            }
            _next = new CreateSessionStepOne(entries);
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                var r = dgv.SelectedRows[0];

                GroupVO g = groupBO.GetGroupWithId((int)r.Cells["idGroupColumn"].Value);
                SubjectVO s = subjectBO.GetSubjectWithId((int) r.Cells["idSubjectColumn"].Value);
                LecturerVO l = lecturerBO.GetLecturerWithId((int) r.Cells["idLecturerColumn"].Value);
                if (g == null)
                {
                    MessageBox.Show("Сессия не актуальна. Группа с данным именем не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (s == null)
                {
                    MessageBox.Show("Сессия не актуальна. Предмет с данным именем не найден", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (l == null)
                {
                    MessageBox.Show("Сессия не актуальна. Преподаватель с данным именем не найден", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                new MarkForm(g, s, l, (DateTime)r.Cells[5].Value).ShowDialog();
            }
        }

        private void SessionReviewForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            session.Description = descrTextBox.Text;
            sessionBO.UpdateSession(session);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                CreateMarksReport(examBO.GetExamWithId((int)dgv.SelectedRows[0].Cells[7].Value));
            }
        }

        private void reportButton_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                CreateReport(examBO.GetExamWithId((int)dgv.SelectedRows[0].Cells[7].Value));
            }
        }
        /// <summary>
        /// Экзаменационная ведомость
        /// </summary>
        /// <param name="exam">Экзамен</param>
        /// <param name="design">дизайнер</param>
        void CreateReport( ExamVO exam, bool design = false)
        {
            using (FastReport.Report report = new FastReport.Report())
            {
                if (design)
                    report.Load("../../Reports/ExamSheetTemplate.frx");
                else
                    report.Load("Reports/ExamSheetTemplate.frx");
                report.AutoFillDataSet = true;
                var t = studentBO.GetStudentsFromGroupRaw(exam.IdGroup);
                GroupVO group = groupBO.GetGroupWithId(exam.IdGroup);
                DepartmentVO department = departmentBO.GetDepartmentWithId(group.IdDepartment);

                report.RegisterData(t, "Student");
                report.GetDataSource("Student").Enabled = true;
                report.SetParameterValue("department", department.Name);
                report.SetParameterValue("specialization", department.SpecializationName);
                report.SetParameterValue("course", group.Course);
                report.SetParameterValue("group", exam.NameGroup);
                report.SetParameterValue("years", years);
                report.SetParameterValue("discipline",subjectBO.GetSubjectWithId(exam.IdSubject).Name);
                report.SetParameterValue("semestr", (group.Course - 1)*2 + (winterSession ? 1 : 2));
                report.SetParameterValue("formControll", session.Gov ? "Державний іспит" : "Іспит");
                report.SetParameterValue("comission", exam.Commission);
                report.SetParameterValue("headLecturer", lecturerBO.GetLecturerWithId(department.IdLecturer).Name);
                if (design)
                {
                    report.Design();
                    File.Copy("../../Reports/ExamSheetTemplate.frx", "Reports/ExamSheetTemplate.frx", true);
                }
                else
                    report.Show();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                CreateReport(examBO.GetExamWithId((int)dgv.SelectedRows[0].Cells[7].Value), true);
            }
        }

        void CreateMarksReport(ExamVO exam, bool design = false)
        {
            using (FastReport.Report report = new FastReport.Report())
            {
                if (design)
                    report.Load("../../Reports/MarksReportTemplate.frx");
                else
                    report.Load("Reports/MarksReportTemplate.frx");
                report.AutoFillDataSet = true;

                GroupVO group = groupBO.GetGroupWithId(exam.IdGroup);
                SubjectVO subject = subjectBO.GetSubjectWithId(exam.IdSubject);
                if (subject == null)
                {
                    MessageBox.Show("Сессия не актуальна. Предмет", "Ошибка",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                var t = studentBO.GetStudentMarkRaw(group, subject);
                DepartmentVO department = departmentBO.GetDepartmentWithId(group.IdDepartment);
                List<object> markStatistic = new List<object>();
                int[] count = new int[13];
                float average = 0;

                foreach (DataRow row in t.Rows)
                    count[(int)(row["mark"] is DBNull ? 0 : row["mark"])]++;
                int mark = 1;
                for (int i = 1; i < (subject.MarkType == SubjectVO.MarkT._5 ? 6 : 13); i++)
                {
                    markStatistic.Add(new {Mark = mark++, Count = count[i]});
                }
                foreach (DataRow row in t.Rows)
                {
                    average += (int)(row["mark"] is DBNull ? 0 : row["mark"]);
                }
                average /= t.Rows.Count;
                


                report.RegisterData(t, "Student");
                report.GetDataSource("Student").Enabled = true;
                report.RegisterData(markStatistic, "Statistic");
                report.GetDataSource("Statistic").Enabled = true;
                report.SetParameterValue("department", department.Name);
                report.SetParameterValue("specialization", department.SpecializationName);
                report.SetParameterValue("group", exam.NameGroup);
                report.SetParameterValue("years", years);
                report.SetParameterValue("average", average);
                report.SetParameterValue("lecturer", lecturerBO.GetLecturerWithId(exam.IdLecturer).Name);
                report.SetParameterValue("discipline", subject.Name);
                report.SetParameterValue("semestr", (group.Course - 1)*2 + (winterSession ? 1 : 2));
                report.SetParameterValue("formControll", session.Gov ? "Державний іспит" : "Іспит");
                report.SetParameterValue("comission", exam.Commission);
                report.SetParameterValue("headLecturer", lecturerBO.GetLecturerWithId(department.IdLecturer).Name);
                if (design)
                {
                    report.Design();
                    File.Copy("../../Reports/MarksReportTemplate.frx", "Reports/MarksReportTemplate.frx", true);
                }
                else
                    report.Show();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                CreateMarksReport(examBO.GetExamWithId((int)dgv.SelectedRows[0].Cells[7].Value), true);
            }
        }
    }
}
