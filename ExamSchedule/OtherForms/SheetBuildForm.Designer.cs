﻿namespace ExamSchedule
{
    partial class SheetBuildForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SheetBuildForm));
            this.label9 = new System.Windows.Forms.Label();
            this.groupComboBox = new System.Windows.Forms.ComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.subjectCodeComboBox = new System.Windows.Forms.ComboBox();
            this.subjectNameComboBox = new System.Windows.Forms.ComboBox();
            this.lecturerComboBox = new System.Windows.Forms.ComboBox();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.makeReportButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.departmentComboBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.sem1RadioButton = new System.Windows.Forms.RadioButton();
            this.sem2RadioButton = new System.Windows.Forms.RadioButton();
            this.dateCheckBox = new System.Windows.Forms.CheckBox();
            this.lecturerCheckBox = new System.Windows.Forms.CheckBox();
            this.subjectCheckBox = new System.Windows.Forms.CheckBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 81);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 20);
            this.label9.TabIndex = 25;
            this.label9.Text = "Группа";
            // 
            // groupComboBox
            // 
            this.groupComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.groupComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.groupComboBox.BackColor = System.Drawing.SystemColors.Window;
            this.groupComboBox.DisplayMember = "Name";
            this.groupComboBox.FormattingEnabled = true;
            this.groupComboBox.Location = new System.Drawing.Point(126, 81);
            this.groupComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupComboBox.Name = "groupComboBox";
            this.groupComboBox.Size = new System.Drawing.Size(340, 28);
            this.groupComboBox.TabIndex = 24;
            this.groupComboBox.ValueMember = "Id";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(12, 117);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(265, 24);
            this.checkBox1.TabIndex = 26;
            this.checkBox1.Text = "Заполнить номера зачеток";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 184);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 20);
            this.label2.TabIndex = 30;
            this.label2.Text = "Код предмета";
            // 
            // subjectCodeComboBox
            // 
            this.subjectCodeComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.subjectCodeComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.subjectCodeComboBox.DisplayMember = "Code";
            this.subjectCodeComboBox.Enabled = false;
            this.subjectCodeComboBox.FormattingEnabled = true;
            this.subjectCodeComboBox.Location = new System.Drawing.Point(221, 181);
            this.subjectCodeComboBox.Name = "subjectCodeComboBox";
            this.subjectCodeComboBox.Size = new System.Drawing.Size(347, 28);
            this.subjectCodeComboBox.TabIndex = 29;
            this.subjectCodeComboBox.ValueMember = "Id";
            this.subjectCodeComboBox.SelectedIndexChanged += new System.EventHandler(this.codeComboBox_SelectedValueChanged);
            // 
            // subjectNameComboBox
            // 
            this.subjectNameComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.subjectNameComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.subjectNameComboBox.DisplayMember = "Name";
            this.subjectNameComboBox.Enabled = false;
            this.subjectNameComboBox.FormattingEnabled = true;
            this.subjectNameComboBox.Location = new System.Drawing.Point(221, 147);
            this.subjectNameComboBox.Name = "subjectNameComboBox";
            this.subjectNameComboBox.Size = new System.Drawing.Size(347, 28);
            this.subjectNameComboBox.TabIndex = 27;
            this.subjectNameComboBox.ValueMember = "Id";
            this.subjectNameComboBox.SelectedIndexChanged += new System.EventHandler(this.nameСomboBox_SelectedValueChanged);
            // 
            // lecturerComboBox
            // 
            this.lecturerComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.lecturerComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.lecturerComboBox.BackColor = System.Drawing.SystemColors.Window;
            this.lecturerComboBox.DisplayMember = "Name";
            this.lecturerComboBox.Enabled = false;
            this.lecturerComboBox.FormattingEnabled = true;
            this.lecturerComboBox.Location = new System.Drawing.Point(221, 217);
            this.lecturerComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lecturerComboBox.Name = "lecturerComboBox";
            this.lecturerComboBox.Size = new System.Drawing.Size(347, 28);
            this.lecturerComboBox.TabIndex = 31;
            this.lecturerComboBox.ValueMember = "Id";
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Enabled = false;
            this.dateTimePicker.Location = new System.Drawing.Point(90, 253);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(200, 28);
            this.dateTimePicker.TabIndex = 33;
            // 
            // makeReportButton
            // 
            this.makeReportButton.Location = new System.Drawing.Point(12, 297);
            this.makeReportButton.Name = "makeReportButton";
            this.makeReportButton.Size = new System.Drawing.Size(262, 42);
            this.makeReportButton.TabIndex = 35;
            this.makeReportButton.Text = "Сформировать ведомость";
            this.makeReportButton.UseVisualStyleBackColor = true;
            this.makeReportButton.Click += new System.EventHandler(this.makeReportButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 46);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 20);
            this.label5.TabIndex = 37;
            this.label5.Text = "Отделение";
            // 
            // departmentComboBox
            // 
            this.departmentComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.departmentComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.departmentComboBox.BackColor = System.Drawing.SystemColors.Window;
            this.departmentComboBox.DisplayMember = "Name";
            this.departmentComboBox.FormattingEnabled = true;
            this.departmentComboBox.Location = new System.Drawing.Point(126, 43);
            this.departmentComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.departmentComboBox.Name = "departmentComboBox";
            this.departmentComboBox.Size = new System.Drawing.Size(340, 28);
            this.departmentComboBox.TabIndex = 36;
            this.departmentComboBox.ValueMember = "Id";
            this.departmentComboBox.SelectedIndexChanged += new System.EventHandler(this.departmentComboBox_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(280, 297);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(55, 42);
            this.button1.TabIndex = 38;
            this.button1.Text = "edit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // sem1RadioButton
            // 
            this.sem1RadioButton.AutoSize = true;
            this.sem1RadioButton.Checked = true;
            this.sem1RadioButton.Location = new System.Drawing.Point(129, 11);
            this.sem1RadioButton.Name = "sem1RadioButton";
            this.sem1RadioButton.Size = new System.Drawing.Size(135, 24);
            this.sem1RadioButton.TabIndex = 41;
            this.sem1RadioButton.TabStop = true;
            this.sem1RadioButton.Text = "1-й семестр";
            this.sem1RadioButton.UseVisualStyleBackColor = true;
            // 
            // sem2RadioButton
            // 
            this.sem2RadioButton.AutoSize = true;
            this.sem2RadioButton.Location = new System.Drawing.Point(263, 11);
            this.sem2RadioButton.Name = "sem2RadioButton";
            this.sem2RadioButton.Size = new System.Drawing.Size(135, 24);
            this.sem2RadioButton.TabIndex = 42;
            this.sem2RadioButton.Text = "2-й семестр";
            this.sem2RadioButton.UseVisualStyleBackColor = true;
            // 
            // dateCheckBox
            // 
            this.dateCheckBox.AutoSize = true;
            this.dateCheckBox.Location = new System.Drawing.Point(12, 258);
            this.dateCheckBox.Name = "dateCheckBox";
            this.dateCheckBox.Size = new System.Drawing.Size(72, 24);
            this.dateCheckBox.TabIndex = 44;
            this.dateCheckBox.Text = "Дата";
            this.dateCheckBox.UseVisualStyleBackColor = true;
            this.dateCheckBox.CheckedChanged += new System.EventHandler(this.dateCheckBox_CheckedChanged);
            // 
            // lecturerCheckBox
            // 
            this.lecturerCheckBox.AutoSize = true;
            this.lecturerCheckBox.Location = new System.Drawing.Point(12, 220);
            this.lecturerCheckBox.Name = "lecturerCheckBox";
            this.lecturerCheckBox.Size = new System.Drawing.Size(166, 24);
            this.lecturerCheckBox.TabIndex = 45;
            this.lecturerCheckBox.Text = "Преподаватель";
            this.lecturerCheckBox.UseVisualStyleBackColor = true;
            this.lecturerCheckBox.CheckedChanged += new System.EventHandler(this.lecturerCheckBox_CheckedChanged);
            // 
            // subjectCheckBox
            // 
            this.subjectCheckBox.AutoSize = true;
            this.subjectCheckBox.Location = new System.Drawing.Point(12, 149);
            this.subjectCheckBox.Name = "subjectCheckBox";
            this.subjectCheckBox.Size = new System.Drawing.Size(203, 24);
            this.subjectCheckBox.TabIndex = 46;
            this.subjectCheckBox.Text = "Название предмета";
            this.subjectCheckBox.UseVisualStyleBackColor = true;
            this.subjectCheckBox.CheckedChanged += new System.EventHandler(this.subjectCheckBox_CheckedChanged);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SheetBuildForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(578, 347);
            this.Controls.Add(this.subjectCheckBox);
            this.Controls.Add(this.lecturerCheckBox);
            this.Controls.Add(this.dateCheckBox);
            this.Controls.Add(this.sem2RadioButton);
            this.Controls.Add(this.sem1RadioButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.departmentComboBox);
            this.Controls.Add(this.makeReportButton);
            this.Controls.Add(this.dateTimePicker);
            this.Controls.Add(this.lecturerComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.subjectCodeComboBox);
            this.Controls.Add(this.subjectNameComboBox);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.groupComboBox);
            this.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(584, 382);
            this.Name = "SheetBuildForm";
            this.Text = "Параметры ведомости";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox groupComboBox;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox subjectCodeComboBox;
        private System.Windows.Forms.ComboBox subjectNameComboBox;
        private System.Windows.Forms.ComboBox lecturerComboBox;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.Button makeReportButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox departmentComboBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton sem1RadioButton;
        private System.Windows.Forms.RadioButton sem2RadioButton;
        private System.Windows.Forms.CheckBox dateCheckBox;
        private System.Windows.Forms.CheckBox lecturerCheckBox;
        private System.Windows.Forms.CheckBox subjectCheckBox;
        private System.Windows.Forms.Timer timer1;
    }
}