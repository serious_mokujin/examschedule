﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ExamSchedule.BO;
using ExamSchedule.VO;
using System.IO;
using System.Reflection;

namespace ExamSchedule
{
    public partial class CreateScheduleForm : Form
    {

        private ExamBO examBO;
        private SessionBO sessionBO;
        private SessionVO session;

        private bool winterSession;
        private string years;

        public CreateScheduleForm(SessionVO session)
        {
            InitializeComponent();
            this.session = session;
            examBO = new ExamBO();
            sessionBO = new SessionBO();
        }
        private void ReloadAutocomplete()
        {
            groupComboBox.Items.Clear();
            groupComboBox.Items.AddRange(sessionBO.GetGroupsInSession(session));

            lecturerComboBox.Items.Clear();
            lecturerComboBox.Items.AddRange(sessionBO.GetLecturersInSession(session));
        }

        private void CreateScheduleForm_Load(object sender, EventArgs e)
        {
            ReloadAutocomplete();
            if (session.StartDate.Month > 10)
                years = session.StartDate.Year.ToString() + "/" + (session.StartDate.Year + 1).ToString();
            else
                years = (session.StartDate.Year - 1).ToString() + "/" + (session.StartDate.Year).ToString();
            winterSession = session.StartDate.Month == 12 || session.StartDate.Month == 1 ||
                            session.StartDate.Month == 2;
            this.Text = (winterSession ? "Зимняя сессия " : "Летняя сессия ") + years;
            dateTimePicker.MinDate = session.StartDate;
            try
            {
                dateTimePicker.MaxDate = session.EndDate;
            }
            catch
            {
                dateTimePicker.MaxDate = session.StartDate.AddDays(1);
            }
            try
            {
                dateTimePicker.Value = DateTime.Now;
            }
            catch {}
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (tabControl.SelectedTab == groupTabPage)
                if (groupComboBox.SelectedItem != null)
                    CreateGroupReport((string) groupComboBox.SelectedItem);
            if (tabControl.SelectedTab == lecturerTabPage)
                if (lecturerComboBox.SelectedItem != null)
                    CreateLecturerReport((LecturerVO) lecturerComboBox.SelectedItem);
            if(tabControl.SelectedTab == courseTabPage)
                CreateGeneralReport((int)courseNumericUpDown.Value);
            if (tabControl.SelectedTab == dateTabPage)
                CreateDateReport(dateTimePicker.Value);
        }

        void CreateGeneralReport(int course, bool design = false)
        {
            if (session == null) return;
            using (FastReport.Report report = new FastReport.Report())
            {
                if(design)
                    report.Load("../../Reports/ScheduleTemplate.frx");
                else
                    report.Load("Reports/ScheduleTemplate.frx");
                report.AutoFillDataSet = true;
                report.RegisterData(GenerateDataForReport(examBO.GetRawInSessionOfCourse(session, course)), "Entries");
                report.GetDataSource("Entries").Enabled = true;
                report.SetParameterValue("winterSession", winterSession);
                report.SetParameterValue("course", Helper.ToRoman(course));
                report.SetParameterValue("years", years);
                report.SetParameterValue("showSubgroups", session.Gov);
                report.SetParameterValue("type",  "course");
                report.SetParameterValue("showCommission", session.Gov);
                if (design)
                {
                    report.Design();
                    File.Copy("../../Reports/ScheduleTemplate.frx", "Reports/ScheduleTemplate.frx", true);
                }
                else
                    report.Show();
            }
        }
        void CreateLecturerReport(LecturerVO lecturer, bool design = false)
        {

            if (session == null) return;
            using (FastReport.Report report = new FastReport.Report())
            {
                if (design)
                    report.Load("../../Reports/ScheduleTemplate.frx");
                else
                    report.Load("Reports/ScheduleTemplate.frx");
                report.AutoFillDataSet = true;
                report.RegisterData(GenerateDataForReport(examBO.GetRawInSessionForLecturer(session, lecturer)), "Entries");
                report.GetDataSource("Entries").Enabled = true;
                report.SetParameterValue("winterSession", winterSession);
                report.SetParameterValue("lecturer", lecturer.Name);
                report.SetParameterValue("years", years);
                report.SetParameterValue("showSubgroups", session.Gov);
                report.SetParameterValue("type", "lecturer");
                report.SetParameterValue("showCommission", session.Gov);
                if (design)
                {
                    report.Design();
                    File.Copy("../../Reports/ScheduleTemplate.frx", "Reports/ScheduleTemplate.frx", true);
                }
                else
                    report.Show();
            }
        }
        void CreateGroupReport(string group, bool design = false)
        {

            if (session == null) return;
            using (FastReport.Report report = new FastReport.Report())
            {
                if (design)
                    report.Load("../../Reports/ScheduleTemplate.frx");
                else
                    report.Load("Reports/ScheduleTemplate.frx");
                report.AutoFillDataSet = true;
                report.RegisterData(GenerateDataForReport(examBO.GetRawInSessionForGroup(session, group)), "Entries");
                report.GetDataSource("Entries").Enabled = true;
                report.SetParameterValue("winterSession", winterSession);
                report.SetParameterValue("group", group);
                report.SetParameterValue("years", years);
                report.SetParameterValue("showSubgroups", session.Gov);
                report.SetParameterValue("type", "group");
                report.SetParameterValue("showCommission", session.Gov);
                if (design)
                {
                    report.Design();
                    File.Copy("../../Reports/ScheduleTemplate.frx", "Reports/ScheduleTemplate.frx", true);
                }
                else
                    report.Show();
            }
        }
        void CreateDateReport(DateTime date, bool design = false)
        {
            if (session == null) return;
            using (FastReport.Report report = new FastReport.Report())
            {
                if (design)
                    report.Load("../../Reports/ScheduleTemplate.frx");
                else
                    report.Load("Reports/ScheduleTemplate.frx");
                report.AutoFillDataSet = true;
                report.RegisterData(GenerateDataForReportOnDate(examBO.GetRawInSessionOnDate(session, date), date), "Entries");
                report.GetDataSource("Entries").Enabled = true;
                report.SetParameterValue("winterSession", winterSession);
                report.SetParameterValue("date", date);
                report.SetParameterValue("years", years);
                report.SetParameterValue("showSubgroups", session.Gov);
                report.SetParameterValue("type", "date");
                report.SetParameterValue("showCommission", session.Gov);
                if (design)
                {
                    report.Design();
                    File.Copy("../../Reports/ScheduleTemplate.frx", "Reports/ScheduleTemplate.frx", true);
                }
                else
                    report.Show();
            }
        }

        public class ReportSessionEntry
        {
            public int Id { get; set; }
            public string NameLecturer { get; set; }
            public string NameGroup { get; set; }
            public string NameSubject { get; set; }
            public string EventType { get; set; }
            public DateTime Date { get; set; }
            public string Auditory { get; set; }
            public string ExamType { get; set; }
            public int SubGroup { get; set; }

            public string Commission { get; set; }
        }

        IEnumerable<ReportSessionEntry> GenerateDataForReport(DataTable table)
        {
            List<ReportSessionEntry> data = new List<ReportSessionEntry>();
            int id = 0;
            foreach (DataRow o in table.Rows)
            {

                data.Add(new ReportSessionEntry()
                    {
                        Id = id++,
                        NameLecturer = (string) o["nameLecturer"],
                        NameGroup = (string) o["nameGroup"],
                        NameSubject = (string) o["nameSubject"],
                        EventType = "exam",
                        Date = (DateTime) o["examDate"],
                        Auditory = (string) o["examAuditory"],
                        SubGroup = (o["subGroup"] is DBNull ? 0 : (int) (o["subGroup"])),
                        Commission = (string)o["commissionExam"]
                    });
                if ((int) o["subGroup"] == 0)
                    data.Add(new ReportSessionEntry()
                        {
                            Id = id++,
                            NameLecturer = (string) o["nameLecturer"],
                            NameGroup = (string) o["nameGroup"],
                            NameSubject = (string) o["nameSubject"],
                            EventType = "cons",
                            Date = (DateTime) o["consDate"],
                            Auditory = (string) o["consAuditory"],
                            SubGroup = (o["subGroup"] is DBNull ? 0 : (int) (o["subGroup"])),
                            Commission = (string)o["commissionExam"]
                        });
            }
            if (data.Count() > 0)
            {
                DateTime minDate = data.Min(p => p.Date);
                DateTime maxDate = data.Max(p => p.Date);
                for (int i = 0; i < (int) ((maxDate - minDate).TotalDays); i++)
                {
                    if (!data.Any(p => p.Date.Date == minDate.Date.AddDays(i)))
                        data.Add(new ReportSessionEntry()
                            {
                                Id = -1,
                                NameLecturer = "",
                                NameGroup = "",
                                NameSubject = "",
                                EventType = "",
                                Date = minDate.Date.AddDays(i),
                                Auditory = "",
                                ExamType = "",
                                SubGroup = 0
                            });
                }
            }
            return data;
        }
        IEnumerable<ReportSessionEntry> GenerateDataForReportOnDate(DataTable table, DateTime date)
        {
            List<ReportSessionEntry> data = new List<ReportSessionEntry>();
            int id = 0;
            foreach (DataRow o in table.Rows)
            {
                if (((DateTime) o["examDate"]).Date == date.Date)
                    data.Add(new ReportSessionEntry()
                        { 
                            Id = id++,
                            NameLecturer = (string) o["nameLecturer"],
                            NameGroup = (string) o["nameGroup"],
                            NameSubject = (string) o["nameSubject"],
                            EventType = "exam",
                            Date = (DateTime) o["examDate"],
                            Auditory = (string) o["examAuditory"],
                            SubGroup = (o["subGroup"] is DBNull ? 0 : (int) (o["subGroup"])),
                            Commission = (string)o["commissionExam"]
                        });
                if (((DateTime)o["consDate"]).Date == date.Date)
                    if ((int) o["subGroup"] == 0)
                        data.Add(new ReportSessionEntry()
                            {
                                Id = id++,
                                NameLecturer = (string) o["nameLecturer"],
                                NameGroup = (string) o["nameGroup"],
                                NameSubject = (string) o["nameSubject"],
                                EventType = "cons",
                                Date = (DateTime) o["consDate"],
                                Auditory = (string) o["consAuditory"],
                                SubGroup = (o["subGroup"] is DBNull ? 0 : (int) (o["subGroup"])),
                                Commission = (string)o["commissionExam"]
                            });
            }
            return data;
        }
        private void button6_Click(object sender, EventArgs e)
        {
            if (tabControl.SelectedTab == groupTabPage)
                if (groupComboBox.SelectedItem != null)
                    CreateGroupReport((string)groupComboBox.SelectedItem, true);
            if (tabControl.SelectedTab == lecturerTabPage)
                if (lecturerComboBox.SelectedItem != null)
                    CreateLecturerReport((LecturerVO)lecturerComboBox.SelectedItem, true);
            if (tabControl.SelectedTab == courseTabPage)
                CreateGeneralReport((int)courseNumericUpDown.Value, true);
            if (tabControl.SelectedTab == dateTabPage)
                CreateDateReport(dateTimePicker.Value, true);
        }

    }
}
