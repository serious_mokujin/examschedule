﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ExamSchedule
{
    public partial class ConnectionForm : Form
    {
        bool forsePassword;
        public ConnectionForm(bool forsePassword = false)
        {
            this.forsePassword = forsePassword;
            InitializeComponent();
        }

        private void ConnectionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            string data = "ConnectionForm{";
            data += "remember["+rememberPasswordCheckBox.Checked.ToString()+"]";

            string t = serverComboBox.Text;
            if (serverComboBox.Items.Contains(t)) serverComboBox.Items.Remove(t);
            serverComboBox.Items.Insert(0, t);

            t = userComboBox.Text;
            if (userComboBox.Items.Contains(t)) userComboBox.Items.Remove(t);
            userComboBox.Items.Insert(0, t);
            
            data += "server[";
            for (int i = 0; i < serverComboBox.Items.Count; i++)
                data += serverComboBox.Items[i].ToString() + (i ==  serverComboBox.Items.Count - 1 ? "" : ";");
            data += "]";
            data += "user[";
            for (int i = 0; i < userComboBox.Items.Count; i++)
                data += userComboBox.Items[i].ToString() + (i == userComboBox.Items.Count - 1 ? "" : ";");
            data += "]";

            if(rememberPasswordCheckBox.Checked)
                data += "password["+passwordTextBox.Text+"]";
            data += "}";

            if (File.Exists("fields.sav"))
            {
                string fields = File.ReadAllText("fields.sav");
                int from = fields.IndexOf("ConnectionForm");
                int to = from + 1;
                while (fields[to] != '}') to++;
                to++;
                fields = fields.Remove(from, to - from);
                fields = fields.Insert(from, data);
                File.WriteAllText("fields.sav", fields);
            }
            else
                File.WriteAllText("fields.sav", data);
        }

        private void ConnectionForm_Load(object sender, EventArgs e)
        {
            if (File.Exists("fields.sav"))
            {
                string fields = File.ReadAllText("fields.sav");
                int index = fields.IndexOf("ConnectionForm");
                if (index == -1)
                    return;
                while (fields[index] != '{') index++;
                index++;

                Dictionary<string, string> values = new Dictionary<string, string>();
                while(fields[index] != '}')
                    values.Add(TakeWhile(fields, ref index, '['), TakeWhile(fields, ref index, ']'));
                index++;
                bool rememberPassword = false;
                foreach (var item in values)
                {
                    string[] vals;
                    switch (item.Key)
                    {
                        case "remember":
                            if (bool.Parse(item.Value))
                            {
                                if (!forsePassword)
                                {
                                    rememberPassword = true;
                                    rememberPasswordCheckBox.Checked = true;
                                }
                            }
                            break;
                        case "server":
                            vals = item.Value.Split(';');
                            serverComboBox.Text = vals[0];
                            foreach (var item2 in vals)
                                serverComboBox.Items.Add(item2);
                            break;
                        case "user":
                            vals = item.Value.Split(';');
                            userComboBox.Text = vals[0];
                            foreach (var item2 in vals)
                                userComboBox.Items.Add(item2);
                            break;
                        case "password":
                            if(!forsePassword)
                                passwordTextBox.Text = item.Value;
                            break;
                        default:
                            break;
                    }
                }

                if (rememberPassword && !forsePassword)
                    Connect();
            }
        }
        /// <summary>
        /// Возвращает подстроку в строке пока не встретит символ
        /// </summary>
        /// <param name="str">Исходная строка</param>
        /// <param name="from">Индекс начала поиска</param>
        /// <param name="to">Символ по которому поиск прекращается</param>
        /// <returns></returns>
        string TakeWhile(string str, ref int from, char to)
        {
            int start = from;
            int count = 0;
            while(str[from] != to)
            {
                from++;
                count++;
            }
            from++;
            return str.Substring(start, count);
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            Connect();
        }

        void Connect()
        {
            infoLabel.Visible = true;
            infoLabel.Text = "установка соединения";
            infoLabel.ForeColor = Color.Blue;
            Refresh();
            if(Server.EstablishConnection(serverComboBox.Text, userComboBox.Text,"3306", passwordTextBox.Text))
            {
                if(!Server.CheckDatabase("ExamSchedule"))
                    Server.CreateDatabase("CreationScript.sql");
                if (!DB.EstablishConnection(serverComboBox.Text, userComboBox.Text, "3306", passwordTextBox.Text, "ExamSchedule"))
                {
                    MessageBox.Show("Error during database connection", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                } 
                DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            else
            {
                infoLabel.Text = "ошибка соединения";
                infoLabel.ForeColor = Color.Red;
            }
        }

        private void rememberPasswordCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (rememberPasswordCheckBox.Checked)
                infoPassLabel.Visible = true;
            else
                infoPassLabel.Visible = false;
        }
    }
}
