﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using ExamSchedule.BO;
using ExamSchedule.VO;


namespace ExamSchedule.OtherForms.CreateSessionMaster
{
    public partial class CreateSessionStepOne : Form
    {
        private int currentStep;

        private BindingList<SessionEntry> _entries;
        private ExamBO examBO;
        private GroupBO groupBO;
        private SubjectBO subjectBO;
        private LecturerBO lecturerBO;
        private SessionBO sessionBO;
        private DepartmentBO departmentBO;

        
        private List<Control> blockableControlls;

        public CreateSessionStepOne(List<SessionEntry> entries = null)
        {
            InitializeComponent();
            dgv.AutoGenerateColumns = false;
            this.ClientSize = new Size(1000, 650);
            stepOnePanel.Location = stepTwoPanel.Location = stepThreePanel.Location = new Point(0, 0);
            stepOnePanel.Size = stepTwoPanel.Size = stepThreePanel.Size = new Size(1000, 600);
            stepOnePanel.Visible = true;
            stepTwoPanel.Visible = stepThreePanel.Visible = false;
            currentStep = 1;

            blockableControlls = new List<Control>();
            blockableControlls.AddRange(new Control[] { addButton, deleteButton, changeButton, govCheckBox, acceptButton, daysNumericUpDown, startDatePicker, BackButton, NextButton });
            
            if(entries != null)
                _entries = new BindingList<SessionEntry>(entries);
            
            examBO = new ExamBO();
            groupBO = new GroupBO();
            subjectBO = new SubjectBO();
            lecturerBO = new LecturerBO();
            sessionBO = new SessionBO();
            departmentBO = new DepartmentBO();
            rnd = new Random((int)DateTime.Now.Ticks);

            ReloadAutocomplete();
        }
        private void ReloadAutocomplete()
        {
            departmentComboBox.Items.Clear();
            departmentComboBox.Items.Add("-- все --");
            departmentComboBox.Items.AddRange(departmentBO.GetDepartments());

            groupComboBox.Items.Clear();
            groupComboBox.Items.AddRange(groupBO.GetNotGraduatedGroups());

            var subjects = subjectBO.GetSubjects();
            subjectNameComboBox.Items.Clear();
            subjectCodeComboBox.Items.Clear();
            subjectNameComboBox.Items.AddRange(subjects);
            subjectCodeComboBox.Items.AddRange(subjects);

            ReloadLecturerComboBox();

            commissionComboBox.Items.Clear();
            commissionComboBox.Items.AddRange(lecturerBO.GetLecturers());
        }
        void ReloadLecturerComboBox()
        {
            lecturerComboBox.Items.Clear();
            if (subjectNameComboBox.SelectedItem != null)
            {
                lecturerComboBox.Items.AddRange(
                    lecturerBO.GetLecturersBySubject((SubjectVO) subjectNameComboBox.SelectedItem));
                if(lecturerComboBox.Items.Count > 0)
                    lecturerComboBox.SelectedIndex = 0;
            }
            else
                lecturerComboBox.Items.AddRange(lecturerBO.GetLecturers());
        }


        private void NextButton_Click(object sender, EventArgs e)
        {
            if (currentStep < 3)
            {
                currentStep++;
                stepOnePanel.Visible = stepTwoPanel.Visible = stepThreePanel.Visible = false;
                NextButton.Enabled = BackButton.Enabled = true;
                if (currentStep == 1) 
                { 
                    stepOnePanel.Visible = true;
                    BackButton.Enabled = false;
                }
                if (currentStep == 2) stepTwoPanel.Visible = true;
                if (currentStep == 3)
                {
                    stepThreePanel.Visible = true;
                    NextButton.Enabled = false;
                }
                Text = "Создание сессии: шаг " + currentStep;
            }
        }
        private void BackButton_Click(object sender, EventArgs e)
        {
            if (currentStep > 1)
            {
                currentStep--;
                stepOnePanel.Visible = stepTwoPanel.Visible = stepThreePanel.Visible = false;
                NextButton.Enabled = BackButton.Enabled = true;
                if (currentStep == 1)
                {
                    stepOnePanel.Visible = true;
                    BackButton.Enabled = false;
                }
                if (currentStep == 2) stepTwoPanel.Visible = true;
                if (currentStep == 3) stepThreePanel.Visible = true;
                DropFoundSolution();
                Text = "Создание сессии: шаг " + currentStep;
            }
        }

        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            WriteListView();
        }
        private void Form_Load(object sender, EventArgs e)
        {
            if (_entries == null)
                ReadListView();
            dgv.DataSource = _entries;
            dgv.Update();
            Text = "Создание сессии: шаг " + currentStep;
        }
        
        private void WriteListView()
        {
            XmlWriter a = XmlWriter.Create("entryItems.xml");
            a.WriteStartDocument();
            a.WriteStartElement("session");
            a.WriteAttributeString("startDate", startDatePicker.Value.ToString());
            a.WriteAttributeString("excludeDates", string.Join(";", excludedDatesListBox.Items.Cast<DateTime>().Select(p => p.ToString("dd.MM.yy"))));

            a.WriteStartElement("entries");
            foreach (SessionEntry item in _entries)
            {
                a.WriteStartElement("item");
                a.WriteAttributeString("group", item.NameGroup);
                a.WriteAttributeString("groupId", item.IdGroup.ToString());
                a.WriteAttributeString("subjectName", item.NameSubject);
                a.WriteAttributeString("subjectId", item.IdSubject.ToString());
                a.WriteAttributeString("lecturerId", item.IdLecturer.ToString());
                a.WriteAttributeString("lecturer", item.NameLecturer);
                a.WriteAttributeString("auditories", item.AuditoriesString);
                a.WriteAttributeString("commission", item.CommissionStr);
                
                a.WriteEndElement();
            }
            a.WriteEndElement();
            a.WriteEndDocument();
            a.Close();
        }
        private void ReadListView()
        {
            _entries = new BindingList<SessionEntry>();
            if (!File.Exists("entryItems.xml")) return;
            XmlReader a = XmlReader.Create("entryItems.xml");
            while (a.Read())
            {
                if (a.Name == "item")
                {
                    SessionEntry e = new SessionEntry();
                    int temp = 0;
                    e.NameGroup = a.GetAttribute("group");
                    e.NameSubject = a.GetAttribute("subjectName");
                    e.IdSubject = int.Parse(a.GetAttribute("subjectId"));
                    e.IdLecturer = int.Parse(a.GetAttribute("lecturerId"));
                    e.NameLecturer = a.GetAttribute("lecturer");
                    e.Auditories = a.GetAttribute("auditories").Split(',');
                    e.CommissionStr = a.GetAttribute("commission");
                    e.IdGroup = int.Parse(a.GetAttribute("groupId"));

                    _entries.Add(e);
                }
                if (a.Name == "session" && a.IsStartElement())
                {
                    startDatePicker.Value = DateTime.Parse(a.GetAttribute("startDate"));
                    try
                    {
                        string[] str = a.GetAttribute("excludeDates")
                                        .Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        excludedDatesListBox.Items.AddRange(str.Select<string, object>(p => DateTime.Parse(p)).ToArray());
                    }
                    catch
                    {
                    }
                }
            }
            a.Close();
        }

        #region Step one
        private void nameСomboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (subjectCodeComboBox.SelectedIndex != subjectNameComboBox.SelectedIndex)
                subjectCodeComboBox.SelectedIndex = subjectNameComboBox.SelectedIndex;
            ReloadLecturerComboBox();
        }
        private void codeComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (subjectNameComboBox.SelectedIndex != subjectCodeComboBox.SelectedIndex)
                subjectNameComboBox.SelectedIndex = subjectCodeComboBox.SelectedIndex;
            ReloadLecturerComboBox();
        }
        private void departmentComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void addCommissionButton_Click(object sender, EventArgs e)
        {
            if (commissionListBox.Items.Count < 5)
                if (commissionComboBox.SelectedItem != null)
                    commissionListBox.Items.Add(((LecturerVO)commissionComboBox.SelectedItem).Name);
            /*if (dgv.SelectedRows.Count > 0)
            {
                SessionEntry entry = (SessionEntry)dgv.SelectedRows[0].DataBoundItem;
                LoadDataFromFields(entry);
            }*/
        }
        private void deleteCommissionButton_Click(object sender, EventArgs e)
        {
            if (commissionListBox.SelectedItem != null)
                commissionListBox.Items.Remove(commissionListBox.SelectedItem);
            /*if (dgv.SelectedRows.Count > 0)
            {
                SessionEntry entry = (SessionEntry)dgv.SelectedRows[0].DataBoundItem;
                LoadDataFromFields(entry);
            }*/
        }

        string[] GetAuditories()
        {
            string tmp = auditoriesTextBox.Text;
            tmp = tmp.Trim();
            string[] arr = tmp.Split(new char[] { ' ', ',', ';', '.', '|' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < arr.Length; i++)
                if (arr[i].Length > 4)
                    arr[i] = arr[i].Remove(4, arr[i].Length - 4);
            return arr;
        }
        private void addButton_Click(object sender, EventArgs e)
        {
            SessionEntry s = new SessionEntry();
            if (LoadDataFromFields(s))
            {
                _entries.Add(s);
                DropFoundSolution();
                dgv.Rows[dgv.Rows.GetLastRow(DataGridViewElementStates.None)].Selected = true;
                if (dgv.SelectedRows.Count > 0)
                dgv.FirstDisplayedScrollingRowIndex = dgv.SelectedRows[0].Index;
            }
        }
        private void changeButton_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                SessionEntry entry = (SessionEntry)dgv.SelectedRows[0].DataBoundItem;
                LoadDataFromFields(entry);
            }
        }
        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                _entries.Remove((SessionEntry)dgv.SelectedRows[0].DataBoundItem);
                if(dgv.SelectedRows.Count>0)
                    if (dgv.SelectedRows.Count > 0)
                dgv.FirstDisplayedScrollingRowIndex = dgv.SelectedRows[0].Index;
            }
        }
        bool LoadDataFromFields(SessionEntry entry)
        {
            if (groupComboBox.SelectedItem != null)
            {
                entry.NameGroup = (groupComboBox.SelectedItem as GroupVO).Name;
                entry.IdGroup = (groupComboBox.SelectedItem as GroupVO).Id;
            }
            else
                return false;
            if (lecturerComboBox.SelectedItem != null)
            {
                entry.NameLecturer = (lecturerComboBox.SelectedItem as LecturerVO).Name;
                entry.IdLecturer = (lecturerComboBox.SelectedItem as LecturerVO).Id;
            }
            else
                return false;
            if (subjectNameComboBox.SelectedItem != null)
            {
                entry.NameSubject = (subjectNameComboBox.SelectedItem as SubjectVO).Name;
                entry.IdSubject = (subjectNameComboBox.SelectedItem as SubjectVO).Id;
            }
            else
                return false;
            entry.Auditories = GetAuditories();
            entry.Commission = commissionListBox.Items.Cast<string>().ToArray();
            return true;
        }
        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                SessionEntry entry = (SessionEntry)dgv.SelectedRows[0].DataBoundItem;
                foreach (GroupVO item in groupComboBox.Items)
                {
                    if (item.Id == entry.IdGroup)
                        groupComboBox.SelectedItem = item;
                }
                foreach (SubjectVO item in subjectNameComboBox.Items)
                {
                    if (item.Id == entry.IdSubject)
                    {
                        subjectNameComboBox.SelectedItem = item;
                        subjectCodeComboBox.SelectedItem = item;
                    }
                }
                foreach (LecturerVO item in lecturerComboBox.Items)
                {
                    if (item.Id == entry.IdLecturer)
                        lecturerComboBox.SelectedItem = item;
                }
                auditoriesTextBox.Text = string.Join(",", entry.Auditories);
                commissionListBox.Items.Clear();
                commissionListBox.Items.AddRange(entry.Commission);
            }
        }
        #endregion
        #region Step two

        private void excludeButton_Click(object sender, EventArgs e)
        {
            if (!excludedDatesListBox.Items.Contains(monthCalendar.SelectionStart))
                excludedDatesListBox.Items.Add(monthCalendar.SelectionStart);
        }

        private void includeButton_Click(object sender, EventArgs e)
        {
            if (excludedDatesListBox.SelectedItem != null)
                excludedDatesListBox.Items.Remove(excludedDatesListBox.SelectedItem);
        }
        private void DropOnValueChanged(object sender, EventArgs e)
        {
            DropFoundSolution();
            monthCalendar.MinDate = startDatePicker.Value.Date;
        }
        #endregion
        #region Step three
        private Random rnd;
        private Thread generationThread;
        private List<SessionEvent> bestEvents;
        private DateTime start;
        private List<DateTime> excludedDates;
        private List<ExamVO> examsInPeriod = new List<ExamVO>();
        private bool testExistingExams;

        private void generateButton_Click(object sender, EventArgs e)
        {
            if (_entries.Count == 0) return;
            
            if (generationThread != null && generationThread.ThreadState == ThreadState.Running)
            {
                blockableControlls.ForEach(p => p.Enabled = true);
                generationThread.Abort();
                progressBar1.MarqueeAnimationSpeed = 0;
                generationStartButton.Text = "Начать генерацию";
                timer1.Enabled = false;
            }
            else
            {
                start = startDatePicker.Value.Date;
                excludedDates = excludedDatesListBox.Items.Cast<DateTime>().ToList();
                testExistingExams = checkExistingCheckBox.Checked;
                examsInPeriod.Clear();
                examsInPeriod.AddRange(examBO.GetExamsFromDate(start));

                blockableControlls.ForEach(p => p.Enabled = false);
                generationThread = new Thread(RunGeneration);
                generationThread.Start(bestEvents != null ? (int)(bestEvents.Max(p => p.DateTime.Date) - start).TotalDays : int.MaxValue);
                progressBar1.MarqueeAnimationSpeed = 10;
                generationStartButton.Text = "Остановить генерацию";
                timer1.Enabled = true;
            }
        }
        private void acceptSolutionButton_Click(object sender, EventArgs e)
        {
            if (bestEvents == null)
            {
                DropFoundSolution();
                return;
            }

            SessionVO session = new SessionVO(-1, start, start.AddDays((int)(bestEvents.Max(p => p.DateTime.Date) - start.Date).TotalDays), descrTextBox.Text, govCheckBox.Checked);
            sessionBO.UpdateSession(session);
            foreach (var sessionEvent in bestEvents)
            {
                if (sessionEvent.EventType == EventType.cons)
                {
                    sessionEvent.Entry.ConsDate = sessionEvent.DateTime;
                    sessionEvent.Entry.ConsAuditory = sessionEvent.Auditory;
                }
                if (sessionEvent.EventType == EventType.exam && sessionEvent.Subgroup == 0)
                {
                    sessionEvent.Entry.ExamDate = sessionEvent.DateTime;
                    sessionEvent.Entry.ExamAuditory = sessionEvent.Auditory;
                }
            }
            foreach (var sessionEntry in _entries)
            {
                examBO.UpdateExam(new ExamVO(-1, session.Id, sessionEntry.IdGroup, sessionEntry.NameGroup,
                                             sessionEntry.IdLecturer, sessionEntry.IdSubject,
                                             sessionEntry.ConsDate, sessionEntry.ExamDate,
                                             sessionEntry.ConsAuditory, sessionEntry.ExamAuditory, 0,
                                             sessionEntry.CommissionStr));
                if (session.Gov)
                    examBO.UpdateExam(new ExamVO(-1, session.Id, sessionEntry.IdGroup, sessionEntry.NameGroup,
                                                 sessionEntry.IdLecturer, sessionEntry.IdSubject,
                                                 sessionEntry.ConsDate, sessionEntry.ExamDate.AddDays(1),
                                                 sessionEntry.ConsAuditory, sessionEntry.ExamAuditory, 1,
                                                 sessionEntry.CommissionStr));
            }
            //this._entries.Clear();
            this.Close();
        }
        
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (generationThread.ThreadState != ThreadState.Running)
            {
                blockableControlls.ForEach(p => p.Enabled = true);
                generationThread.Abort();
                progressBar1.MarqueeAnimationSpeed = 0;
                generationStartButton.Text = "Начать генерацию";
                int days = ((int) (bestEvents.Max(p => p.DateTime.Date) - start).TotalDays);
                solutionLabel.Text = "Найдено решение: " +
                                     (days+1).ToString() + " дней. Дата окончания: "+start.AddDays(days).ToString("dd.MM.yy");
                timer1.Enabled = false;
            }
        }

        void DropFoundSolution()
        {
            acceptButton.Enabled = false;
            bestEvents = null;
            solutionLabel.Text = "Решение не найдено";
        }
        private void RunGeneration(object param)
        {
            int minDays = (int)param;
            List<SessionEvent> events;
            List<SessionEntry> entries = new List<SessionEntry>(_entries);
            try
            {
                while (true)
                {
                    if (GenerateSchedule(start, entries, out events))
                    {
                        if (minDays > (int)(events.Max(p => p.DateTime.Date) - start.Date).TotalDays)
                        {
                            bestEvents = events;
                            return;
                        }

                    }
                }
            }
            catch (ThreadAbortException abortException)
            {
            }
        }

        bool GenerateSchedule(DateTime start, List<SessionEntry> entries, out List<SessionEvent> events)
        {
            events = new List<SessionEvent>();
            var groupsLeft = GetGroups(entries);
            groupsLeft.RandomMix(rnd.Next());
            while (groupsLeft.Any())
            {
                var groupEntries = GetGroupEntries(entries, groupsLeft[0]);
                groupEntries.RandomMix(rnd.Next());
                groupsLeft.RemoveAt(0);

                DateTime curDate = start;
                while (groupEntries.Any())
                {
                    for (int i = 0; i < groupEntries.Count; i++)
                    {
                        if (TryToPushEntry(events, groupEntries[i], curDate))
                        {
                            groupEntries.RemoveAt(i);
                            break;
                        }
                    }
                    curDate = curDate.AddDays(1);
                }
            }
            return true;
        }

        bool TryToPushEntry(List<SessionEvent> events, SessionEntry entry, DateTime date)
        {
            if (CanPushCons(events, entry, date))
            {
                if (govCheckBox.Checked)
                {
                    if (CanPushDividedExam(events, entry, date.AddDays(1)))
                    {
                        PushDividedEntry(events, entry, date.AddDays(1), date.AddDays(2), date);
                        return true;
                    }
                    else if (CanPushDividedExam(events, entry, date.AddDays(2)) && 
                        excludedDates.Contains(date.AddDays(1)))
                    {
                        PushDividedEntry(events, entry, date.AddDays(2), date.AddDays(3), date);
                        return true;
                    }
                }
                if (CanPushExam(events, entry, date.AddDays(1)))
                {
                    PushEntry(events, entry, date.AddDays(1), date);
                    return true;
                }
                else if (CanPushExam(events, entry, date.AddDays(2)) && 
                    excludedDates.Contains(date.AddDays(1)))
                {
                    PushEntry(events, entry, date.AddDays(2), date);
                    return true;
                }
            }
            return false;
        }

        string SelectAuditoryForCons(List<SessionEvent> events, SessionEntry entry, DateTime date)
        {
            List<string> consAuditoriesOnDate = GetBusyAuditoriesConsOnDate(events, date);
            List<string> examAuditoriesOnDate = GetBusyAuditoriesExamOnDate(events, date);
            foreach (var a in entry.Auditories)
            {
                int countCons = consAuditoriesOnDate.Count(p => p == a);
                int countExam = examAuditoriesOnDate.Count(p => p == a);
                if (countCons == 1 && countExam == 0) return a;
                if (countCons == 0 && countExam == 1) return a;
                if (countCons == 0 && countExam == 0) return a;
            }
            return "";
        }
        string SelectAuditoryForExam(List<SessionEvent> events, SessionEntry entry, DateTime date, string consAuditory)
        {
            List<string> consAuditoriesOnDate = GetBusyAuditoriesConsOnDate(events, date);
            List<string> examAuditoriesOnDate = GetBusyAuditoriesExamOnDate(events, date);
            if (examAuditoriesOnDate.All(p => p != consAuditory) &&
                consAuditoriesOnDate.Count(p => p == consAuditory) < 2)
                return consAuditory;
            foreach (var a in entry.Auditories)
            {
                int countCons = consAuditoriesOnDate.Count(p => p == a);
                int countExam = examAuditoriesOnDate.Count(p => p == a);
                if (countCons == 1 && countExam == 0) return a;
                //if (countCons == 0 && countExam == 1) return a;
                if (countCons == 0 && countExam == 0) return a;
            }
            return "";
        }
        bool CanPushCons(List<SessionEvent> events, SessionEntry entry, DateTime date)
        {
            if (excludedDates.Contains(date)) return false;

            if (!TestLecturerConditionsForConsultation(events, entry, date) ||
                !TestGroupConditionForConsultation(events, entry, date) ||
                !TestAuditoryConditionForConsultation(events, entry, date))
                return false;
            if (events.Any(p => p.DateTime.Date > date.Date && entry.NameGroup == p.Entry.NameGroup))
                return false;
            return true;
        }
        bool CanPushExam(List<SessionEvent> events, SessionEntry entry, DateTime date)
        {
            if (excludedDates.Contains(date)) return false;

            if (!TestLecturerConditionsForExam(events, entry, date) ||
                !TestGroupConditionForExam(events, entry, date)||
                !TestAuditoryConditionForExam(events, entry, date))
                return false;

            if (events.Any(p => p.DateTime.Date > date.Date && entry.NameGroup == p.Entry.NameGroup))
                return false;
            return true;
        }
        private void PushEntry(List<SessionEvent> events, SessionEntry entry, DateTime examDate, DateTime consDate)
        {
            var cons = new SessionEvent(entry, EventType.cons,
                                        new DateTime(consDate.Year, consDate.Month, consDate.Day,
                                                     events.Any(p => p.Entry.NameLecturer == entry.NameLecturer &&
                                                                     p.DateTime.Date == consDate.Date &&
                                                                     p.EventType == EventType.cons)
                                                         ? 9
                                                         : 8, 0, 0),

                                        SelectAuditoryForCons(events, entry, consDate));
            var exam = new SessionEvent(entry, EventType.exam,
                                        new DateTime(examDate.Year, examDate.Month, examDate.Day, 9, 0, 0),
                                        SelectAuditoryForExam(events, entry, examDate, cons.Auditory));
            events.Add(cons);
            events.Add(exam);
        }
        private void PushDividedEntry(List<SessionEvent> events, SessionEntry entry, DateTime examDate1, DateTime examDate2, DateTime consDate)
        {
            var cons = new SessionEvent(entry, EventType.cons,
                                              new DateTime(consDate.Year, consDate.Month, consDate.Day,
                                              events.Any(p => p.Entry.NameLecturer == entry.NameLecturer &&
                                              p.DateTime.Date == consDate.Date && p.EventType == EventType.cons) ? 9 : 8, 0, 0),
                                              SelectAuditoryForCons(events, entry, consDate));
            var exam1 = new SessionEvent(entry, EventType.exam,
                                         new DateTime(examDate1.Year, examDate1.Month, examDate1.Day, 9, 0, 0),
                                         SelectAuditoryForExam(events, entry, examDate1, cons.Auditory), 0);
            var exam2 = new SessionEvent(entry, EventType.exam,
                                         new DateTime(examDate2.Year, examDate2.Month, examDate2.Day, 9, 0, 0),
                                         SelectAuditoryForExam(events, entry, examDate2, cons.Auditory), 1);
            events.Add(cons);
            events.Add(exam1);
            events.Add(exam2);
        }
        private bool CanPushDividedExam(List<SessionEvent> events, SessionEntry entry, DateTime date)
        {
            return CanPushExam(events, entry, date) && CanPushExam(events, entry, date.AddDays(1));
        }
        List<string> GetGroups(IEnumerable<SessionEntry> entries)
        {
            List<string> g = new List<string>();
            for (int i = 0; i < entries.Count(); i++)
            {
                if (!g.Contains(entries.ElementAt(i).NameGroup))
                    g.Add(entries.ElementAt(i).NameGroup);
            }
            return g;
        }
        List<SessionEntry> GetGroupEntries(IEnumerable<SessionEntry> entries, string groupName)
        {
            return entries.Where(p => p.NameGroup == groupName).ToList();
        }
        
        bool TestLecturerConditionsForConsultation(List<SessionEvent> events, SessionEntry entry, DateTime date)
        {
            int eventsWithLecturer = events.Count(p => p.DateTime.Date == date && p.Entry.IdLecturer == entry.IdLecturer);
            if (testExistingExams)
            {
                eventsWithLecturer += examsInPeriod.Count(p => (p.ConsDate.Date == date || p.ExamDate.Date == date) && p.IdLecturer == entry.IdLecturer);
            }
            return eventsWithLecturer < 2;
        }
        bool TestLecturerConditionsForExam(List<SessionEvent> events, SessionEntry entry, DateTime date)
        {
            //if (events.Any(p => p.DateTime.Date == date &&
            //                    p.Entry.IdLecturer == entry.IdLecturer &&
            //                    p.EventType == EventType.exam))
            //    return false;
            //if (testExistingExams)
            //    if (examsInPeriod.Any(p => p.ExamDate.Date == date &&
            //                                p.IdLecturer == entry.IdLecturer))
            //        return false;

            //int eventsWithLecturer = events.Count(p => p.DateTime.Date == date && p.Entry.IdLecturer == entry.IdLecturer);
            //if (testExistingExams)
            //    eventsWithLecturer += examsInPeriod.Count(p => (p.ConsDate.Date == date || p.ExamDate.Date == date) && p.IdLecturer == entry.IdLecturer);
            //return eventsWithLecturer < 2;
            
            if (events.Any(p => p.DateTime.Date == date && p.EventType == EventType.exam &&
                               ( p.Entry.IdLecturer == entry.IdLecturer || 
                                p.Entry.Commission.Any(c => c == entry.NameLecturer || entry.Commission.Any( d => d == c))
                               ) ))
                return false;
            if (testExistingExams)
                if (examsInPeriod.Any(p => p.ExamDate.Date == date &&
                                            (p.IdLecturer == entry.IdLecturer ||
                                            p.Commission.Any(c => c == entry.NameLecturer || entry.Commission.Any(d => d == c))
                                            )))
                    return false;

            int eventsWithLecturer = events.Count(p => p.DateTime.Date == date && p.Entry.IdLecturer == entry.IdLecturer);
            if (testExistingExams)
                eventsWithLecturer += examsInPeriod.Count(p => (p.ConsDate.Date == date || p.ExamDate.Date == date) && p.IdLecturer == entry.IdLecturer);
            return eventsWithLecturer < 2;
        }
        bool TestGroupConditionForConsultation(List<SessionEvent> events, SessionEntry entry, DateTime date)
        {
            if (events.Any(p => p.DateTime.Date == date && p.Entry.IdGroup == entry.IdGroup)) return false;
            if (testExistingExams)
                if (examsInPeriod.Any(p => (p.ConsDate.Date == date || p.ExamDate.Date == date) &&
                                           p.IdGroup == entry.IdGroup))
                    return false;
            return true;
        }
        bool TestGroupConditionForExam(List<SessionEvent> events, SessionEntry entry, DateTime date)
        {
            if (events.Any(p => p.DateTime.Date == date && p.Entry.IdGroup == entry.IdGroup)) return false;
            if (events.Any(p => (p.DateTime.Date < date &&
                                 p.DateTime.Date >= date.AddDays((int) -daysNumericUpDown.Value)) &&
                                p.Entry.IdGroup == entry.IdGroup &&
                                p.EventType == EventType.exam)) return false;
            if (testExistingExams)
            {
                if (examsInPeriod.Any(p => (p.ConsDate.Date == date || p.ExamDate.Date == date) &&
                                           p.IdGroup == entry.IdGroup)) return false;
                if (examsInPeriod.Any(p => (p.ExamDate.Date < date && p.ExamDate.Date >= date.AddDays((int)-daysNumericUpDown.Value)) &&
                                           p.IdGroup == entry.IdGroup)) return false;
            }
            return true;
        }
        bool TestAuditoryConditionForConsultation(List<SessionEvent> events, SessionEntry entry, DateTime date)
        {
            List<string> consAuditoriesOnDate = GetBusyAuditoriesConsOnDate(events, date);
            List<string> examAuditoriesOnDate = GetBusyAuditoriesExamOnDate(events, date);
            foreach (var a in entry.Auditories)
            {
                int countCons = consAuditoriesOnDate.Count(p => p == a);
                int countExam = examAuditoriesOnDate.Count(p => p == a);
                if (countCons == 1 && countExam == 0) return true;
                if (countCons == 0 && countExam == 1) return true;
                if (countCons == 0 && countExam == 0) return true;
            }
            return false;
        }
        bool TestAuditoryConditionForExam(List<SessionEvent> events, SessionEntry entry, DateTime date)
        {
            List<string> consAuditoriesOnDate = GetBusyAuditoriesConsOnDate(events, date);
            List<string> examAuditoriesOnDate = GetBusyAuditoriesExamOnDate(events, date);
            foreach (var a in entry.Auditories)
            {
                int countCons = consAuditoriesOnDate.Count(p => p == a);
                int countExam = examAuditoriesOnDate.Count(p => p == a);
                if (countCons == 1 && countExam == 0) return true;
                if (countCons == 0 && countExam == 0) return true;
            }
            return false;
        }
        List<string> GetBusyAuditoriesConsOnDate(List<SessionEvent> events, DateTime date)
        {
            List<string> consAuditoriesOnDate = new List<string>();
            consAuditoriesOnDate.AddRange(events.Where(p => p.DateTime.Date == date && p.EventType == EventType.cons).Select(p => p.Auditory));
            if (testExistingExams)
                consAuditoriesOnDate.AddRange(
                    examsInPeriod.Where(e => e.ConsDate.Date == date && e.SubGroup == 0).Select(p => p.ConsAuditory));
            return consAuditoriesOnDate;
        }
        List<string> GetBusyAuditoriesExamOnDate(List<SessionEvent> events, DateTime date)
        {
            List<string> examAuditoriesOnDate = new List<string>();

            examAuditoriesOnDate.AddRange(events.Where(p => p.DateTime.Date == date && p.EventType == EventType.exam).Select(p => p.Auditory));
            if (testExistingExams)
                examAuditoriesOnDate.AddRange(
                    examsInPeriod.Where(e => e.ExamDate.Date == date).Select(p => p.ExamAuditory));
            return examAuditoriesOnDate;
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            if (bestEvents != null && bestEvents.Count > 0)
                CreateGeneralReport();
        }
        void CreateGeneralReport(bool design = false)
        {
            string years;
            if (start.Month > 10)
                years = start.Year.ToString() + "/" + (start.Year + 1).ToString();
            else
                years = (start.Year - 1).ToString() + "/" + (start.Year).ToString();
            bool winterSession = start.Month == 12 || start.Month == 1 ||
                            start.Month == 2;
            using (FastReport.Report report = new FastReport.Report())
            {
                if (design)
                    report.Load("../../Reports/ScheduleTemplate.frx");
                else
                    report.Load("Reports/ScheduleTemplate.frx");
                report.AutoFillDataSet = true;
                report.RegisterData(GenerateDataForReport(), "Entries");
                report.GetDataSource("Entries").Enabled = true;
                report.SetParameterValue("winterSession", winterSession);
                report.SetParameterValue("years", years);
                report.SetParameterValue("showSubgroups", govCheckBox.Checked);
                report.SetParameterValue("type", "preview");
                report.SetParameterValue("showCommission", govCheckBox.Checked);
                if (design)
                {
                    report.Design();
                    File.Copy("../../Reports/ScheduleTemplate.frx", "Reports/ScheduleTemplate.frx", true);
                }
                else
                    report.Show();
            }
        }
        IEnumerable<CreateScheduleForm.ReportSessionEntry> GenerateDataForReport()
        {
            List<CreateScheduleForm.ReportSessionEntry> data = new List<CreateScheduleForm.ReportSessionEntry>();
            int id = 0;
            foreach (var ev in bestEvents)
            {

                data.Add(new CreateScheduleForm.ReportSessionEntry()
                {
                    Id = id++,
                    NameLecturer = ev.Entry.NameLecturer,
                    NameGroup = ev.Entry.NameGroup,
                    NameSubject = ev.Entry.NameSubject,
                    EventType = ev.EventType.ToString(),
                    Date = ev.DateTime,
                    Auditory = ev.Auditory,
                    SubGroup = ev.Subgroup,
                    Commission = ev.Entry.CommissionStr
                });
            }
            if (data.Count() > 0)
            {
                DateTime minDate = data.Min(p => p.Date);
                DateTime maxDate = data.Max(p => p.Date);
                for (int i = 0; i < (int)((maxDate - minDate).TotalDays); i++)
                {
                    if (!data.Any(p => p.Date.Date == minDate.Date.AddDays(i)))
                        data.Add(new CreateScheduleForm.ReportSessionEntry()
                        {
                            Id = -1,
                            NameLecturer = "",
                            NameGroup = "",
                            NameSubject = "",
                            EventType = "",
                            Date = minDate.Date.AddDays(i),
                            Auditory = "",
                            ExamType = "",
                            SubGroup = 0
                        });
                }
            }
            return data;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (bestEvents != null && bestEvents.Count > 0)
                CreateGeneralReport(true);
        }

        #endregion

    }
}
