﻿namespace ExamSchedule.OtherForms.CreateSessionMaster
{
    partial class CreateSessionStepOne
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.NameGroupColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subjectNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lecturerNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.auditoriesColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comissionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idGroupColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idSubjectColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idLecturerColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.commissionListBox = new System.Windows.Forms.ListBox();
            this.commissionComboBox = new System.Windows.Forms.ComboBox();
            this.deleteCommissionButton = new System.Windows.Forms.Button();
            this.addCommissionButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.auditoriesTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.subjectCodeComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lecturerComboBox = new System.Windows.Forms.ComboBox();
            this.groupComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.subjectNameComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.departmentComboBox = new System.Windows.Forms.ComboBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.changeButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.NextButton = new System.Windows.Forms.Button();
            this.BackButton = new System.Windows.Forms.Button();
            this.stepOnePanel = new System.Windows.Forms.Panel();
            this.stepTwoPanel = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.excludedDatesListBox = new System.Windows.Forms.ListBox();
            this.excludeButton = new System.Windows.Forms.Button();
            this.monthCalendar = new System.Windows.Forms.MonthCalendar();
            this.includeButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.startDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.daysNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.govCheckBox = new System.Windows.Forms.CheckBox();
            this.stepThreePanel = new System.Windows.Forms.Panel();
            this.checkExistingCheckBox = new System.Windows.Forms.CheckBox();
            this.button3 = new System.Windows.Forms.Button();
            this.descrTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.solutionLabel = new System.Windows.Forms.Label();
            this.acceptButton = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.generationStartButton = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.stepOnePanel.SuspendLayout();
            this.stepTwoPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.daysNumericUpDown)).BeginInit();
            this.stepThreePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameGroupColumn,
            this.subjectNameColumn,
            this.lecturerNameColumn,
            this.auditoriesColumn,
            this.comissionColumn,
            this.idGroupColumn,
            this.idSubjectColumn,
            this.idLecturerColumn});
            this.dgv.Location = new System.Drawing.Point(0, 0);
            this.dgv.Margin = new System.Windows.Forms.Padding(8);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 24;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(1000, 67);
            this.dgv.TabIndex = 18;
            this.dgv.TabStop = false;
            this.dgv.SelectionChanged += new System.EventHandler(this.dgv_SelectionChanged);
            // 
            // NameGroupColumn
            // 
            this.NameGroupColumn.DataPropertyName = "NameGroup";
            this.NameGroupColumn.HeaderText = "Группа";
            this.NameGroupColumn.Name = "NameGroupColumn";
            this.NameGroupColumn.ReadOnly = true;
            this.NameGroupColumn.Width = 80;
            // 
            // subjectNameColumn
            // 
            this.subjectNameColumn.DataPropertyName = "NameSubject";
            this.subjectNameColumn.HeaderText = "Предмет";
            this.subjectNameColumn.Name = "subjectNameColumn";
            this.subjectNameColumn.ReadOnly = true;
            this.subjectNameColumn.Width = 300;
            // 
            // lecturerNameColumn
            // 
            this.lecturerNameColumn.DataPropertyName = "NameLecturer";
            this.lecturerNameColumn.HeaderText = "Преподаватель";
            this.lecturerNameColumn.Name = "lecturerNameColumn";
            this.lecturerNameColumn.ReadOnly = true;
            this.lecturerNameColumn.Width = 250;
            // 
            // auditoriesColumn
            // 
            this.auditoriesColumn.DataPropertyName = "AuditoriesString";
            this.auditoriesColumn.HeaderText = "Аудитории";
            this.auditoriesColumn.Name = "auditoriesColumn";
            this.auditoriesColumn.ReadOnly = true;
            this.auditoriesColumn.Width = 120;
            // 
            // comissionColumn
            // 
            this.comissionColumn.DataPropertyName = "CommissionStr";
            this.comissionColumn.HeaderText = "Комиссия";
            this.comissionColumn.Name = "comissionColumn";
            this.comissionColumn.ReadOnly = true;
            this.comissionColumn.Width = 300;
            // 
            // idGroupColumn
            // 
            this.idGroupColumn.DataPropertyName = "idGroup";
            this.idGroupColumn.HeaderText = "idGroup";
            this.idGroupColumn.Name = "idGroupColumn";
            this.idGroupColumn.ReadOnly = true;
            this.idGroupColumn.Visible = false;
            // 
            // idSubjectColumn
            // 
            this.idSubjectColumn.DataPropertyName = "idSubject";
            this.idSubjectColumn.HeaderText = "idSubject";
            this.idSubjectColumn.Name = "idSubjectColumn";
            this.idSubjectColumn.ReadOnly = true;
            this.idSubjectColumn.Visible = false;
            // 
            // idLecturerColumn
            // 
            this.idLecturerColumn.DataPropertyName = "idLecturer";
            this.idLecturerColumn.HeaderText = "idLecturer";
            this.idLecturerColumn.Name = "idLecturerColumn";
            this.idLecturerColumn.ReadOnly = true;
            this.idLecturerColumn.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.groupBox1.Controls.Add(this.commissionListBox);
            this.groupBox1.Controls.Add(this.commissionComboBox);
            this.groupBox1.Controls.Add(this.deleteCommissionButton);
            this.groupBox1.Controls.Add(this.addCommissionButton);
            this.groupBox1.Location = new System.Drawing.Point(545, 79);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(346, 215);
            this.groupBox1.TabIndex = 44;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Комиссия";
            // 
            // commissionListBox
            // 
            this.commissionListBox.FormattingEnabled = true;
            this.commissionListBox.ItemHeight = 20;
            this.commissionListBox.Location = new System.Drawing.Point(6, 61);
            this.commissionListBox.Name = "commissionListBox";
            this.commissionListBox.Size = new System.Drawing.Size(291, 144);
            this.commissionListBox.TabIndex = 30;
            this.commissionListBox.TabStop = false;
            // 
            // commissionComboBox
            // 
            this.commissionComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.commissionComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.commissionComboBox.DisplayMember = "Name";
            this.commissionComboBox.FormattingEnabled = true;
            this.commissionComboBox.Location = new System.Drawing.Point(6, 28);
            this.commissionComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.commissionComboBox.Name = "commissionComboBox";
            this.commissionComboBox.Size = new System.Drawing.Size(332, 28);
            this.commissionComboBox.TabIndex = 28;
            this.commissionComboBox.TabStop = false;
            this.commissionComboBox.ValueMember = "Id";
            // 
            // deleteCommissionButton
            // 
            this.deleteCommissionButton.Location = new System.Drawing.Point(303, 104);
            this.deleteCommissionButton.Name = "deleteCommissionButton";
            this.deleteCommissionButton.Size = new System.Drawing.Size(35, 35);
            this.deleteCommissionButton.TabIndex = 32;
            this.deleteCommissionButton.TabStop = false;
            this.deleteCommissionButton.Text = "-";
            this.deleteCommissionButton.UseVisualStyleBackColor = true;
            this.deleteCommissionButton.Click += new System.EventHandler(this.deleteCommissionButton_Click);
            // 
            // addCommissionButton
            // 
            this.addCommissionButton.Location = new System.Drawing.Point(303, 63);
            this.addCommissionButton.Name = "addCommissionButton";
            this.addCommissionButton.Size = new System.Drawing.Size(35, 35);
            this.addCommissionButton.TabIndex = 31;
            this.addCommissionButton.TabStop = false;
            this.addCommissionButton.Text = "+";
            this.addCommissionButton.UseVisualStyleBackColor = true;
            this.addCommissionButton.Click += new System.EventHandler(this.addCommissionButton_Click);
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(107, 227);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(204, 20);
            this.label8.TabIndex = 43;
            this.label8.Text = "Доступные аудитории";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // auditoriesTextBox
            // 
            this.auditoriesTextBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.auditoriesTextBox.Location = new System.Drawing.Point(325, 224);
            this.auditoriesTextBox.Name = "auditoriesTextBox";
            this.auditoriesTextBox.Size = new System.Drawing.Size(213, 28);
            this.auditoriesTextBox.TabIndex = 39;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(104, 154);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 20);
            this.label6.TabIndex = 42;
            this.label6.Text = "Код предмета";
            // 
            // subjectCodeComboBox
            // 
            this.subjectCodeComboBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.subjectCodeComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.subjectCodeComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.subjectCodeComboBox.DisplayMember = "Code";
            this.subjectCodeComboBox.FormattingEnabled = true;
            this.subjectCodeComboBox.Location = new System.Drawing.Point(262, 151);
            this.subjectCodeComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.subjectCodeComboBox.Name = "subjectCodeComboBox";
            this.subjectCodeComboBox.Size = new System.Drawing.Size(276, 28);
            this.subjectCodeComboBox.TabIndex = 36;
            this.subjectCodeComboBox.ValueMember = "Id";
            this.subjectCodeComboBox.SelectedIndexChanged += new System.EventHandler(this.codeComboBox_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(107, 192);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 20);
            this.label4.TabIndex = 41;
            this.label4.Text = "Преподаватель";
            // 
            // lecturerComboBox
            // 
            this.lecturerComboBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lecturerComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.lecturerComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.lecturerComboBox.DisplayMember = "Name";
            this.lecturerComboBox.FormattingEnabled = true;
            this.lecturerComboBox.Location = new System.Drawing.Point(262, 189);
            this.lecturerComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.lecturerComboBox.Name = "lecturerComboBox";
            this.lecturerComboBox.Size = new System.Drawing.Size(276, 28);
            this.lecturerComboBox.TabIndex = 38;
            this.lecturerComboBox.ValueMember = "Id";
            // 
            // groupComboBox
            // 
            this.groupComboBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.groupComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.groupComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.groupComboBox.DisplayMember = "Name";
            this.groupComboBox.FormattingEnabled = true;
            this.groupComboBox.Location = new System.Drawing.Point(431, 79);
            this.groupComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.groupComboBox.Name = "groupComboBox";
            this.groupComboBox.Size = new System.Drawing.Size(107, 28);
            this.groupComboBox.TabIndex = 34;
            this.groupComboBox.ValueMember = "Id";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(351, 82);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 20);
            this.label3.TabIndex = 40;
            this.label3.Text = "Группа";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(107, 118);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 20);
            this.label2.TabIndex = 37;
            this.label2.Text = "Предмет";
            // 
            // subjectNameComboBox
            // 
            this.subjectNameComboBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.subjectNameComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.subjectNameComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.subjectNameComboBox.DisplayMember = "Name";
            this.subjectNameComboBox.FormattingEnabled = true;
            this.subjectNameComboBox.Location = new System.Drawing.Point(262, 115);
            this.subjectNameComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.subjectNameComboBox.Name = "subjectNameComboBox";
            this.subjectNameComboBox.Size = new System.Drawing.Size(276, 28);
            this.subjectNameComboBox.TabIndex = 35;
            this.subjectNameComboBox.ValueMember = "Id";
            this.subjectNameComboBox.SelectedIndexChanged += new System.EventHandler(this.nameСomboBox_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(104, 82);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 20);
            this.label1.TabIndex = 46;
            this.label1.Text = "Отделение";
            // 
            // departmentComboBox
            // 
            this.departmentComboBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.departmentComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.departmentComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.departmentComboBox.DisplayMember = "Department";
            this.departmentComboBox.FormattingEnabled = true;
            this.departmentComboBox.Location = new System.Drawing.Point(225, 79);
            this.departmentComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.departmentComboBox.Name = "departmentComboBox";
            this.departmentComboBox.Size = new System.Drawing.Size(107, 28);
            this.departmentComboBox.TabIndex = 45;
            this.departmentComboBox.ValueMember = "Id";
            this.departmentComboBox.SelectedIndexChanged += new System.EventHandler(this.departmentComboBox_SelectedIndexChanged);
            // 
            // deleteButton
            // 
            this.deleteButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.deleteButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.deleteButton.Location = new System.Drawing.Point(431, 259);
            this.deleteButton.Margin = new System.Windows.Forms.Padding(4);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(106, 35);
            this.deleteButton.TabIndex = 49;
            this.deleteButton.Text = "Убрать";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // changeButton
            // 
            this.changeButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.changeButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.changeButton.Location = new System.Drawing.Point(284, 259);
            this.changeButton.Margin = new System.Windows.Forms.Padding(4);
            this.changeButton.Name = "changeButton";
            this.changeButton.Size = new System.Drawing.Size(139, 35);
            this.changeButton.TabIndex = 48;
            this.changeButton.Text = "Изменить";
            this.changeButton.UseVisualStyleBackColor = true;
            this.changeButton.Click += new System.EventHandler(this.changeButton_Click);
            // 
            // addButton
            // 
            this.addButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.addButton.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.addButton.Location = new System.Drawing.Point(110, 259);
            this.addButton.Margin = new System.Windows.Forms.Padding(4);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(166, 35);
            this.addButton.TabIndex = 47;
            this.addButton.Text = "Добавить";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // NextButton
            // 
            this.NextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.NextButton.Location = new System.Drawing.Point(1194, 622);
            this.NextButton.Name = "NextButton";
            this.NextButton.Size = new System.Drawing.Size(97, 35);
            this.NextButton.TabIndex = 50;
            this.NextButton.Text = "Далее";
            this.NextButton.UseVisualStyleBackColor = true;
            this.NextButton.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // BackButton
            // 
            this.BackButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BackButton.Location = new System.Drawing.Point(1078, 622);
            this.BackButton.Name = "BackButton";
            this.BackButton.Size = new System.Drawing.Size(110, 35);
            this.BackButton.TabIndex = 51;
            this.BackButton.Text = "Назад";
            this.BackButton.UseVisualStyleBackColor = true;
            this.BackButton.Click += new System.EventHandler(this.BackButton_Click);
            // 
            // stepOnePanel
            // 
            this.stepOnePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.stepOnePanel.Controls.Add(this.dgv);
            this.stepOnePanel.Controls.Add(this.departmentComboBox);
            this.stepOnePanel.Controls.Add(this.subjectNameComboBox);
            this.stepOnePanel.Controls.Add(this.deleteButton);
            this.stepOnePanel.Controls.Add(this.label2);
            this.stepOnePanel.Controls.Add(this.changeButton);
            this.stepOnePanel.Controls.Add(this.label3);
            this.stepOnePanel.Controls.Add(this.addButton);
            this.stepOnePanel.Controls.Add(this.groupComboBox);
            this.stepOnePanel.Controls.Add(this.label1);
            this.stepOnePanel.Controls.Add(this.lecturerComboBox);
            this.stepOnePanel.Controls.Add(this.label4);
            this.stepOnePanel.Controls.Add(this.groupBox1);
            this.stepOnePanel.Controls.Add(this.subjectCodeComboBox);
            this.stepOnePanel.Controls.Add(this.label8);
            this.stepOnePanel.Controls.Add(this.label6);
            this.stepOnePanel.Controls.Add(this.auditoriesTextBox);
            this.stepOnePanel.Location = new System.Drawing.Point(43, 334);
            this.stepOnePanel.Name = "stepOnePanel";
            this.stepOnePanel.Size = new System.Drawing.Size(1000, 298);
            this.stepOnePanel.TabIndex = 0;
            // 
            // stepTwoPanel
            // 
            this.stepTwoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.stepTwoPanel.Controls.Add(this.label10);
            this.stepTwoPanel.Controls.Add(this.excludedDatesListBox);
            this.stepTwoPanel.Controls.Add(this.excludeButton);
            this.stepTwoPanel.Controls.Add(this.monthCalendar);
            this.stepTwoPanel.Controls.Add(this.includeButton);
            this.stepTwoPanel.Controls.Add(this.label5);
            this.stepTwoPanel.Controls.Add(this.startDatePicker);
            this.stepTwoPanel.Controls.Add(this.label9);
            this.stepTwoPanel.Controls.Add(this.daysNumericUpDown);
            this.stepTwoPanel.Location = new System.Drawing.Point(781, 9);
            this.stepTwoPanel.Margin = new System.Windows.Forms.Padding(0);
            this.stepTwoPanel.Name = "stepTwoPanel";
            this.stepTwoPanel.Size = new System.Drawing.Size(510, 320);
            this.stepTwoPanel.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(157, 84);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(171, 20);
            this.label10.TabIndex = 64;
            this.label10.Text = "Исключенные дни";
            // 
            // excludedDatesListBox
            // 
            this.excludedDatesListBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.excludedDatesListBox.FormattingEnabled = true;
            this.excludedDatesListBox.ItemHeight = 20;
            this.excludedDatesListBox.Location = new System.Drawing.Point(276, 113);
            this.excludedDatesListBox.Name = "excludedDatesListBox";
            this.excludedDatesListBox.Size = new System.Drawing.Size(222, 204);
            this.excludedDatesListBox.TabIndex = 59;
            // 
            // excludeButton
            // 
            this.excludeButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.excludeButton.Location = new System.Drawing.Point(215, 113);
            this.excludeButton.Name = "excludeButton";
            this.excludeButton.Size = new System.Drawing.Size(55, 56);
            this.excludeButton.TabIndex = 60;
            this.excludeButton.Text = "+>";
            this.excludeButton.UseVisualStyleBackColor = true;
            this.excludeButton.Click += new System.EventHandler(this.excludeButton_Click);
            // 
            // monthCalendar
            // 
            this.monthCalendar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.monthCalendar.Location = new System.Drawing.Point(11, 113);
            this.monthCalendar.MaxSelectionCount = 1;
            this.monthCalendar.Name = "monthCalendar";
            this.monthCalendar.TabIndex = 63;
            // 
            // includeButton
            // 
            this.includeButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.includeButton.Location = new System.Drawing.Point(215, 175);
            this.includeButton.Name = "includeButton";
            this.includeButton.Size = new System.Drawing.Size(55, 56);
            this.includeButton.TabIndex = 61;
            this.includeButton.Text = "X";
            this.includeButton.UseVisualStyleBackColor = true;
            this.includeButton.Click += new System.EventHandler(this.includeButton_Click);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(46, 10);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 20);
            this.label5.TabIndex = 58;
            this.label5.Text = "Начало сессии";
            // 
            // startDatePicker
            // 
            this.startDatePicker.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.startDatePicker.Location = new System.Drawing.Point(192, 4);
            this.startDatePicker.Margin = new System.Windows.Forms.Padding(4);
            this.startDatePicker.Name = "startDatePicker";
            this.startDatePicker.Size = new System.Drawing.Size(248, 28);
            this.startDatePicker.TabIndex = 57;
            this.startDatePicker.TabStop = false;
            this.startDatePicker.ValueChanged += new System.EventHandler(this.DropOnValueChanged);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(108, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(226, 20);
            this.label9.TabIndex = 56;
            this.label9.Text = "Дней между экзаменами";
            // 
            // daysNumericUpDown
            // 
            this.daysNumericUpDown.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.daysNumericUpDown.Location = new System.Drawing.Point(340, 54);
            this.daysNumericUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.daysNumericUpDown.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.daysNumericUpDown.Name = "daysNumericUpDown";
            this.daysNumericUpDown.Size = new System.Drawing.Size(52, 28);
            this.daysNumericUpDown.TabIndex = 55;
            this.daysNumericUpDown.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.daysNumericUpDown.ValueChanged += new System.EventHandler(this.DropOnValueChanged);
            // 
            // govCheckBox
            // 
            this.govCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.govCheckBox.AutoSize = true;
            this.govCheckBox.Location = new System.Drawing.Point(149, 101);
            this.govCheckBox.Name = "govCheckBox";
            this.govCheckBox.Size = new System.Drawing.Size(398, 24);
            this.govCheckBox.TabIndex = 62;
            this.govCheckBox.TabStop = false;
            this.govCheckBox.Text = "Государственная сессия (с подгруппами)";
            this.govCheckBox.UseVisualStyleBackColor = true;
            this.govCheckBox.CheckedChanged += new System.EventHandler(this.DropOnValueChanged);
            // 
            // stepThreePanel
            // 
            this.stepThreePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.stepThreePanel.Controls.Add(this.button1);
            this.stepThreePanel.Controls.Add(this.checkExistingCheckBox);
            this.stepThreePanel.Controls.Add(this.button3);
            this.stepThreePanel.Controls.Add(this.descrTextBox);
            this.stepThreePanel.Controls.Add(this.label7);
            this.stepThreePanel.Controls.Add(this.govCheckBox);
            this.stepThreePanel.Controls.Add(this.solutionLabel);
            this.stepThreePanel.Controls.Add(this.acceptButton);
            this.stepThreePanel.Controls.Add(this.progressBar1);
            this.stepThreePanel.Controls.Add(this.generationStartButton);
            this.stepThreePanel.Location = new System.Drawing.Point(12, 12);
            this.stepThreePanel.Name = "stepThreePanel";
            this.stepThreePanel.Size = new System.Drawing.Size(709, 316);
            this.stepThreePanel.TabIndex = 52;
            // 
            // checkExistingCheckBox
            // 
            this.checkExistingCheckBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkExistingCheckBox.AutoSize = true;
            this.checkExistingCheckBox.Checked = true;
            this.checkExistingCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkExistingCheckBox.Location = new System.Drawing.Point(149, 71);
            this.checkExistingCheckBox.Name = "checkExistingCheckBox";
            this.checkExistingCheckBox.Size = new System.Drawing.Size(402, 24);
            this.checkExistingCheckBox.TabIndex = 67;
            this.checkExistingCheckBox.Text = "Брать во внимание существующие сессии";
            this.checkExistingCheckBox.UseVisualStyleBackColor = true;
            this.checkExistingCheckBox.CheckedChanged += new System.EventHandler(this.DropOnValueChanged);
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button3.Location = new System.Drawing.Point(323, 206);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(161, 38);
            this.button3.TabIndex = 66;
            this.button3.Text = "Предпросмотр";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // descrTextBox
            // 
            this.descrTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.descrTextBox.Location = new System.Drawing.Point(272, 37);
            this.descrTextBox.Name = "descrTextBox";
            this.descrTextBox.Size = new System.Drawing.Size(279, 28);
            this.descrTextBox.TabIndex = 64;
            this.descrTextBox.TabStop = false;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(145, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 20);
            this.label7.TabIndex = 65;
            this.label7.Text = "Описание";
            // 
            // solutionLabel
            // 
            this.solutionLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.solutionLabel.Location = new System.Drawing.Point(31, 150);
            this.solutionLabel.Name = "solutionLabel";
            this.solutionLabel.Size = new System.Drawing.Size(647, 23);
            this.solutionLabel.TabIndex = 62;
            this.solutionLabel.Text = "Решение не найдено";
            this.solutionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // acceptButton
            // 
            this.acceptButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.acceptButton.Enabled = false;
            this.acceptButton.Location = new System.Drawing.Point(491, 205);
            this.acceptButton.Name = "acceptButton";
            this.acceptButton.Size = new System.Drawing.Size(187, 38);
            this.acceptButton.TabIndex = 63;
            this.acceptButton.Text = "Принять решение";
            this.acceptButton.UseVisualStyleBackColor = true;
            this.acceptButton.Click += new System.EventHandler(this.acceptSolutionButton_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.progressBar1.Location = new System.Drawing.Point(31, 176);
            this.progressBar1.MarqueeAnimationSpeed = 0;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(647, 23);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 61;
            // 
            // generationStartButton
            // 
            this.generationStartButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.generationStartButton.Location = new System.Drawing.Point(31, 206);
            this.generationStartButton.Margin = new System.Windows.Forms.Padding(4);
            this.generationStartButton.Name = "generationStartButton";
            this.generationStartButton.Size = new System.Drawing.Size(284, 38);
            this.generationStartButton.TabIndex = 60;
            this.generationStartButton.Text = "Начать генерацию";
            this.generationStartButton.UseVisualStyleBackColor = true;
            this.generationStartButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.Location = new System.Drawing.Point(409, 251);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 31);
            this.button1.TabIndex = 68;
            this.button1.Text = "edit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // CreateSessionStepOne
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1303, 669);
            this.Controls.Add(this.stepThreePanel);
            this.Controls.Add(this.stepTwoPanel);
            this.Controls.Add(this.stepOnePanel);
            this.Controls.Add(this.BackButton);
            this.Controls.Add(this.NextButton);
            this.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CreateSessionStepOne";
            this.Text = "CreateSessionStepOne";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_FormClosing);
            this.Load += new System.EventHandler(this.Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.stepOnePanel.ResumeLayout(false);
            this.stepOnePanel.PerformLayout();
            this.stepTwoPanel.ResumeLayout(false);
            this.stepTwoPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.daysNumericUpDown)).EndInit();
            this.stepThreePanel.ResumeLayout(false);
            this.stepThreePanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameGroupColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn subjectNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lecturerNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn auditoriesColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn comissionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idGroupColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSubjectColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idLecturerColumn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox commissionListBox;
        private System.Windows.Forms.ComboBox commissionComboBox;
        private System.Windows.Forms.Button deleteCommissionButton;
        private System.Windows.Forms.Button addCommissionButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox auditoriesTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox subjectCodeComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox lecturerComboBox;
        private System.Windows.Forms.ComboBox groupComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox subjectNameComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox departmentComboBox;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button changeButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button NextButton;
        private System.Windows.Forms.Button BackButton;
        private System.Windows.Forms.Panel stepOnePanel;
        private System.Windows.Forms.Panel stepTwoPanel;
        private System.Windows.Forms.ListBox excludedDatesListBox;
        private System.Windows.Forms.Button excludeButton;
        private System.Windows.Forms.MonthCalendar monthCalendar;
        private System.Windows.Forms.Button includeButton;
        private System.Windows.Forms.CheckBox govCheckBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker startDatePicker;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown daysNumericUpDown;
        private System.Windows.Forms.Panel stepThreePanel;
        private System.Windows.Forms.CheckBox checkExistingCheckBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox descrTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label solutionLabel;
        private System.Windows.Forms.Button acceptButton;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Button generationStartButton;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button1;
    }
}