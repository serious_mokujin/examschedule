﻿namespace ExamSchedule
{
    partial class CreateScheduleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateScheduleForm));
            this.courseNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupComboBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.lecturerComboBox = new System.Windows.Forms.ComboBox();
            this.button6 = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.groupTabPage = new System.Windows.Forms.TabPage();
            this.lecturerTabPage = new System.Windows.Forms.TabPage();
            this.courseTabPage = new System.Windows.Forms.TabPage();
            this.dateTabPage = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.courseNumericUpDown)).BeginInit();
            this.tabControl.SuspendLayout();
            this.groupTabPage.SuspendLayout();
            this.lecturerTabPage.SuspendLayout();
            this.courseTabPage.SuspendLayout();
            this.dateTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // courseNumericUpDown
            // 
            this.courseNumericUpDown.Location = new System.Drawing.Point(204, 7);
            this.courseNumericUpDown.Margin = new System.Windows.Forms.Padding(4);
            this.courseNumericUpDown.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.courseNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.courseNumericUpDown.Name = "courseNumericUpDown";
            this.courseNumericUpDown.Size = new System.Drawing.Size(203, 28);
            this.courseNumericUpDown.TabIndex = 45;
            this.courseNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(143, 9);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 20);
            this.label6.TabIndex = 44;
            this.label6.Text = "Курс";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(137, 11);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 20);
            this.label9.TabIndex = 47;
            this.label9.Text = "Группа";
            // 
            // groupComboBox
            // 
            this.groupComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.groupComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.groupComboBox.BackColor = System.Drawing.SystemColors.Window;
            this.groupComboBox.DisplayMember = "Name";
            this.groupComboBox.FormattingEnabled = true;
            this.groupComboBox.Location = new System.Drawing.Point(217, 8);
            this.groupComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupComboBox.Name = "groupComboBox";
            this.groupComboBox.Size = new System.Drawing.Size(173, 28);
            this.groupComboBox.TabIndex = 46;
            this.groupComboBox.ValueMember = "Id";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(270, 81);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(229, 36);
            this.button1.TabIndex = 50;
            this.button1.Text = "Сформировать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 20);
            this.label4.TabIndex = 52;
            this.label4.Text = "Преподаватель";
            // 
            // lecturerComboBox
            // 
            this.lecturerComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.lecturerComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.lecturerComboBox.DisplayMember = "Name";
            this.lecturerComboBox.FormattingEnabled = true;
            this.lecturerComboBox.Location = new System.Drawing.Point(161, 7);
            this.lecturerComboBox.Margin = new System.Windows.Forms.Padding(4);
            this.lecturerComboBox.Name = "lecturerComboBox";
            this.lecturerComboBox.Size = new System.Drawing.Size(392, 28);
            this.lecturerComboBox.TabIndex = 51;
            this.lecturerComboBox.ValueMember = "Id";
            // 
            // button6
            // 
            this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button6.Location = new System.Drawing.Point(505, 81);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(53, 36);
            this.button6.TabIndex = 57;
            this.button6.Text = "edit";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.groupTabPage);
            this.tabControl.Controls.Add(this.lecturerTabPage);
            this.tabControl.Controls.Add(this.courseTabPage);
            this.tabControl.Controls.Add(this.dateTabPage);
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(571, 75);
            this.tabControl.TabIndex = 58;
            // 
            // groupTabPage
            // 
            this.groupTabPage.Controls.Add(this.groupComboBox);
            this.groupTabPage.Controls.Add(this.label9);
            this.groupTabPage.Location = new System.Drawing.Point(4, 29);
            this.groupTabPage.Name = "groupTabPage";
            this.groupTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.groupTabPage.Size = new System.Drawing.Size(563, 42);
            this.groupTabPage.TabIndex = 0;
            this.groupTabPage.Text = "Для группы";
            this.groupTabPage.UseVisualStyleBackColor = true;
            // 
            // lecturerTabPage
            // 
            this.lecturerTabPage.Controls.Add(this.label4);
            this.lecturerTabPage.Controls.Add(this.lecturerComboBox);
            this.lecturerTabPage.Location = new System.Drawing.Point(4, 29);
            this.lecturerTabPage.Name = "lecturerTabPage";
            this.lecturerTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.lecturerTabPage.Size = new System.Drawing.Size(563, 42);
            this.lecturerTabPage.TabIndex = 1;
            this.lecturerTabPage.Text = "Для преподавателя";
            this.lecturerTabPage.UseVisualStyleBackColor = true;
            // 
            // courseTabPage
            // 
            this.courseTabPage.Controls.Add(this.courseNumericUpDown);
            this.courseTabPage.Controls.Add(this.label6);
            this.courseTabPage.Location = new System.Drawing.Point(4, 29);
            this.courseTabPage.Name = "courseTabPage";
            this.courseTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.courseTabPage.Size = new System.Drawing.Size(563, 42);
            this.courseTabPage.TabIndex = 2;
            this.courseTabPage.Text = "Для курса";
            this.courseTabPage.UseVisualStyleBackColor = true;
            // 
            // dateTabPage
            // 
            this.dateTabPage.Controls.Add(this.label1);
            this.dateTabPage.Controls.Add(this.dateTimePicker);
            this.dateTabPage.Location = new System.Drawing.Point(4, 29);
            this.dateTabPage.Name = "dateTabPage";
            this.dateTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.dateTabPage.Size = new System.Drawing.Size(563, 42);
            this.dateTabPage.TabIndex = 3;
            this.dateTabPage.Text = "На дату";
            this.dateTabPage.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(144, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Дата";
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.Location = new System.Drawing.Point(200, 6);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(200, 28);
            this.dateTimePicker.TabIndex = 0;
            // 
            // CreateScheduleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 129);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateScheduleForm";
            this.Text = "CreateScheduleForm";
            this.Load += new System.EventHandler(this.CreateScheduleForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.courseNumericUpDown)).EndInit();
            this.tabControl.ResumeLayout(false);
            this.groupTabPage.ResumeLayout(false);
            this.groupTabPage.PerformLayout();
            this.lecturerTabPage.ResumeLayout(false);
            this.lecturerTabPage.PerformLayout();
            this.courseTabPage.ResumeLayout(false);
            this.courseTabPage.PerformLayout();
            this.dateTabPage.ResumeLayout(false);
            this.dateTabPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NumericUpDown courseNumericUpDown;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox groupComboBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox lecturerComboBox;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage groupTabPage;
        private System.Windows.Forms.TabPage lecturerTabPage;
        private System.Windows.Forms.TabPage courseTabPage;
        private System.Windows.Forms.TabPage dateTabPage;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker;

    }
}