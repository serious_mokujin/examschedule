﻿namespace ExamSchedule.OtherForms
{
    partial class SessionReviewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SessionReviewForm));
            this.dgv = new System.Windows.Forms.DataGridView();
            this.descrTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.govCheckBox = new System.Windows.Forms.CheckBox();
            this.generateScheduleButton = new System.Windows.Forms.Button();
            this.reportButton = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.NameGroupColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subgroupColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subjectNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lecturerNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comissionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.examDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idGroupColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idLecturerColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idSubjectColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameGroupColumn,
            this.subgroupColumn,
            this.subjectNameColumn,
            this.lecturerNameColumn,
            this.comissionColumn,
            this.examDateColumn,
            this.idGroupColumn,
            this.Column1,
            this.idLecturerColumn,
            this.idSubjectColumn});
            this.dgv.Location = new System.Drawing.Point(0, 0);
            this.dgv.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.RowTemplate.Height = 24;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(1097, 400);
            this.dgv.TabIndex = 18;
            this.dgv.TabStop = false;
            // 
            // descrTextBox
            // 
            this.descrTextBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.descrTextBox.Location = new System.Drawing.Point(113, 412);
            this.descrTextBox.MaxLength = 80;
            this.descrTextBox.Name = "descrTextBox";
            this.descrTextBox.Size = new System.Drawing.Size(426, 28);
            this.descrTextBox.TabIndex = 35;
            this.descrTextBox.TabStop = false;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 415);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(95, 20);
            this.label7.TabIndex = 36;
            this.label7.Text = "Описание";
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button1.Location = new System.Drawing.Point(12, 447);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(234, 30);
            this.button1.TabIndex = 38;
            this.button1.Text = "Создать на основе этой";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button2.Location = new System.Drawing.Point(947, 413);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(73, 30);
            this.button2.TabIndex = 39;
            this.button2.Text = "Итоги";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button3.Location = new System.Drawing.Point(773, 413);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(168, 30);
            this.button3.TabIndex = 40;
            this.button3.Text = "Ввести оценки";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // govCheckBox
            // 
            this.govCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.govCheckBox.AutoCheck = false;
            this.govCheckBox.AutoSize = true;
            this.govCheckBox.Location = new System.Drawing.Point(545, 414);
            this.govCheckBox.Name = "govCheckBox";
            this.govCheckBox.Size = new System.Drawing.Size(181, 24);
            this.govCheckBox.TabIndex = 41;
            this.govCheckBox.Text = "Государственная";
            this.govCheckBox.UseVisualStyleBackColor = true;
            // 
            // generateScheduleButton
            // 
            this.generateScheduleButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.generateScheduleButton.Location = new System.Drawing.Point(252, 447);
            this.generateScheduleButton.Name = "generateScheduleButton";
            this.generateScheduleButton.Size = new System.Drawing.Size(312, 30);
            this.generateScheduleButton.TabIndex = 42;
            this.generateScheduleButton.Text = "Сформировать расписание";
            this.generateScheduleButton.UseVisualStyleBackColor = true;
            this.generateScheduleButton.Click += new System.EventHandler(this.generateSchedule);
            // 
            // reportButton
            // 
            this.reportButton.Location = new System.Drawing.Point(773, 449);
            this.reportButton.Name = "reportButton";
            this.reportButton.Size = new System.Drawing.Size(247, 28);
            this.reportButton.TabIndex = 43;
            this.reportButton.Text = "Составить ведомость";
            this.reportButton.UseVisualStyleBackColor = true;
            this.reportButton.Click += new System.EventHandler(this.reportButton_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1026, 449);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(59, 28);
            this.button4.TabIndex = 44;
            this.button4.Text = "edit";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(1026, 414);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(59, 28);
            this.button5.TabIndex = 45;
            this.button5.Text = "edit";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // NameGroupColumn
            // 
            this.NameGroupColumn.DataPropertyName = "nameGroup";
            this.NameGroupColumn.HeaderText = "Группа";
            this.NameGroupColumn.Name = "NameGroupColumn";
            this.NameGroupColumn.ReadOnly = true;
            this.NameGroupColumn.Width = 80;
            // 
            // subgroupColumn
            // 
            this.subgroupColumn.DataPropertyName = "subGroup";
            this.subgroupColumn.HeaderText = "Подгр.";
            this.subgroupColumn.Name = "subgroupColumn";
            this.subgroupColumn.ReadOnly = true;
            this.subgroupColumn.Width = 80;
            // 
            // subjectNameColumn
            // 
            this.subjectNameColumn.DataPropertyName = "nameSubject";
            this.subjectNameColumn.HeaderText = "Предмет";
            this.subjectNameColumn.Name = "subjectNameColumn";
            this.subjectNameColumn.ReadOnly = true;
            this.subjectNameColumn.Width = 300;
            // 
            // lecturerNameColumn
            // 
            this.lecturerNameColumn.DataPropertyName = "nameLecturer";
            this.lecturerNameColumn.HeaderText = "Преподаватель";
            this.lecturerNameColumn.Name = "lecturerNameColumn";
            this.lecturerNameColumn.ReadOnly = true;
            this.lecturerNameColumn.Width = 250;
            // 
            // comissionColumn
            // 
            this.comissionColumn.DataPropertyName = "commissionExam";
            this.comissionColumn.HeaderText = "Комиссия";
            this.comissionColumn.Name = "comissionColumn";
            this.comissionColumn.ReadOnly = true;
            this.comissionColumn.Width = 250;
            // 
            // examDateColumn
            // 
            this.examDateColumn.DataPropertyName = "examDate";
            this.examDateColumn.HeaderText = "Дата экзамена";
            this.examDateColumn.Name = "examDateColumn";
            this.examDateColumn.ReadOnly = true;
            this.examDateColumn.Width = 130;
            // 
            // idGroupColumn
            // 
            this.idGroupColumn.DataPropertyName = "idGroup";
            this.idGroupColumn.HeaderText = "idGroup";
            this.idGroupColumn.Name = "idGroupColumn";
            this.idGroupColumn.ReadOnly = true;
            this.idGroupColumn.Visible = false;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "idExam";
            this.Column1.HeaderText = "idExam";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // idLecturerColumn
            // 
            this.idLecturerColumn.DataPropertyName = "idLecturer";
            this.idLecturerColumn.HeaderText = "idLecurer";
            this.idLecturerColumn.Name = "idLecturerColumn";
            this.idLecturerColumn.ReadOnly = true;
            this.idLecturerColumn.Visible = false;
            // 
            // idSubjectColumn
            // 
            this.idSubjectColumn.DataPropertyName = "idSubject";
            this.idSubjectColumn.HeaderText = "idSubject";
            this.idSubjectColumn.Name = "idSubjectColumn";
            this.idSubjectColumn.ReadOnly = true;
            this.idSubjectColumn.Visible = false;
            // 
            // SessionReviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1097, 489);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.reportButton);
            this.Controls.Add(this.generateScheduleButton);
            this.Controls.Add(this.govCheckBox);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.descrTextBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dgv);
            this.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SessionReviewForm";
            this.Text = "SessionReviewForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SessionReviewForm_FormClosing);
            this.Load += new System.EventHandler(this.SessionReviewForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.TextBox descrTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox govCheckBox;
        private System.Windows.Forms.Button generateScheduleButton;
        private System.Windows.Forms.Button reportButton;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameGroupColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn subgroupColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn subjectNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lecturerNameColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn comissionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn examDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idGroupColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idLecturerColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idSubjectColumn;
    }
}