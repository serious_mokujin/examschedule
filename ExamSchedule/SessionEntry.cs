﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using ExamSchedule.BO;

namespace ExamSchedule
{
    public class SessionEntry : INotifyPropertyChanged
    {
        private int _idGroup;

        public int IdGroup
        {
            get { return _idGroup; }
            set { _idGroup = value; if (PropertyChanged != null)PropertyChanged(this, new PropertyChangedEventArgs("IdGroup")); }
        }

        private string _nameGroup;
        private string _nameSubject;
        //private string _codeSubject;
        private int _idLecturer;

        public int IdLecturer
        {
            get { return _idLecturer; }
            set { _idLecturer = value; if (PropertyChanged != null)PropertyChanged(this, new PropertyChangedEventArgs("IdLecturer")); }
        }

        public int IdSubject
        {
            get { return _idSubject; }
            set { _idSubject = value; if (PropertyChanged != null)PropertyChanged(this, new PropertyChangedEventArgs("IdSubject")); }
        }

        private int _idSubject;
        private string[] _commission;

        public string[] Commission
        {
            get { return _commission; }
            set
            {
                _commission = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Commission"));
                    PropertyChanged(this, new PropertyChangedEventArgs("CommissionStr"));
                }
            }
        }
        public string CommissionStr
        {
            get { return string.Join(";", _commission); }
            set { _commission = value.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries); if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("CommissionStr")); }
        }
        private string _nameLecturer;

        public string NameGroup
        {
            get { return _nameGroup; }
            set { _nameGroup = value; if (PropertyChanged != null)PropertyChanged(this, new PropertyChangedEventArgs("NameGroup")); }
        }

        public string NameSubject
        {
            get { return _nameSubject; }
            set { _nameSubject = value; if (PropertyChanged != null)PropertyChanged(this, new PropertyChangedEventArgs("NameSubject")); }
        }

        public string NameLecturer
        {
            get { return _nameLecturer; }
            set { _nameLecturer = value; if (PropertyChanged != null)PropertyChanged(this, new PropertyChangedEventArgs("NameLecturer")); }
        }

        private DateTime examDate;

        public DateTime ExamDate
        {
            get { return examDate; }
            set { examDate = value; if (PropertyChanged != null)PropertyChanged(this, new PropertyChangedEventArgs("ExamDate")); }
        }

        public DateTime ConsDate
        {
            get { return consDate; }
            set { consDate = value; if (PropertyChanged != null)PropertyChanged(this, new PropertyChangedEventArgs("ConsDate")); }
        }

        private DateTime consDate;

        private string[] _auditories;

        public string[] Auditories
        {
            get { return _auditories; }
            set { _auditories = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Audiotories"));
                    PropertyChanged(this, new PropertyChangedEventArgs("AuditoriesString"));
                }
            }
        }
        public string AuditoriesString { get { return string.Join(",", Auditories); } }

        public string ConsAuditory
        {
            get { return _consAuditory; }
            set { _consAuditory = value; if (PropertyChanged != null)PropertyChanged(this, new PropertyChangedEventArgs("ConsAuditory")); }
        }

        public string ExamAuditory
        {
            get { return _examAuditory; }
            set { _examAuditory = value;if(PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("ExamAuditory")); }
        }

        private string _consAuditory;
        private string _examAuditory;


        public SessionEntry()
        {
            _idGroup = -1;
            _idLecturer = -1;
            _idSubject = -1;
            this._nameGroup = "";
            this._nameSubject = "";
            this._nameLecturer = "";
        }

        public SessionEntry(int idGroup, string group, string lecturer,int idLecturer, string nameSubject, int idSubject, string[] auditories, string[] commission, bool divided = false)
        {
            this._nameGroup = group;
            this._nameSubject = nameSubject;
            this._nameLecturer = lecturer;
            _idLecturer = idLecturer;
            _idSubject = idSubject;
            _commission = commission;
            _auditories = auditories;
            _idGroup = idGroup;
        }

        public bool InComission(string lecturerName)
        {
            foreach (var s in _commission)
            {
                if (s == lecturerName)
                    return true;
            }
            return false;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
