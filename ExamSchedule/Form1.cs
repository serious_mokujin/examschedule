﻿using System.Globalization;
using ExamSchedule.BO;
using ExamSchedule.InputForms;
using ExamSchedule.OtherForms;
using ExamSchedule.OtherForms.CreateSessionMaster;
using ExamSchedule.VO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace ExamSchedule
{
    public partial class Form1 : Form
    {
        Dictionary<string, string> settings;
        private SessionBO sessionBO;
        private SessionVO current;
       
        public Form1()
        {
            InitializeComponent();
            sessionBO = new SessionBO();
            

        }
        void LoadSettings(string fileName)
        {
            if (settings == null)
                settings = new Dictionary<string, string>();
            else
                settings.Clear();
            if (File.Exists(fileName))
            {
                StreamReader r = File.OpenText(fileName);
                while (r.Peek() != -1)
                {
                    string l = r.ReadLine();
                    string[] values = l.Split(" =".ToCharArray());
                    settings.Add(values[0], values[1].Trim('"'));
                }
                r.Close();
            }
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Server.DisposeConnection();
            DB.DisposeConnection();

            this.Hide();
            if (new ConnectionForm(true).ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.Show();
            else
                this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {           
            if (new ConnectionForm().ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                this.Close();
                return;
            }
            dgv.DataSource = sessionBO.GetRaw();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Server.DisposeConnection();
            DB.DisposeConnection();
            Close();
        }
        private void отделенияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new DepartmentForm();
            f.ShowDialog();
            while (((InputForm)f).Next != null)
            {
                f = ((InputForm)f).Next;
                f.ShowDialog();
            }
        }
        private void studentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new StudentForm();
            f.ShowDialog();
            while (((InputForm)f).Next != null)
            {
                f = ((InputForm)f).Next;
                f.ShowDialog();
            }
        }
        private void lecturerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new LecturerForm();
            f.ShowDialog();
            while (((InputForm)f).Next != null)
            {
                f = ((InputForm)f).Next;
                f.ShowDialog();
            }
        }
        private void groupToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new GroupForm();
            f.ShowDialog();
            while (((InputForm)f).Next != null)
            {
                f = ((InputForm)f).Next;
                f.ShowDialog();
            }
        }
        private void subjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new SubjectForm();
            f.ShowDialog();
            while (((InputForm) f).Next != null)
            {
                f = ((InputForm) f).Next;
                f.ShowDialog();
            }
        }
        private void markToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new MarkForm().ShowDialog();
        }

        private void ведомостьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new SheetBuildForm().ShowDialog();
        }

        private void createButtonSession(object sender, EventArgs e)
        {
            var f = new CreateSessionStepOne();
            f.ShowDialog();
            //new CreateSessionForm().ShowDialog();
            dgv.DataSource = sessionBO.GetRaw();
        }

        private void DeleteSession(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                sessionBO.DeleteSession(current);
                dgv.DataSource = sessionBO.GetRaw();
            }
        }
        private void dgv_SelectionChanged(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                current = sessionBO.GetSessionWithId((int)dgv.SelectedRows[0].Cells[0].Value);
            }
        }

        
        private void generateSchedule(object sender, EventArgs e)
        {
            new CreateScheduleForm(current).ShowDialog();
        }

        private void сбросочисткаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (
                MessageBox.Show("Вы действительно хотите удалить все данные из БД и создать ее заново?",
                                "Опасная операция", MessageBoxButtons.YesNo,MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                Server.DropDatabase("ExamSchedule");
                Server.CreateDatabase("CreationScript.sql");
                dgv.DataSource = sessionBO.GetRaw();
            }

        }

        private void sessionReviewButton_Click(object sender, EventArgs e)
        {
            if (dgv.SelectedRows.Count > 0)
            {
                SessionReviewForm f = new SessionReviewForm(sessionBO.GetSessionWithId((int) dgv.SelectedRows[0].Cells[0].Value));
                f.ShowDialog();
                if(f.Next != null)
                    f.Next.ShowDialog();
                dgv.DataSource = sessionBO.GetRaw();
            }
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutForm().ShowDialog();
        }

        private void поискToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SearchForm f = new SearchForm();
            if (f.ShowDialog() == DialogResult.OK)
            {
                int year = 0;
                if (int.TryParse(f.SearchText, out year))
                {
                    dgv.DataSource = sessionBO.GetOnYearRaw(year);
                }
                else
                {
                    dgv.DataSource = sessionBO.GetRaw();
                }
            }
        }

    }
}
