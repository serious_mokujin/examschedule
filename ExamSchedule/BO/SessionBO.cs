﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ExamSchedule.DAO;
using ExamSchedule.VO;

namespace ExamSchedule.BO
{
    public class SessionBO
    {
        private SessionDAO sessionDAO;

        public SessionBO()
        {
            sessionDAO = new SessionDAO();
        }

        public SessionVO GetSessionWithId(int id)
        {
            return Parse(sessionDAO.GetSessionWithId(id));
        }

        public LecturerVO[] GetLecturersInSession(SessionVO session)
        {
            return LecturerBO.Parse(sessionDAO.GetLecturersInSession(session.Id));
        }

        public string[] GetGroupsInSession(SessionVO session)
        {
            var table = sessionDAO.GetGroupsInSession(session.Id);
            string[] groups = new string[table.Rows.Count];
            int i = 0;
            foreach (DataRow item in table.Rows)
            {
                groups[i++] = (string) item["nameGroup"];
            }
            return groups;
        }

        public DataTable GetRaw()
        {
            return sessionDAO.GetSessions();
        }

        public DataTable GetOnYearRaw(int year)
        {
            return sessionDAO.GetSessionsOnYear(year);
        }

        public void UpdateSession(SessionVO session)
        {
            if (session.Id == -1)
            {
                session.Id = sessionDAO.InsertSession(session.StartDate, session.EndDate, session.Description,
                                                      session.Gov);
            }
            else
            {
                sessionDAO.UpdateSession(session.Id, session.StartDate, session.EndDate, session.Description,
                                         session.Gov);
            }
        }

        public void DeleteSession(SessionVO session)
        {
            if (session.Id != -1)
                sessionDAO.DeleteSession(session.Id);
        }

        public static SessionVO Parse(DataRow row)
        {
            return new SessionVO((int) row["idSession"], (DateTime) row["startDateSession"],
                                 (DateTime) row["endDateSession"],
                                 (string) (row.IsNull("descriptionSession") ? "" : row["descriptionSession"]),
                                 (bool) row["govSession"]);
        }
    }
}
