﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ExamSchedule.DAO;
using ExamSchedule.VO;

namespace ExamSchedule.BO
{
    public class ExamBO
    {
        ExamDAO examDAO;

        public ExamBO()
        {
            examDAO = new ExamDAO();
        }

        public DataTable GetRaw()
        {
            return examDAO.GetExams();
        }
        public DataTable GetRawInSession(int sessionId)
        {
            return examDAO.GetExamsInSession(sessionId);
        }
        public DataTable GetRawInSessionWithNames(int sessionId)
        {
            return examDAO.GetExamsInSessionWithNames(sessionId);
        }
        public ExamVO GetExamWithId(int id)
        {
            return Parse(examDAO.GetExamWithId(id));
        }

        public void UpdateExam(ExamVO exam)
        {
            if (exam.Id == -1)
            {
                exam.Id = examDAO.InsertExam(exam.IdSession, exam.IdGroup, exam.NameGroup, exam.IdLecturer,
                                             exam.IdSubject, exam.ConsAuditory,
                                             exam.ConsDate, exam.ExamAuditory, exam.ExamDate,
                                             exam.SubGroup, exam.CommissionStr);
            }
            else
            {
                examDAO.UpdateExam(exam.Id, exam.IdSession, exam.IdGroup, exam.NameGroup, exam.IdLecturer,
                                   exam.IdSubject, exam.ConsAuditory,
                                   exam.ConsDate, exam.ExamAuditory, exam.ExamDate,
                                   exam.SubGroup, exam.CommissionStr);
            }
        }
        public void DeleteExam(ExamVO exam)
        {
            if (exam.Id != -1)
                examDAO.DeleteExam(exam.Id);
        }

        public static ExamVO[] Parse(DataTable table)
        {
            ExamVO[] exams = new ExamVO[table.Rows.Count];
            int i = 0;
            foreach (DataRow row in table.Rows)
                exams[i++] = Parse(row);
            return exams;
        }
        public static ExamVO Parse(DataRow row)
        {
            return new ExamVO((int) row["idExam"], (int) row["idSession"], (int)row["idGroup"], (string) row["nameGroup"],
                              (int) row["idLecturer"], (int) row["idSubject"],
                              (DateTime) row["consDate"], (DateTime) row["examDate"], (string) row["consAuditory"],
                              (string) row["examAuditory"], (int) row["subGroup"], (string)row["commissionExam"]);
        }
        /*
        public string[] GetGroupSubjectLecturerNames(int idGroup, int idSubject, int idLecturer)
        {
            var table = examDAO.GetNamesGroupSubjectLecturer(idGroup, idSubject, idLecturer);
            if (table.Rows.Count > 0)
                return new string[] { table.Rows[0]["nameGroup"].ToString(), table.Rows[0]["nameSubject"].ToString(), table.Rows[0]["nameLecturer"].ToString() };
            return null;
        }*/

        internal DataTable GetRawInSessionOfCourse(SessionVO session, int course)
        {
            return examDAO.GetExamsInSessionOfCourse(session.Id, course);
        }

        internal DataTable GetRawInSessionForLecturer(SessionVO session, LecturerVO lecturer)
        {
            return examDAO.GetExamsInSessionForLecturer(session.Id, lecturer.Id);
        }

        public DataTable GetRawInSessionForGroup(SessionVO session, string group)
        {
            return examDAO.GetExamsInSessionForGroup(session.Id, group);
        }
        public DataTable GetRawInSessionOnDate(SessionVO session, DateTime date)
        {
            return examDAO.GetExamsInSessionOnDate(session.Id, date);
        }

        public ExamVO[] GetExamsFromDate(DateTime date)
        {
            return Parse(examDAO.GetExamsFromDate(date));
        }
    }
}
