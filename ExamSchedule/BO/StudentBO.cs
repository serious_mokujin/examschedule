﻿using ExamSchedule.DAO;
using ExamSchedule.VO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ExamSchedule.BO
{
    public class StudentBO
    {
        StudentDAO studentDAO;

        public StudentBO()
        {
            studentDAO = new StudentDAO();
        }

        public DataTable GetRaw()
        {
            return studentDAO.GetStudentsWithGroups();
        }
        public DataTable GetRawSearchName(string name)
        {
            return studentDAO.GetStudentsWithGroupsSearchName(name);
        }
        public DataTable GetRawWithinGroup(GroupVO group)
        {
            return studentDAO.GetStudentsWithGroupsWithinGroup(group.Id);
        }

        public DataTable GetRawWithinGroupSearchName(GroupVO group, string name)
        {
            return studentDAO.GetStudentsWithGroupsWithinGroupSearchName(group.Id, name);
        }

        public DataTable GetStudentMarkRaw(GroupVO group, SubjectVO subject)
        {
            return studentDAO.GetStudentMark(group.Id, subject.Id);
        }

        public StudentVO GetStudentWithId(int id)
        {
            return Parse(studentDAO.GetStudentWithId(id));
        }
        public DataTable GetStudentsFromGroupRaw(int groupId)
        {
            return studentDAO.GetStudentsFromGroup(groupId);
        }

        public StudentVO[] GetStudents()
        {
            var t = studentDAO.GetStudents();
            StudentVO[] arr = new StudentVO[t.Rows.Count];
            int i = 0;
            foreach (DataRow r in t.Rows)
            {
                arr[i++] = Parse(r);
            }
            return arr;
        }
        public StudentVO[] GetStudentsFromGroup(GroupVO group)
        {
            var t = studentDAO.GetStudentsFromGroup(group.Id);
            StudentVO[] arr = new StudentVO[t.Rows.Count];
            int i = 0;
            foreach (DataRow r in t.Rows)
            {
                arr[i++] = Parse(r);
            }
            return arr;
        }

        public void UpdateStudent(StudentVO student)
        {
            if (student.Id == -1)
            {
                student.Id = studentDAO.InsertStudent(student.Name, student.IdGroup, student.RegisterNumber);
            }
            else
            {
                studentDAO.UpdateStudent(student.Id, student.Name, student.IdGroup, student.RegisterNumber);
            }
        }
        public void DeleteStudent(StudentVO student)
        {
            if (student.Id != -1)
                studentDAO.DeleteStudent(student.Id);
        }
        public static StudentVO Parse(DataRow row)
        {
            return new StudentVO((int)row["idStudent"], (string)row["nameStudent"], (int)row["idGroup"], (int)row["regStudent"]);
        }
    }
}
