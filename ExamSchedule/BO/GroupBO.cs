﻿using ExamSchedule.DAO;
using ExamSchedule.VO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ExamSchedule.BO
{
    class GroupBO
    {
        GroupDAO groupDAO;

        public GroupBO()
        {
            groupDAO = new GroupDAO();
        }

        public DataTable GetRaw()
        {
            return groupDAO.GetGroups();
        }
        public DataTable GetNotGraduatedGroupsRaw()
        {
            return groupDAO.GetNotGraduatedGroups();
        }
        public DataTable GetGroupsInDepartmentRaw(DepartmentVO department)
        {
            return groupDAO.GetGroupsInDepartment(department.Id);
        }
        public DataTable GetGroupsInDepartmentNotGraduatedRaw(DepartmentVO department)
        {
            return groupDAO.GetGroupsInDepartmentNotGraduated(department.Id);
        }
        public GroupVO GetGroupWithId(int id)
        {
            return Parse(groupDAO.GetGroupWithId(id));
        }
        
        public GroupVO[] GetGroupsInDepartment(DepartmentVO department)
        {
            return Parse(groupDAO.GetGroupsInDepartment(department.Id));
        }
        
        public GroupVO[] GetGroups()
        {
            return Parse(groupDAO.GetGroups());
        }
        
        public GroupVO[] GetNotGraduatedGroups()
        {
            return Parse( groupDAO.GetNotGraduatedGroups());
        }
        public GroupVO[] GetGroupsInDepartmentNotGraduated(DepartmentVO department)
        {
            return Parse(groupDAO.GetGroupsInDepartmentNotGraduated(department.Id));
        }
        public static GroupVO[] Parse(DataTable table)
        {
            GroupVO[] groups = new GroupVO[table.Rows.Count];
            int i = 0;
            foreach (DataRow item in table.Rows)
            {
                groups[i++] = Parse(item);
            }
            return groups;
        }
        public void UpdateGroup(GroupVO group)
        {
            if (group.Id == -1)
            {
                group.Id = groupDAO.InsertGroup(group.Course, group.Number, group.YearPostfix, group.CustomPostfix, group.IdDepartment, group.Graduated);
            }
            else
            {
                groupDAO.UpdateGroup(group.Id, group.Course, group.Number, group.YearPostfix, group.CustomPostfix, group.IdDepartment, group.Graduated);
            }
        }
        public void DeleteGroup(GroupVO group)
        {
            if (group.Id != -1)
                groupDAO.DeleteGroup(group.Id);
        }
        public static GroupVO Parse(DataRow row)
        {
            var g = new GroupVO((int) row["idGroup"], (int) row["numberGroup"], (int) row["courseGroup"],
                                (int) row["yearPostfixGroup"],
                                (row["customPostfixGroup"] is DBNull) ? ' ' : ((string) row["customPostfixGroup"])[0],
                                (int) row["idDepartment"], (bool)row["graduated"]);
            try
            {
                g.Name = (string)row["nameGroup"];
            }
            catch (Exception){}
            
            return g;
        }

        internal GroupVO GetGroupWithName(string name)
        {
            var r = groupDAO.GetGroupWithName(name);
            if (r != null)
                return Parse(r);
            return null;
        }
    }
}
