﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ExamSchedule.DAO;
using ExamSchedule.VO;

namespace ExamSchedule.BO
{
    public class MarkBO
    {
        MarkDAO markDAO;

        public MarkBO()
        {
            markDAO = new MarkDAO();
        }

        public DataTable GetRaw()
        {
            return markDAO.GetMarks();
        }
        public MarkVO GetMarkWithId(int id)
        {
            return Parse(markDAO.GetMarkWithId(id));
        }
        public DataTable GetMarksFromGroupRaw(GroupVO group)
        {
            return markDAO.GetMarksFromGroup(group.Id);
        }

        public MarkVO[] GetMarks()
        {
            return Parse(markDAO.GetMarks());
        }
        public void UpdateMark(MarkVO mark)
        {
            if (mark.Id == -1)
            {
                mark.Id = markDAO.InsertMark(mark.Mark, mark.Date, mark.IdLecturer, mark.IdStudent, mark.IdSubject);
            }
            else
            {
                markDAO.UpdateMark(mark.Id, mark.Mark, mark.Date, mark.IdLecturer, mark.IdStudent, mark.IdSubject);
            }
        }
        public void DeleteMark(MarkVO mark)
        {
            if (mark.Id != -1)
                markDAO.DeleteMark(mark.Id);
        }
        public static MarkVO Parse(DataRow row)
        {
            return new MarkVO((int) row["idMark"], (int) row["mark"], (DateTime) row["dateMark"],
                              (int) row["idLecturer"], (int) row["idStudent"], (int) row["idSubject"]);
        }
        public static MarkVO[] Parse(DataTable table)
        {
            MarkVO[] marks = new MarkVO[table.Rows.Count];
            int i = 0;
            foreach (DataRow item in table.Rows)
            {
                marks[i++] = Parse(item);
            }
            return marks;
        }

        internal MarkVO[] GetMarksFromGroup(GroupVO group)
        {
            return Parse(markDAO.GetMarksFromGroup(group.Id));
        }

        internal MarkVO[] GetMarksFromGroupBySubject(GroupVO group, SubjectVO subject)
        {
            return Parse(markDAO.GetMarksFromGroupBySubject(group.Id, subject.Id));
        }
        internal MarkVO[] GetMarksForStudent(StudentVO student)
        {
            return Parse(markDAO.GetMarksForStudent(student.Id));
        }

        internal MarkVO[] GetMarksForStudentBySubject(StudentVO student, SubjectVO subject)
        {
            return Parse(markDAO.GetMarksForStudentBySubject(student.Id, subject.Id));
        }
        public DataTable GetMarksFromGroupBySubjectWithNamesRaw(GroupVO group, SubjectVO subject)
        {
            return markDAO.GetMarksFromGroupBySubjectWithNames(group.Id, subject.Id);
        }
        public bool MarkExists(StudentVO student, SubjectVO subject, LecturerVO lecturer)
        {
            return markDAO.GetMarkWithSubjectStudentLecturer(subject.Id, student.Id, lecturer.Id).Rows.Count>0;
        }
    }
}
