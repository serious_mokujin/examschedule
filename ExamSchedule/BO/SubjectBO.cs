﻿using ExamSchedule.DAO;
using ExamSchedule.VO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ExamSchedule.BO
{
    public class SubjectBO
    {
        SubjectDAO subjectDAO;

        public SubjectBO()
        {
            subjectDAO = new SubjectDAO();
        }

        public DataTable GetRaw()
        {
            return subjectDAO.GetSubjects();
        }
        public void LinkWithLecturer(SubjectVO subject, LecturerVO lecturer)
        {
            subjectDAO.InsertLecturerSubject(lecturer.Id, subject.Id);
        }
        
        public void DetachSubjectFromLecturer(SubjectVO subject, LecturerVO lecturer)
        {
            subjectDAO.DeleteLecturerSubject(lecturer.Id, subject.Id);
        }
        public SubjectVO GetSubjectWithId(int id)
        {
            return Parse(subjectDAO.GetSubjectrWithId(id));
        }
        public SubjectVO[] GetSubjects()
        {
            var table = GetRaw();
            SubjectVO[] subjects = new SubjectVO[table.Rows.Count];
            int i = 0;
            foreach (DataRow item in table.Rows)
            {
                subjects[i++] = Parse(item);
            }
            return subjects;
        }
        public void UpdateSubject(SubjectVO subject)
        {
            string mark = (subject.MarkType == SubjectVO.MarkT._5 ? "5" : "12");

            if (subject.Id == -1)
            {
                subject.Id = subjectDAO.InsertSubject(subject.Code, subject.Name, subject.Hours, mark);
            }
            else
            {
                subjectDAO.UpdateSubject(subject.Id, subject.Code, subject.Name, subject.Hours, mark);
            }
        }
        public SubjectVO[] GetSubjectsByLecturer(int lecturerId)
        {
            DataTable table = subjectDAO.GetSubjectsByLecturer(lecturerId);
            SubjectVO[] vos = new SubjectVO[table.Rows.Count];
            int i = 0;
            foreach (var item in table.Rows)
            {
                vos[i++] = Parse((DataRow)item);
            }
            return vos;

        }
        public void DeleteSubject(SubjectVO subject)
        {
            if (subject.Id != -1)
                subjectDAO.DeleteSubject(subject.Id);
        }
        private SubjectVO Parse(DataRow row)
        {
            SubjectVO.MarkT mark = (string)row["markTypeSubject"] == "5" ? SubjectVO.MarkT._5 : SubjectVO.MarkT._12;
            return new SubjectVO((int)row["idSubject"], (string)row["nameSubject"], (string)row["codeSubject"], (int)row["hoursSubject"],  mark);
        }

        public SubjectVO GetSubjectWithName(string name)
        {
            var r = subjectDAO.GetSubjectrWithName(name);
            if (r != null)
                return Parse(r);
            return null;
        }

        public SubjectVO GetSubjectWithCode(string code)
        {
            var r = subjectDAO.GetSubjectrWithCode(code);
            if (r != null)
                return Parse(r);
            return null;
        }
    }
}
