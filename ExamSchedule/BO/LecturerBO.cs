﻿using ExamSchedule.DAO;
using ExamSchedule.VO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ExamSchedule.BO
{
    public class LecturerBO
    {
        LecturerDAO lecturerDAO;

        public LecturerBO()
        {
            lecturerDAO = new LecturerDAO();
        }

        public DataTable GetRaw()
        {
            return lecturerDAO.GetLecturers();
        }

        public LecturerVO GetLecturerWithId(int id)
        {
            return Parse(lecturerDAO.GetLecturerWithId(id));
        }
        public LecturerVO[] GetLecturers()
        {
            var table = GetRaw();
            LecturerVO[] lecturers = new LecturerVO[table.Rows.Count];
            int i = 0;
            foreach (DataRow item in table.Rows)
            {
                lecturers[i++] = Parse(item);
            }
            return lecturers;
        }
        public void UpdateLecturer(LecturerVO lecturer)
        {
            if (lecturer.Id == -1)
            {
                lecturer.Id = lecturerDAO.InsertLecturer(lecturer.Name);
            }
            else
            {
                lecturerDAO.UpdateLecturer(lecturer.Id, lecturer.Name);
            }
        }
        public LecturerVO[] GetLecturersBySubject(SubjectVO subject)
        {
            DataTable table = lecturerDAO.GetLecturersBySubject(subject.Id);
            LecturerVO[] vos = new LecturerVO[table.Rows.Count];
            int i = 0;
            foreach (var item in table.Rows)
            {
                vos[i++] = Parse((DataRow)item);
            }
            return vos;

        }
        public void DeleteLecturer(LecturerVO lecturer)
        {
            if (lecturer.Id != -1)
                lecturerDAO.DeleteLecturer(lecturer.Id);
        }
        public static LecturerVO[] Parse(DataTable table)
        {
            LecturerVO[] lecturers = new LecturerVO[table.Rows.Count];
            int i = 0;
            foreach (DataRow row in table.Rows)
                lecturers[i++] = Parse(row);
            return lecturers;
        }
        public static LecturerVO Parse(DataRow row)
        {
            return new LecturerVO((int)row["idLecturer"], (string)row["nameLecturer"]);
        }

        public LecturerVO GetLecturerWithName(string name)
        {
            var r = lecturerDAO.GetLecturerWithName(name);
            if (r != null)
                return Parse(r);
            return null;
        }
    }
}
