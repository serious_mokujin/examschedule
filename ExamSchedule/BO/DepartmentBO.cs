﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ExamSchedule.DAO;
using ExamSchedule.VO;

namespace ExamSchedule.BO
{
    class DepartmentBO
    {
        DepartmentDAO departmentDAO;

        public DepartmentBO()
        {
            departmentDAO = new DepartmentDAO();
        }

        public DataTable GetRaw()
        {
            return departmentDAO.GetDepartments();
        }

        public DataTable GetRawWithNames()
        {
            return departmentDAO.GetDepartmentsWithNames();
        }

        public DepartmentVO GetDepartmentWithId(int id)
        {
            return Parse(departmentDAO.GetDepartmentWithId(id));
        }
        
        public DepartmentVO[] GetDepartments()
        {
            var table = GetRaw();
            DepartmentVO[] departments = new DepartmentVO[table.Rows.Count];
            int i = 0;
            foreach (DataRow item in table.Rows)
            {
                departments[i++] = Parse(item);
            }
            return departments;
        }
        public void UpdateDepartment(DepartmentVO department)
        {
            if (department.Id == -1)
            {
                department.Id = departmentDAO.InsertDepartment(department.Name, department.ShortForm, department.IdLecturer, department.SpecializationName, department.SpecializationCode);
            }
            else
            {
                departmentDAO.UpdateDepartment(department.Id, department.Name, department.ShortForm, department.IdLecturer, department.SpecializationName, department.SpecializationCode);
            }
        }
        public void DeleteDepartment(DepartmentVO department)
        {
            if (department.Id != -1)
                departmentDAO.DeleteDepartment(department.Id);
        }
        public static DepartmentVO Parse(DataRow row)
        {
            return new DepartmentVO((int) row["idDepartment"], (string) row["nameDepartment"],
                                    (int) row["idHeadLecturer"], (string) row["shortFormDepartment"],
                                    (string) row["specializationName"], (string) row["specializationCode"]);
        }
    }
}
