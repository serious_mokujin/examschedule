﻿using System;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace ExamSchedule
{
    public enum EventType
    {
        exam, cons
    }

    public struct SessionEvent
    {
        private SessionEntry _entry;
        private EventType _eventType;
        private DateTime _dateTime;
        private string _auditory;
        private int _subgroup;

        public int Subgroup
        {
            get { return _subgroup; }
            set { _subgroup = value; }
        }

        public DateTime DateTime
        {
            get { return _dateTime; }
            set { _dateTime = value; }
        }

        public string Auditory { get { return _auditory; } set { _auditory = value; } }

        public SessionEntry Entry
        {
            get { return _entry; }
            set { _entry = value; }
        }

        public EventType EventType
        {
            get { return _eventType; }
            set { _eventType = value; }
        }

        public SessionEvent(SessionEntry entry, EventType eventType, DateTime dateTime, string auditory, int subgroup = 0)
        {
            _dateTime = dateTime;
            _entry = entry;
            _eventType = eventType;
            _auditory = auditory;
            _subgroup = subgroup;
        }
    }
}
