﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExamSchedule
{
    public static class Extensions
    {
        public static string Embrace(this string str, string brace)
        {
            return brace + str + brace;
        }
        public static void RandomMix<T>(this List<T> list, int seed)
        {
            Random rnd = new Random(seed);
            for (int i = 0; i < list.Count; i++)
            {
                T element = list[i];
                list.RemoveAt(i);
                list.Insert(rnd.Next(0, list.Count), element);
            }
        }
    }
}
